﻿using System;
using System.Threading.Tasks;
using System.Web.Http;
using Bootes.Core.Common;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(Bootes.Service.Startup))]

namespace Bootes.Service
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration config = new HttpConfiguration(); 
            AutoMapperConfig.RegisterMappings();
            Core.Data.DB.BootesPoco.Setup(); 
            config = WebApiConfig.Register(config); 
            app.UseWebApi(config); 
        }
    }
}
