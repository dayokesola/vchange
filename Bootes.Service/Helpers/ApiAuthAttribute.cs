using Bootes.Core.Common; 
using Bootes.Core.Domain.Model.USSD;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
namespace Bootes.Service.Helpers
{
public class ApiAuthAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var model = Process(actionContext);
            if (model.status != HashStatus.SUCCESSFUL)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, model.message);
            }
            base.OnActionExecuting(actionContext);
        }

        public override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            var model = Process(actionContext);
            if (model.status != HashStatus.SUCCESSFUL)
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, model.message);
            }
            return base.OnActionExecutingAsync(actionContext, cancellationToken);
        }


        private Relay Process(HttpActionContext actionContext)
        {
            var model = new Relay();
            if (actionContext.ActionArguments.ContainsKey("model"))
            {
                model = actionContext.ActionArguments["model"] as Relay;
            }
            model.status = HashStatus.SUCCESSFUL;

            var vals = new List<string>().AsEnumerable();
            if (!actionContext.Request.Headers.TryGetValues("ApiKey", out vals))
            {
                model.status = HashStatus.INVALID_AUTH;
                model.message = "No ApiKey Header";
                return model;
            }

            var vousHash = vals.FirstOrDefault();
            if (string.IsNullOrEmpty(vousHash))
            {
                model.status = HashStatus.INVALID_AUTH;
                model.message = "No ApiKey Header value";
                return model;
            }

            vousHash = vousHash.ToUpper();


            var nousHash = CryptUtil.Hash512(Constants.HASH_KEY + model.session + model.mobile).ToUpper();

            if (nousHash != vousHash)
            {
                model.status = HashStatus.INVALID_AUTH;
                model.message = "Invalid Hash Credentials";
                return model;
            }
            return model;
        }
    }
}