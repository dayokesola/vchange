﻿using Bootes.Core.Common;
using Bootes.Core.Data.Module;
using Bootes.Core.Domain.Model.Account;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Bootes.Service.Helpers
{

    public class APIGuardAttribute : ActionFilterAttribute
    {
        private DataModule data;
        public string Permission { get; set; }
        private ClientModel NuClient { get; set; }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var m = AuthenticateMe(actionContext);
            if (m != "00")
            {
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Unauthorized, m);
                return;
            }

            m = AuthorizeMe(actionContext);
            if (m != "00")
            {
                Task.Factory.StartNew(() => AddPermissionIfNotExist());
                actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.Forbidden, m);
            }
            base.OnActionExecuting(actionContext);
        }

        private void AddPermissionIfNotExist()
        {

            //var exists = data.Permissions.Search(name: Permission);
            //if (exists == null)
            //{
            //    data.Permissions.Insert(new Permission() { Id = 0, PermissionName = Permission });
            //}
        }
        
        private string AuthenticateMe(HttpActionContext actionContext)
        {
            var model = "00";

            var vals = new List<string>().AsEnumerable();
            if (!actionContext.Request.Headers.TryGetValues("clientId", out vals))
            {
                model = "no clientId header";
                return model;
            }
            var clientId = vals.FirstOrDefault();
            if (string.IsNullOrEmpty(clientId))
            {
                model = "no clientId value";
                return model;
            }
            clientId = clientId.ToLower();


            if (!actionContext.Request.Headers.TryGetValues("apikey", out vals))
            {
                model = "no apikey header";
                return model;
            }
            var apikey = vals.FirstOrDefault();
            if (string.IsNullOrEmpty(apikey))
            {
                model = "no apikey value";
                return model;
            }
            apikey = apikey.ToLower();


            if (!actionContext.Request.Headers.TryGetValues("timeshot", out vals))
            {
                model = "no timeshot header";
                return model;
            }
            var timeshot = vals.FirstOrDefault();
            if (string.IsNullOrEmpty(apikey))
            {
                model = "no timeshot value";
                return model;
            }
            timeshot = timeshot.ToLower();

            var _timeshot = Util.ToDateTime(timeshot);
            if (_timeshot < Util.CurrentDateTime().AddMinutes(-15))
            {
                model = "timeshot is expired: " + timeshot;
                return model;
            }
            data = new DataModule();

            //get api from db
            NuClient = data.Clients.GetByClientId(clientId);

            if (NuClient == null)
            {
                model = "Invalid Credentials";
                return model;
            }

            var nousHash1 = CryptUtil.Hash256(timeshot + NuClient.ClientSecret1 + clientId).ToLower();
            var nousHash2 = CryptUtil.Hash256(timeshot + NuClient.ClientSecret2 + clientId).ToLower();

            if (nousHash1 != apikey || nousHash2 != apikey)
            {
                model = "Invalid Hash Credentials";
                return model;
            }
            return model;
        }


        private string AuthorizeMe(HttpActionContext actionContext)
        {
            var model = "00";

            ////get api from db
            //var isOk = data.ClientPermissions.Verify(NuClient.Id, Permission);
            //if (!isOk)
            //{
            //    model = "permission!!!";
            //    return model;
            //}
            return model;
        }
    }
}