﻿using Bootes.Core.Common;
using Bootes.Service.Helpers;
using Swashbuckle.Application;
using System.Web.Http;
using System.Web.Http.Cors;

namespace Bootes.Service
{
    public static class WebApiConfig
    {
        public static HttpConfiguration Register(HttpConfiguration config)
        {
            // Web API configuration and services 


            // Web API routes
            config.MapHttpAttributeRoutes();

            config.MessageHandlers.Add(new TokenValidationHandler());

            config.Filters.Add(new AuthorizeAttribute());

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
            var cors = new EnableCorsAttribute("*", "*", "*", "X-Pagination")
            {
                PreflightMaxAge = 86400
            };

            config.EnableSwagger(c =>
            {
                c.SingleApiVersion("v1", Constants.APP_NAME);
                c.OperationFilter(() => new AddRequiredHeaderParameter());
            })
            .EnableSwaggerUi();

            config.EnableCors(cors);
            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();
            return config;
        }
    }
}
