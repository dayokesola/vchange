﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model.USSD;
using Bootes.Service.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Bootes.Service.Controllers.USSD
{
    [RoutePrefix("api/Hashtalk/USSD")]
    public class USSDController : BaseUSSDController
    {
        [ApiAuth]
        [HttpPost]
        [AllowAnonymous]
        [Route("AirtimePurchase")]
        public IHttpActionResult AirtimePurchase(Relay model)
        {
            try
            { 
                var amount = Util.ToDecimal( model.GetValue("AMOUNT"));
                if (amount <= 0)
                {
                    return FailedApiCall(model, "99", "Invalid Amount");
                } 
                var user = Logic.SiteUserService.GetByMobile(model.mobile);
                if (user == null)
                {
                    return FailedApiCall(model, "99", "Invalid USER");
                } 
                var wallet = Logic.WalletService.SearchView(ownerId: user.Id, walletTypeId: 3, page: 0).Items.FirstOrDefault();
                if (user == null)
                {
                    return FailedApiCall(model, "99", "No wallet found");
                }

                if(wallet.AvailableBalance < amount)
                {
                    return FailedApiCall(model, "99", "Insufficient Funds");
                }

                //buy airyime
                var ok = false; 
                //api call is Ok
                if (!ok)
                {
                    return FailedApiCall(model, "99", "Airtime Purchase Provider Failed");
                }
                else
                { 
                    return FailedApiCall(model, "00", "Airtime Purchased Ok");
                } 
            }
            catch (Exception ex)
            { 
                return FailedApiCall(model, "Error", ex.Message, "3");
            }
        }

        [ApiAuth]
        [HttpPost]
        [AllowAnonymous]
        [Route("AccountBalance")]
        public IHttpActionResult AccountBalance(Relay model)
        {
            try
            {
                var user = Logic.SiteUserService.GetByMobile(model.mobile);
                if(user == null)
                {
                    return FailedApiCall(model, "99", "Invalid USER");
                }

                var wallet = Logic.WalletService.SearchView(ownerId: user.Id, walletTypeId: 3, page: 0).Items.FirstOrDefault();
                if (wallet == null)
                {
                    return FailedApiCall(model, "99", "No wallet found");
                }
                model.AddData("BALANCE", wallet.AvailableBalance.ToString("0"));
                return SuccessApiCall(model, "00", "Successful"); 
            }
            catch (Exception ex)
            {
                return FailedApiCall(model, "Error", ex.Message, "3");
            }
        }


        [ApiAuth]
        [HttpPost]
        [AllowAnonymous]
        [Route("MiniStatement")]
        public IHttpActionResult MiniStatement(Relay model)
        {
            try
            {
                var user = Logic.SiteUserService.GetByMobile(model.mobile);
                if (user == null)
                {
                    return FailedApiCall(model, "99", "Invalid USER");
                }

                var wallet = Logic.WalletService.SearchView(ownerId: user.Id, walletTypeId: 3, page: 0).Items.FirstOrDefault();
                if (wallet == null)
                {
                    return FailedApiCall(model, "99", "No wallet found");
                }
                //get trnx
                int walletTypeid = 3;
                var items = Logic.WalletStatementService.SearchView(0, 0, 0, 0,
                    0, 0, null, "", "", "", 0, user.Id, walletTypeid, 1, 5, "-ValueDate,-Id").Items;
                var txt = "Hello,";
                if(items != null && items.Any())
                {
                    foreach(var tx in items)
                    {

                        txt += "\n" + tx.ValueDate.ToShortDateString() + " " + tx.DrCrText + " " + tx.Amount;
                    }
                }
                else
                {
                    return FailedApiCall(model, "99", "No transactions found");
                }


                model.AddData("STATEMENT", txt);
                return SuccessApiCall(model, "00", "Successful");
            }
            catch (Exception ex)
            {
                return FailedApiCall(model, "Error", ex.Message, "3");
            }
        }


        [ApiAuth]
        [HttpPost]
        [AllowAnonymous]
        [Route("GiveChange")]
        public IHttpActionResult GiveChange(Relay model)
        {
            try
            {
                var amount = Util.ToDecimal(model.GetValue("AMOUNT"));
                if (amount <= 0)
                {
                    return FailedApiCall(model, "99", "Invalid Amount");
                }

                var benef = model.GetValue("BENEF");


                var user = Logic.SiteUserService.GetByMobile(model.mobile);
                if (user == null)
                {
                    return FailedApiCall(model, "99", "Invalid USER");
                }

                var merchant = Logic.CorporateService.GetUserCorporates(user.Id, 2)
                    .Where(x => x.PublicName == model.GetValue("MERCHANT")).FirstOrDefault(); ;
                if (merchant == null)
                {
                    return FailedApiCall(model, "99", "no merchants found");
                }



                var gc = new GiveChangeForm()
                {
                    Amount = amount,
                    AuthId = user.Id,
                    Beneficiary = benef,
                    SendAlert = false,
                    MerchantId = merchant.Id,
                    Mobile = benef,
                    PaymentReference = model.session,
                    SourceId = "USSD",
                    TransactionCodeId = 3
                };

                var ff = Logic.WalletTransactService.GiveChange(gc);
                if (ff.AuthStatusId == 6)
                {
                    var wallet = Logic.WalletService.SearchView(ownerId: merchant.Id, walletTypeId: 2, page: 0).Items.FirstOrDefault();
                    model.AddData("BALANCE", wallet.AvailableBalance.ToString("0"));
                    return SuccessApiCall(model, "00", "Successful");
                }
                else
                {
                    return FailedApiCall(model, "99", "Transaction failed: " + ff.AuthStatusId);
                }

            }
            catch (Exception ex)
            {
                return FailedApiCall(model, "Error", ex.Message, "3");
            }
        }



        [ApiAuth]
        [HttpPost]
        [AllowAnonymous]
        [Route("MerchantBalance")]
        public IHttpActionResult MerchantBalance(Relay model)
        {
            try
            {
                var user = Logic.SiteUserService.GetByMobile(model.mobile);
                if (user == null)
                {
                    return FailedApiCall(model, "99", "Invalid user");
                }

                var txt = "Hello,";

                var merchants = Logic.CorporateService.GetUserCorporates(user.Id, 2);

                foreach(var merchant in merchants)
                {
                    var wallets = Logic.WalletService.SearchView(ownerId: merchant.Id, walletTypeId: 2, page: 0).Items;
                    if (wallets == null)
                    {
                        continue;
                    }
                    foreach (var wallet in wallets)
                    {
                        txt += "\n" + wallet.Name + " NGN " + wallet.AvailableBalance.ToString("0");
                    }
                }
                
                model.AddData("BALANCE", txt);
                return SuccessApiCall(model, "00", "Successful");
            }
            catch (Exception ex)
            {
                return FailedApiCall(model, "Error", ex.Message, "3");
            }
        }


        [ApiAuth]
        [HttpPost]
        [AllowAnonymous]
        [Route("MerchantStatement")]
        public IHttpActionResult MerchantStatement(Relay model)
        {
            try
            {
                var user = Logic.SiteUserService.GetByMobile(model.mobile);
                if (user == null)
                {
                    return FailedApiCall(model, "99", "Invalid USER");
                }

                var merchant = Logic.CorporateService.GetUserCorporates(user.Id, 2)
                    .Where(x => x.PublicName == model.GetValue("MERCHANT")).FirstOrDefault();;
                if (merchant == null)
                {
                    return FailedApiCall(model, "99", "no merchants found");
                }

                var wallet = Logic.WalletService.SearchView(ownerId: merchant.Id, walletTypeId: 2, page: 0).Items.FirstOrDefault();
                if (wallet == null)
                {
                    return FailedApiCall(model, "99", "No wallet found");
                }
                //get trnx
                int walletTypeid = 3;
                var items = Logic.WalletStatementService.SearchView(0, 0, 0, 0,
                    0, 0, null, "", "", "", 0, user.Id, walletTypeid, 1, 5, "-ValueDate,-Id").Items;
                var txt = "Hello,";
                if (items != null && items.Any())
                {
                    foreach (var tx in items)
                    {

                        txt += "\n" + tx.ValueDate.ToShortDateString() + " " + tx.DrCrText + " " + tx.Amount;
                    }
                }
                else
                {
                    return FailedApiCall(model, "99", "No transactions found");
                }


                model.AddData("STATEMENT", txt);
                return SuccessApiCall(model, "00", "Successful");
            }
            catch (Exception ex)
            {
                return FailedApiCall(model, "Error", ex.Message, "3");
            }
        }


        [ApiAuth]
        [HttpPost]
        [AllowAnonymous]
        [Route("MyMerchants")]
        public IHttpActionResult MyMerchants(Relay model)
        {
            try
            {
                var user = Logic.SiteUserService.GetByMobile(model.mobile);
                if(user == null)
                {
                    return FailedApiCall(model, "99", "Invalid user");
                } 
                var merchants = Logic.CorporateService.GetUserCorporates(user.Id, 2);
                var list = new List<string>();
                foreach(var merc in merchants)
                {
                    list.Add(merc.PublicName);
                } 
                model.status = "00";
                model.data = Util.SerializeJSON(list); 
                return SuccessApiCall(model, "00", "Successful");
            }
            catch (Exception ex)
            {
                return FailedApiCall(model, "Error", ex.Message, "3");
            }
        }
    }
}
