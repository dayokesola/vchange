using Bootes.Core.Common;
using Bootes.Core.Domain.Model.USSD;
using Bootes.Core.Logic.Module;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.ModelBinding;

namespace Bootes.Service.Controllers.USSD
{
    public class BaseUSSDController : ApiController
    {
        private LogicModule _module;

        /// <summary>
        ///
        /// </summary>
        public LogicModule Logic
        {
            get
            {
                if (_module == null)
                {
                    _module = new LogicModule();
                }
                return _module;
            }
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public string ModelError(ModelStateDictionary modelState)
        {
            string error = "";
            foreach (var state in modelState.Values)
            {
                foreach (var msg in state.Errors)
                {
                    error += msg.ErrorMessage + "<br />";
                }
            }
            return error;
        }

        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult ApiError(Relay model, string message)
        {
            model.status = HashStatus.CORE_COMM_ERROR;
            model.message = HashStatus.GetDescription(model.status, message);
            model.apicode = "XX";
            model.apitext = message;
            return Ok(model);
        }
     

        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult FailedApiCall(Relay model, string status, string message, string flag = "2")
        {
            model.status = HashStatus.FAILED_CONTINUE;
            model.message = flag;
            model.apicode = status;
            model.apitext = message;
            model.AddData("ERROR", message);
            model.log = message;
            return Ok(model);
        }


        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult SuccessApiCall(Relay model, string status, string message)
        {
            model.status = HashStatus.SUCCESSFUL_CONTINUE;
            model.message = "1";
            model.apicode = status;
            model.apitext = message;
            model.AddData("INFO", message);
            return Ok(model);
        }

       
    }
}