using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Account
{
    /// <summary>
    /// CorporateUsers CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Account/CorporateUsers")]
    public class CorporateUsersController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped CorporateUsers
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="userId"></param>
        /// <param name="corporateId"></param>
        /// <param name="corporateRoleId"></param>
        /// <param name="userStatusId"></param>
        /// <param name="isLockedOut"></param>
        /// <param name="authPin"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<CorporateUserModel>))]
        [Route("Search", Name = "CorporateUserApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", long? userId = 0, int? corporateId = 0,
            int? corporateRoleId = 0, int? userStatusId = 0, bool? isLockedOut = null, string authPin = "",
            long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.CorporateUserService.SearchView(userId.GetValueOrDefault(), corporateId.GetValueOrDefault(),
                    corporateRoleId.GetValueOrDefault(), userStatusId.GetValueOrDefault(), isLockedOut, authPin, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("userId", userId.GetValueOrDefault());
                jo.Add("corporateId", corporateId.GetValueOrDefault());
                jo.Add("corporateRoleId", corporateRoleId.GetValueOrDefault());
                jo.Add("userStatusId", userStatusId.GetValueOrDefault());
                jo.Add("isLockedOut", isLockedOut);
                jo.Add("authPin", authPin);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "CorporateUserApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<CorporateUserModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get CorporateUser by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(CorporateUserModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = Logic.CorporateUserService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add CorporateUser
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(CorporateUserModel))]
        public IHttpActionResult Create(CorporateUserForm form)
        {
            try
            {
                var signupForm = Logic.FactoryModule.SiteUsers.CreateSignUpForm(form);


                var msg = "";
                msg = Logic.SiteUserService.ValidateSignUp(signupForm);
                if (!string.IsNullOrEmpty(msg))
                {
                    return BadRequest(msg);
                }
                var user = Logic.FactoryModule.SiteUsers.DefaultModel();
                var signupStatus = Logic.SiteUserService.SignUpCorporate(signupForm, out user);
                switch (signupStatus)
                {
                    case SignUpStatus.Ok:
                        msg = "Sign up was successful!";
                        break;

                    case SignUpStatus.MobileExists:
                        msg = "Account Mobile already exists, please reset your account";
                        break;
                    case SignUpStatus.EmailExists:
                        msg = "Account Email already exists, please reset your account";
                        break;
                }
                if (signupStatus != SignUpStatus.Ok)
                {
                    return BadRequest(msg);
                }


                //var model = Logic.CorporateUserService.Create(form);
                //var check = Logic.CorporateUserService.CreateExists(model);
                //if (check)
                //{
                //    return BadRequest("CorporateUser already exists");
                //}
                //var dto = Logic.CorporateUserService.Insert(model);
                var dto = Logic.CorporateUserService.SearchModel(userId: user.Id).FirstOrDefault();
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update CorporateUser
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(CorporateUserModel))]
        public IHttpActionResult Update(int id, CorporateUserForm form)
        {
            try
            {
                var model = Logic.CorporateUserService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.CorporateUserService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.CorporateUserService.UpdateExists(model);
                if (Logic.CorporateUserService.UpdateExists(model))
                    return BadRequest("CorporateUser configuration already exists");
                var dto = Logic.CorporateUserService.Update(found, model,
                    "UserId,CorporateId,CorporateRoleId,UserStatusId,IsLockedOut,AuthPin,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete CorporateUser
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(CorporateUserModel))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var found = Logic.CorporateUserService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.CorporateUserService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }
}