using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Account
{

    /// <summary>
    /// EntitySettings CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Account/EntitySettings")]
    public class EntitySettingsController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped EntitySettings
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="entityType"></param>
        /// <param name="entityRef"></param>
        /// <param name="sKey"></param>
        /// <param name="sVal"></param>
        /// <param name="paramType"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<EntitySettingModel>))]
        [Route("Search", Name = "EntitySettingApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "EntityType,EntityRef,SKey", string entityType = "", long? entityRef = 0, string sKey = "", string sVal = "", string paramType = "", long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.EntitySettingService.SearchView(entityType, entityRef.GetValueOrDefault(), sKey, sVal, paramType, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("entityType", entityType);
                jo.Add("entityRef", entityRef.GetValueOrDefault());
                jo.Add("sKey", sKey);
                jo.Add("sVal", sVal);
                jo.Add("paramType", paramType);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "EntitySettingApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<EntitySettingModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get EntitySetting by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(EntitySettingModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = Logic.EntitySettingService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add EntitySetting
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(EntitySettingModel))]
        public IHttpActionResult Create(EntitySettingForm form)
        {
            try
            {
                var model = Logic.EntitySettingService.Create(form);
                var check = Logic.EntitySettingService.CreateExists(model);
                if (check)
                {
                    return BadRequest("EntitySetting already exists");
                }
                var dto = Logic.EntitySettingService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update EntitySetting
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(EntitySettingModel))]
        public IHttpActionResult Update(int id, EntitySettingForm form)
        {
            try
            {
                var model = Logic.EntitySettingService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.EntitySettingService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.EntitySettingService.UpdateExists(model);
                if (Logic.EntitySettingService.UpdateExists(model))
                    return BadRequest("EntitySetting configuration already exists");
                var dto = Logic.EntitySettingService.Update(found, model,
                    "EntityType,EntityRef,SKey,SVal,ParamType,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete EntitySetting
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(EntitySettingModel))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var found = Logic.EntitySettingService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.EntitySettingService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }

}