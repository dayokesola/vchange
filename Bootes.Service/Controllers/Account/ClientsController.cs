using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Account
{

    /// <summary>
    /// Clients CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Account/Clients")]
    public class ClientsController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped Clients
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="clientId"></param>
        /// <param name="clientSecret1"></param>
        /// <param name="clientSecret2"></param>
        /// <param name="clientTypeId"></param>
        /// <param name="clientStatusId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<ClientModel>))]
        [Route("Search", Name = "ClientApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string clientId = "", string clientSecret1 = "", string clientSecret2 = "", int clientTypeId = 0, int clientStatusId = 0, long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.ClientService.SearchView(clientId, clientSecret1, clientSecret2, clientTypeId, clientStatusId, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("clientId", clientId);
                jo.Add("clientSecret1", clientSecret1);
                jo.Add("clientSecret2", clientSecret2);
                jo.Add("clientTypeId", clientTypeId);
                jo.Add("clientStatusId", clientStatusId);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "ClientApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<ClientModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get Client by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(ClientModel))]
        public IHttpActionResult Get(long id)
        {
            try
            {
                var item = Logic.ClientService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add Client
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(ClientModel))]
        public IHttpActionResult Create(ClientForm form)
        {
            try
            {
                var model = Logic.ClientService.Create(form);
                var check = Logic.ClientService.CreateExists(model);
                if (check)
                {
                    return BadRequest("Client already exists");
                }
                var dto = Logic.ClientService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update Client
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(ClientModel))]
        public IHttpActionResult Update(long id, ClientForm form)
        {
            try
            {
                var model = Logic.ClientService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.ClientService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.ClientService.UpdateExists(model);
                if (Logic.ClientService.UpdateExists(model))
                    return BadRequest("Client configuration already exists");
                var dto = Logic.ClientService.Update(found, model,
                    "ClientId,ClientSecret1,ClientSecret2,ClientTypeId,ClientStatusId,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete Client
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(ClientModel))]
        public IHttpActionResult Delete(long id)
        {
            try
            {
                var found = Logic.ClientService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.ClientService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }

}