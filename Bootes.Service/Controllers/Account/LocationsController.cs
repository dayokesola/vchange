﻿using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Account
{


    /// <summary>
    /// Locations CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Account/Locations")]
    public class LocationsController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped Locations
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <param name="parentId"></param>
        /// <param name="locationTypeId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<LocationModel>))]
        [Route("Search", Name = "LocationApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string name = "", string code = "", string info = "", int parentId = 0, int locationTypeId = 0, long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.LocationService.SearchView(name, code, info, parentId, locationTypeId, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("name", name);
                jo.Add("code", code);
                jo.Add("info", info);
                jo.Add("parentId", parentId);
                jo.Add("locationTypeId", locationTypeId);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "LocationApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<LocationModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get Location by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(LocationModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = Logic.LocationService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add Location
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(LocationModel))]
        public IHttpActionResult Create(LocationForm form)
        {
            try
            {
                var model = Logic.LocationService.Create(form);
                var check = Logic.LocationService.CreateExists(model);
                if (check)
                {
                    return BadRequest("Location already exists");
                }
                var dto = Logic.LocationService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update Location
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(LocationModel))]
        public IHttpActionResult Update(int id, LocationForm form)
        {
            try
            {
                var model = Logic.LocationService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.LocationService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.LocationService.UpdateExists(model);
                if (Logic.LocationService.UpdateExists(model))
                    return BadRequest("Location configuration already exists");
                var dto = Logic.LocationService.Update(found, model,
                    "Name,Code,Info,ParentId,LocationTypeId,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete Location
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(LocationModel))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var found = Logic.LocationService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.LocationService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }


}