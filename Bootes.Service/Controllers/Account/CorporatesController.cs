using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Account
{


    /// <summary>
    /// Corporates CRUD
    /// </summary>
    [RoutePrefix("api/Account/Corporates")]
    public class CorporatesController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped Corporates
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="countryId"></param>
        /// <param name="regId"></param>
        /// <param name="publicName"></param>
        /// <param name="appId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<CorporateModel>))]
        [Route("Search", Name = "CorporateApi")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string name = "", string code = "", int? countryId = 0,
            string regId = "", string publicName = "", int? appId = 0, int? corporateStatus = -1,
            long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.CorporateService.SearchView(name, code, countryId.GetValueOrDefault(),
                    regId, publicName, appId.GetValueOrDefault(), corporateStatus.GetValueOrDefault(), page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("name", name);
                jo.Add("code", code);
                jo.Add("countryId", countryId.GetValueOrDefault());
                jo.Add("regId", regId);
                jo.Add("publicName", publicName);
                jo.Add("appId", appId.GetValueOrDefault());
                jo.Add("corporateStatus", corporateStatus.GetValueOrDefault());

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "CorporateApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<CorporateModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get Corporate by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(CorporateModel))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = Logic.CorporateService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("UserCorporates")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(CorporateModel))]
        public IHttpActionResult UserCorporates(long id, int? corporateStatus = -1)
        {
            try
            {
                var items = Logic.CorporateService.GetUserCorporates(id, corporateStatus.GetValueOrDefault());

                return Ok(items);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }



        /// <summary>
        /// Add Corporate
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(CorporateModel))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Create(CorporateForm form)
        {
            try
            {
                var model = Logic.CorporateService.Create(form);
                var check = Logic.CorporateService.CreateExists(model);
                if (check)
                {
                    return BadRequest("Corporate already exists");
                }
                var dto = Logic.CorporateService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("SignUp")]
        [HttpPost]
        [ResponseType(typeof(CorporateModel))] 
        public IHttpActionResult SignUp(CorporateForm form)
        {
            try
            {
                
                if (!ModelState.IsValid)
                {
                    return BadRequest(ModelError(ModelState)); 
                } 
                //for now users can only create one merchnat

                var msg = "";
                msg = Logic.CorporateService.ValidateSignUp(form);
                if (!string.IsNullOrEmpty(msg))
                {
                    return BadRequest(msg); 
                }
                var code = "";
                var signupStatus = Logic.CorporateService.SignUp(form, LoggedInUser.User, out code);
                switch (signupStatus)
                {
                    case SignUpStatus.Ok:
                        msg = "Sign up was successful!";
                        break;
                    case SignUpStatus.MobileExists:
                        msg = "Account already exists, please reset your account";
                        break;
                }
                if (signupStatus != SignUpStatus.Ok)
                {
                    return BadRequest(msg); 
                }
                return Ok(msg); 
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }



        /// <summary>
        /// Add Corporate
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Approve")]
        [HttpPost]
        [ResponseType(typeof(CorporateModel))] 
        public IHttpActionResult Approve(CorporateApprovalForm form)
        {
            try
            {
                var entity = Logic.CorporateService.Get(form.CorporateId);
                if (entity == null)
                {
                    return BadRequest("Invalid Merchant");

                }
                var verdict = form.Verdict.Trim().ToLower();

                if (verdict == "approved")
                {
                    entity.CorporateStatus = 2;
                }
                else if (verdict == "rejected")
                {
                    entity.CorporateStatus = 3;
                }
                else
                {
                    return BadRequest("Invalid Verdict");
                }

                var model = Logic.CorporateService.Update(entity);
                //alert someone later
                model = Logic.CorporateService.GetModel(form.CorporateId);
                //create wallet for merchant in base currency 
                Logic.WalletService.CreateMerchantWallet(model);
                //

                return Ok(model);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update Corporate
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(CorporateModel))]
        [ApiExplorerSettings(IgnoreApi = true)]
        public IHttpActionResult Update(int id, CorporateForm form)
        {
            try
            {
                var model = Logic.CorporateService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.CorporateService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.CorporateService.UpdateExists(model);
                if (check)
                    return BadRequest("Corporate configuration already exists");
                if (found.CorporateStatus == 0)
                {
                    //updating moves to approval stage
                    model.CorporateStatus = 1;
                }

                var dto = Logic.CorporateService.Update(found, model,
                    "Name,Code,CountryId,RegId,PublicName,AppId,RecordStatus,CorporateStatus");

                //We store tags here
                if (!string.IsNullOrEmpty(form.Tags))
                {
                    Logic.TagService.MerchantUpdateOrAdd(dto, form.Tags);
                }
                //refresh info
                dto = Logic.CorporateService.GetModel(id);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete Corporate
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(CorporateModel))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var found = Logic.CorporateService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.CorporateService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }
}