﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Account
{
    /// <summary>
    /// Apps CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Account/Apps")]
    public class AppsController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped Apps
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="runtimeStatusId"></param>
        /// <param name="availableAt"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<AppModel>))]
        [Route("Search", Name = "AppApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string name = "", string code = "", int? runtimeStatusId = 0, DateTime? availableAt = null, 
            long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.AppService.SearchView(name, code, runtimeStatusId.GetValueOrDefault(), availableAt, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("name", name);
                jo.Add("code", code);
                jo.Add("runtimeStatusId", runtimeStatusId.GetValueOrDefault());
                jo.Add("availableAt", availableAt);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "AppApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<AppModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get App by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(AppModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = Logic.AppService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add App
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(AppModel))]
        public IHttpActionResult Create(AppForm form)
        {
            try
            {
                var model = Logic.AppService.Create(form);
                var check = Logic.AppService.CreateExists(model);
                if (check)
                {
                    return BadRequest("App already exists");
                }
                var dto = Logic.AppService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update App
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(AppModel))]
        public IHttpActionResult Update(int id, AppForm form)
        {
            try
            {
                var model = Logic.AppService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.AppService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.AppService.UpdateExists(model);
                if (Logic.AppService.UpdateExists(model))
                    return BadRequest("App configuration already exists");
                var dto = Logic.AppService.Update(found, model,
                    "Name,Code,RuntimeStatusId,AvailableAt,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete App
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(AppModel))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var found = Logic.AppService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.AppService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }







}
