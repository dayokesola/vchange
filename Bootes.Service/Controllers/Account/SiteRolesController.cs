using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Account
{

    /// <summary>
    /// SiteRoles CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Account/SiteRoles")]
    public class SiteRolesController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped SiteRoles
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="departmentId"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="info"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<SiteRoleModel>))]
        [Route("Search", Name = "SiteRoleApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string name = "", string code = "", int? departmentId = 0,
            int? roleTypeId = 0, string info = "", long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.SiteRoleService.SearchView(name, code, departmentId.GetValueOrDefault(), roleTypeId.GetValueOrDefault(), info, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("name", name);
                jo.Add("code", code);
                jo.Add("departmentId", departmentId.GetValueOrDefault());
                jo.Add("roleTypeId", roleTypeId.GetValueOrDefault());
                jo.Add("info", info);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "SiteRoleApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<SiteRoleModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get SiteRole by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(SiteRoleModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = Logic.SiteRoleService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add SiteRole
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(SiteRoleModel))]
        public IHttpActionResult Create(SiteRoleForm form)
        {
            try
            {
                var model = Logic.SiteRoleService.Create(form);
                var check = Logic.SiteRoleService.CreateExists(model);
                if (check)
                {
                    return BadRequest("SiteRole already exists");
                }
                var dto = Logic.SiteRoleService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update SiteRole
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(SiteRoleModel))]
        public IHttpActionResult Update(int id, SiteRoleForm form)
        {
            try
            {
                var model = Logic.SiteRoleService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.SiteRoleService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.SiteRoleService.UpdateExists(model);
                if (Logic.SiteRoleService.UpdateExists(model))
                    return BadRequest("SiteRole configuration already exists");
                var dto = Logic.SiteRoleService.Update(found, model,
                    "Name,Code,DepartmentId,RoleTypeId,Info,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete SiteRole
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(SiteRoleModel))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var found = Logic.SiteRoleService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.SiteRoleService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }




}