using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Account
{
    /// <summary>
    /// CorporateRoles CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Account/CorporateRoles")]
    public class CorporateRolesController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped CorporateRoles
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="name"></param>
        /// <param name="corporateId"></param>
        /// <param name="isAdmin"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<CorporateRoleModel>))]
        [Route("Search", Name = "CorporateRoleApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string name = "", int corporateId = 0, bool? isAdmin = null, int roleTypeId = 0, long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.CorporateRoleService.SearchView(name, corporateId, isAdmin, roleTypeId, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("name", name);
                jo.Add("corporateId", corporateId);
                jo.Add("isAdmin", isAdmin);
                jo.Add("roleTypeId", roleTypeId);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "CorporateRoleApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<CorporateRoleModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get CorporateRole by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(CorporateRoleModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = Logic.CorporateRoleService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add CorporateRole
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(CorporateRoleModel))]
        public IHttpActionResult Create(CorporateRoleForm form)
        {
            try
            {
                var model = Logic.CorporateRoleService.Create(form);
                var check = Logic.CorporateRoleService.CreateExists(model);
                if (check)
                {
                    return BadRequest("CorporateRole already exists");
                }
                var dto = Logic.CorporateRoleService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update CorporateRole
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(CorporateRoleModel))]
        public IHttpActionResult Update(int id, CorporateRoleForm form)
        {
            try
            {
                var model = Logic.CorporateRoleService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.CorporateRoleService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.CorporateRoleService.UpdateExists(model);
                if (Logic.CorporateRoleService.UpdateExists(model))
                    return BadRequest("CorporateRole configuration already exists");
                var dto = Logic.CorporateRoleService.Update(found, model,
                    "Name,CorporateId,IsAdmin,RoleTypeId,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete CorporateRole
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(CorporateRoleModel))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var found = Logic.CorporateRoleService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.CorporateRoleService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }
}