using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Accounts;
using Bootes.Core.Domain.Model.Account;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bootes.Service.Controllers.Account
{

    [RoutePrefix("api/Accounts")]
    public class AccountsController : BaseApiController
    {
        /// <summary>
        /// Sign Up 
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("SignUp")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [AllowAnonymous]
        public IHttpActionResult SignUp(SignUpForm form)
        {
            try
            {
                //we need to validate the signup form
                var verdict = Logic.SiteUserService.ValidateSignUp(form);
                if (!string.IsNullOrEmpty(verdict))
                {
                    return BadRequest(verdict);
                }

                var user = Logic.FactoryModule.SiteUsers.DefaultModel();
                var signupStatus = Logic.SiteUserService.SignUp(form, out user);
                var msg = "";
                switch (signupStatus)
                {
                    case SignUpStatus.Ok:
                        msg = "Sign up was successful, please continue to login";
                        break;
                    case SignUpStatus.MobileExists:
                        msg = "Account already exists, please reset your account";
                        break;
                }
                if (signupStatus == SignUpStatus.Ok)
                {
                    return Ok(msg);
                }
                else
                {
                    return BadRequest(msg);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }


        [Route("SignIn")]
        [HttpPost]
        [ResponseType(typeof(string))]
        [AllowAnonymous]
        public IHttpActionResult SignIn(SignInForm form)
        {
            try
            {
                var user = Logic.FactoryModule.SiteUsers.DefaultModel();
                var msg = "";
                msg = Logic.SiteUserService.ValidateSignIn(form);
                if (msg == "undef")
                {
                    return BadRequest("Sign in mode could not be determined");
                }
                var signinStatus = Logic.SiteUserService.SignIn(form, msg, out user);
                switch (signinStatus)
                {
                    case SignInStatus.Ok:
                        msg = "Sign In was successful!";
                        break;
                    case SignInStatus.UserLockedOut:
                        msg = "Your account is locked out, contact administrator";
                        break;
                    default:
                        msg = "Invalid Credentials";
                        break;
                }
                if (signinStatus != SignInStatus.Ok)
                {
                    return Content(System.Net.HttpStatusCode.Unauthorized, msg);
                }
                var token = new
                {
                    usertoken = DoLogin(user)
                };
                return Ok(token);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }



        protected string DoLogin(SiteUserModel user)
        {
            var newIdentity = new ClaimsIdentity("Cookies", ClaimTypes.Email, ClaimTypes.Role);
            newIdentity.AddClaim(new Claim(ClaimTypes.GivenName, user.FirstName));
            newIdentity.AddClaim(new Claim(ClaimTypes.Surname, user.LastName));
            newIdentity.AddClaim(new Claim(ClaimTypes.Name, user.GetName()));
            newIdentity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
            newIdentity.AddClaim(new Claim(ClaimTypes.MobilePhone, user.Mobile));
            newIdentity.AddClaim(new Claim(ClaimTypes.Role, "Subscriber"));
            newIdentity.AddClaim(new Claim("id", user.Id.ToString()));
            newIdentity.AddClaim(new Claim("unique_user_key", "bootes_web_" + user.Id)); 
            var liu = Logic.FactoryModule.SiteUsers.Profile(user);
            bool useSelector = false; 
            var accounts = Logic.CorporateUserService.SearchView(userId: user.Id, userStatusId: 1, isLockedOut: false).Items;

            var lst = new List<string>();
            lst.Add("Subscriber");
            if (accounts != null)
            {
                if (accounts.Count() > 0)
                {
                    liu.AddCorporateUserAccounts(accounts);
                    if (accounts.Count() > 1)
                    {
                        useSelector = true;
                    }
                    foreach (var acct in accounts)
                    {
                        if (!lst.Contains(acct.CorporateRoleTypeName))
                        {
                            newIdentity.AddClaim(new Claim(ClaimTypes.Role, acct.CorporateRoleTypeName));
                        }
                    }
                }
            }
            

            liu.UseSelector = useSelector;
            newIdentity.AddClaim(new Claim("profile", Util.SerializeJSON(liu))); 
            return Logic.SiteUserService.CreateToken(newIdentity); 
        }
    }

}