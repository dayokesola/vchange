using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Account
{

    /// <summary>
    /// Permissions CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Account/Permissions")]
    public class PermissionsController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped Permissions
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<PermissionModel>))]
        [Route("Search", Name = "PermissionApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string name = "", string code = "", string info = "", long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.PermissionService.SearchView(name, code, info, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("name", name);
                jo.Add("code", code);
                jo.Add("info", info);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "PermissionApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<PermissionModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get Permission by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(PermissionModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = Logic.PermissionService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add Permission
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(PermissionModel))]
        public IHttpActionResult Create(PermissionForm form)
        {
            try
            {
                var model = Logic.PermissionService.Create(form);
                var check = Logic.PermissionService.CreateExists(model);
                if (check)
                {
                    return BadRequest("Permission already exists");
                }
                var dto = Logic.PermissionService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update Permission
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(PermissionModel))]
        public IHttpActionResult Update(int id, PermissionForm form)
        {
            try
            {
                var model = Logic.PermissionService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.PermissionService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.PermissionService.UpdateExists(model);
                if (Logic.PermissionService.UpdateExists(model))
                    return BadRequest("Permission configuration already exists");
                var dto = Logic.PermissionService.Update(found, model,
                    "Name,Code,Info,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete Permission
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(PermissionModel))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var found = Logic.PermissionService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.PermissionService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }

}