using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.UI;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;
using Bootes.Core.Domain.Enum;

namespace Bootes.Service.Controllers.Account
{


    /// <summary>
    /// SiteUsers CRUD
    /// </summary> 
    [RoutePrefix("api/Account/SiteUsers")] 
    public class SiteUsersController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped SiteUsers
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="userName"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="password"></param>
        /// <param name="pIN"></param>
        /// <param name="token"></param>
        /// <param name="appId"></param>
        /// <param name="passwordChangedAt"></param>
        /// <param name="userStatusId"></param>
        /// <param name="mobileValidated"></param>
        /// <param name="emailValidated"></param>
        /// <param name="isLookedOut"></param>
        /// <param name="userStamp"></param>
        /// <param name="countryId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
    [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(IEnumerable<SiteUserModel>))]
        [Route("Search", Name = "SiteUserApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string userName = "", string firstName = "", string lastName = "",
            string email = "", string mobile = "",
            string token = "", int appId = 0, 
            UserStatus? userStatusId = null, bool? mobileValidated = null, bool? emailValidated = null, 
            bool? isLookedOut = null, string userStamp = "", int countryId = 0,
            long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.SiteUserService.SearchView(userName, firstName, lastName, email, mobile, 
                    token, appId, userStatusId, mobileValidated, emailValidated, isLookedOut, userStamp,
                    countryId, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("userName", userName);
                jo.Add("firstName", firstName);
                jo.Add("lastName", lastName);
                jo.Add("email", email);
                jo.Add("mobile", mobile);
                jo.Add("token", token);
                jo.Add("appId", appId); 
                jo.Add("userStatusId", userStatusId.ToString());
                jo.Add("mobileValidated", mobileValidated);
                jo.Add("emailValidated", emailValidated);
                jo.Add("isLookedOut", isLookedOut);
                jo.Add("userStamp", userStamp);
                jo.Add("countryId", countryId);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "SiteUserApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<SiteUserModel>();
                if (items.TotalItems <= 0) return Ok(dto);

                foreach(var i in items.Items)
                {
                    i.Hide();
                    dto.Add(i);
                } 
                var dtos = dto.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get SiteUser by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(SiteUserModel))]
        public IHttpActionResult Get(long id)
        {
            try
            {
                var item = Logic.SiteUserService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                item.Hide();
             
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add SiteUser
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
    [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(SiteUserModel))]
        public IHttpActionResult Create(SiteUserForm form)
        {
            try
            {
                var model = Logic.SiteUserService.Create(form);
                var check = Logic.SiteUserService.CreateExists(model);
                if (check)
                {
                    return BadRequest("SiteUser already exists");
                }
                var dto = Logic.SiteUserService.Insert(model);
                dto.Hide();
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update SiteUser
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
    [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(SiteUserModel))]
        public IHttpActionResult Update(long id, SiteUserForm form)
        {
            try
            {
                var model = Logic.SiteUserService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.SiteUserService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.SiteUserService.UpdateExists(model);
                if (Logic.SiteUserService.UpdateExists(model))
                    return BadRequest("SiteUser configuration already exists");
                var dto = Logic.SiteUserService.Update(found, model,
                    "UserName,FirstName,LastName,Email,Mobile,Password,PIN,Token,AppId,PasswordChangedAt,UserStatusId,MobileValidated,EmailValidated,IsLookedOut,UserStamp,CountryId,RecordStatus");

                dto.Hide();
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete SiteUser
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
    [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(SiteUserModel))]
        public IHttpActionResult Delete(long id)
        {
            try
            {
                var found = Logic.SiteUserService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.SiteUserService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get SiteUser by email
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Mobile")]
        [ResponseType(typeof(SiteUserModel))]
        public IHttpActionResult Mobile(string id)
        {
            try
            {
                var item = Logic.SiteUserService.SearchView(mobile:id).Items.FirstOrDefault();
                if (item == null)
                {
                    return NotFound();
                }
                item.Hide();
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Stamp")]
        [ResponseType(typeof(SiteUserModel))]
        public IHttpActionResult Stamp(string id)
        {
            try
            {
                var item = Logic.SiteUserService.SearchView(userStamp: id).Items.FirstOrDefault();
                if (item == null)
                {
                    return NotFound();
                }
                item.Hide();
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpGet]
        [Route("Email")]
        [ResponseType(typeof(SiteUserModel))]
        public IHttpActionResult Email(string id)
        {
            try
            {
                var item = Logic.SiteUserService.SearchView(email: id).Items.FirstOrDefault();
                if (item == null)
                {
                    return NotFound();
                }
                item.Hide();
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }


}