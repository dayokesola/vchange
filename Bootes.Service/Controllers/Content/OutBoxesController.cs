using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Content;
using Bootes.Core.Domain.Model.Content;
using Bootes.Core.UI;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Content
{

    /// <summary>
    /// OutBoxes CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Content/OutBoxes")]
    public class OutBoxesController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped OutBoxes
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="toAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="messageCode"></param>
        /// <param name="messageTypeId"></param>
        /// <param name="scheduledAt"></param>
        /// <param name="contents"></param>
        /// <param name="attachments"></param>
        /// <param name="isSent"></param>
        /// <param name="sentAt"></param>
        /// <param name="sentRemarks"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<OutBoxModel>))]
        [Route("Search", Name = "OutBoxApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string toAddress = "", string fromAddress = "", string subject = "", string messageCode = "", int messageTypeId = 0, DateTime? scheduledAt = null, string contents = "", string attachments = "", bool? isSent = null, DateTime? sentAt = null, string sentRemarks = "", long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.OutBoxService.SearchView(toAddress, fromAddress, subject, messageCode, messageTypeId, scheduledAt, contents, attachments, isSent, sentAt, sentRemarks, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("toAddress", toAddress);
                jo.Add("fromAddress", fromAddress);
                jo.Add("subject", subject);
                jo.Add("messageCode", messageCode);
                jo.Add("messageTypeId", messageTypeId);
                jo.Add("scheduledAt", scheduledAt);
                jo.Add("contents", contents);
                jo.Add("attachments", attachments);
                jo.Add("isSent", isSent);
                jo.Add("sentAt", sentAt);
                jo.Add("sentRemarks", sentRemarks);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "OutBoxApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<OutBoxModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get OutBox by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(OutBoxModel))]
        public IHttpActionResult Get(long id)
        {
            try
            {
                var item = Logic.OutBoxService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add OutBox
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(OutBoxModel))]
        public IHttpActionResult Create(OutBoxForm form)
        {
            try
            {
                var model = Logic.OutBoxService.Create(form);
                var check = Logic.OutBoxService.CreateExists(model);
                if (check)
                {
                    return BadRequest("OutBox already exists");
                }
                var dto = Logic.OutBoxService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update OutBox
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(OutBoxModel))]
        public IHttpActionResult Update(long id, OutBoxForm form)
        {
            try
            {
                var model = Logic.OutBoxService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.OutBoxService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.OutBoxService.UpdateExists(model);
                if (Logic.OutBoxService.UpdateExists(model))
                    return BadRequest("OutBox configuration already exists");
                var dto = Logic.OutBoxService.Update(found, model,
                    "ToAddress,FromAddress,Subject,MessageCode,MessageTypeId,ScheduledAt,Contents,Attachments,IsSent,SentAt,SentRemarks,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete OutBox
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(OutBoxModel))]
        public IHttpActionResult Delete(long id)
        {
            try
            {
                var found = Logic.OutBoxService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.OutBoxService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }
}