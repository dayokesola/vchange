﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Form.Content;
using Bootes.Core.Domain.Model.Content;
using Bootes.Core.UI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.Content 
{


    /// <summary>
    /// Articles CRUD
    /// </summary>
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Content/Articles")]
    public class ArticlesController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped Articles
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="name"></param>
        /// <param name="title"></param>
        /// <param name="tags"></param>
        /// <param name="content"></param>
        /// <param name="editorType"></param>
        /// <param name="altContents"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<ArticleModel>))]
        [Route("Search", Name = "ArticleApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string name = "", string title = "", string tags = "", string content = "", int editorType = 0, string altContents = "", long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.ArticleService.SearchView(name, title, tags, content, editorType, altContents, page, pageSize, sort);
 


                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("name", name);
                jo.Add("title", title);
                jo.Add("tags", tags);
                jo.Add("content", content);
                jo.Add("editorType", editorType);
                jo.Add("altContents", altContents);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "ArticleApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<ArticleModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get Article by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(ArticleModel))]
        public IHttpActionResult Get(int id)
        {
            try
            {
                var item = Logic.ArticleService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add Article
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ResponseType(typeof(ArticleModel))]
        public IHttpActionResult Create(ArticleForm form)
        {
            try
            {
                var model = Logic.ArticleService.Create(form);
                var check = Logic.ArticleService.CreateExists(model);
                if (check)
                {
                    return BadRequest("Article already exists");
                }
                var dto = Logic.ArticleService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update Article
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(ArticleModel))]
        public IHttpActionResult Update(int id, ArticleForm form)
        {
            try
            {
                var model = Logic.ArticleService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.ArticleService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.ArticleService.UpdateExists(model);
                if (Logic.ArticleService.UpdateExists(model))
                    return BadRequest("Article configuration already exists");
                var dto = Logic.ArticleService.Update(found, model,
                    "Name,Title,Tags,Content,EditorType,AltContents,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete Article
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(ArticleModel))]
        public IHttpActionResult Delete(int id)
        {
            try
            {
                var found = Logic.ArticleService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.ArticleService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }







}
