using System;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Domain.Model.Content;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bootes.Service.Controllers.Content
{
    [ApiExplorerSettings(IgnoreApi = true)]
    [RoutePrefix("api/Content/Tags")]
    public class TagsController: BaseApiController
    {
        [ResponseType(typeof(IEnumerable<TagFrequency>))]
        [Route("Frequencies")]
        [HttpGet]
        public IHttpActionResult Frequencies(string objectType)
        {
            try
            { 
                return Ok(Logic.TagService.TagFrequencies(objectType));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }
}