using Bootes.Core.Common;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model.MyChange;
using Bootes.Core.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.MyChange
{
    /// <summary>
    /// WalletTransacts CRUD
    /// </summary>
    [RoutePrefix("api/MyChange/WalletTransacts")]
    public class WalletTransactsController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped WalletTransacts
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="drWalletId"></param>
        /// <param name="crWalletId"></param>
        /// <param name="amount"></param>
        /// <param name="currencyId"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="initId"></param>
        /// <param name="initDate"></param>
        /// <param name="authId"></param>
        /// <param name="authDate"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactCodeId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<WalletTransactModel>))]
        [Route("Search", Name = "WalletTransactApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", long drWalletId = 0, long crWalletId = 0,
            decimal amount = 0, int currencyId = 0, DateTime? valueDate = null,
            string paymentReference = "", string narration = "", long initId = 0, DateTime? initDate = null,
            long authId = 0, DateTime? authDate = null, string sourceId = "", int transactCodeId = 0,
            string txId = "", string gatewayRef = "", int authStatusId = 0,
            long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.WalletTransactService.SearchView(drWalletId, crWalletId, amount, currencyId, valueDate, paymentReference,
                    narration, initId, initDate, authId, authDate, sourceId, transactCodeId, txId, gatewayRef, authStatusId, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("drWalletId", drWalletId);
                jo.Add("crWalletId", crWalletId);
                jo.Add("amount", amount);
                jo.Add("currencyId", currencyId);
                jo.Add("valueDate", valueDate);
                jo.Add("paymentReference", paymentReference);
                jo.Add("narration", narration);
                jo.Add("initId", initId);
                jo.Add("initDate", initDate);
                jo.Add("authId", authId);
                jo.Add("authDate", authDate);
                jo.Add("sourceId", sourceId);
                jo.Add("transactCodeId", transactCodeId);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "WalletTransactApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<WalletTransactModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Get WalletTransact by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(WalletTransactModel))]
        public IHttpActionResult Get(long id)
        {
            try
            {
                var item = Logic.WalletTransactService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add WalletTransact
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
    [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(WalletTransactModel))]
        public IHttpActionResult Create(WalletTransactForm form)
        {
            try
            {
                var model = Logic.WalletTransactService.Create(form);
                var check = Logic.WalletTransactService.CreateExists(model);
                if (check)
                {
                    return BadRequest("WalletTransact already exists");
                }
                var dto = Logic.WalletTransactService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        [HttpPost]
        [Route("InitiateFund")]
        [ResponseType(typeof(WalletPayForm))]
        public IHttpActionResult InitiateFund(WalletPayForm form)
        {
            try
            {
                var wallet = Logic.WalletService.GetModel(form.WalletId);
                if (wallet == null)
                {
                    throw new Exception("Invalid Wallet selected");
                }
                form.WalletCurrencyId = wallet.CurrencyId;
                form.TransactionCodeId = 1; //Fund  
                var tx = Logic.FactoryModule.WalletTransacts.CreateModel(form);
                //set dr wallet which is system
                var syswallet = Logic.WalletService.GetSystemWallet(tx.CurrencyId, tx.TransactCodeId);
                tx.DrWalletId = syswallet.Id;
                tx.InitId = LoggedInUser.User.Id;
                //log transaction to db 
                tx = Logic.WalletTransactService.Insert(tx);
                form.TxId = tx.TxId;
                return Ok(form);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update WalletTransact
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ResponseType(typeof(WalletTransactModel))]
        public IHttpActionResult Update(long id, WalletTransactForm form)
        {
            try
            {
                var model = Logic.WalletTransactService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.WalletTransactService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.WalletTransactService.UpdateExists(model);
                if (Logic.WalletTransactService.UpdateExists(model))
                    return BadRequest("WalletTransact configuration already exists");
                var dto = Logic.WalletTransactService.Update(found, model,
                    "DrWalletId,CrWalletId,Amount,CurrencyId,ValueDate,PaymentReference,Narration,InitId,InitDate,AuthId,AuthDate,SourceId,TransactCodeId,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete WalletTransact
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [HttpPost]
        [ResponseType(typeof(WalletTransactModel))]
        public IHttpActionResult Delete(long id)
        {
            try
            {
                var found = Logic.WalletTransactService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.WalletTransactService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("UserCancel")]
        [HttpGet]
        [ResponseType(typeof(string))]
        public IHttpActionResult UserCancel(string id)
        {
            try
            {
                var found = Logic.WalletTransactService.SearchView(txId: id, page: 0).Items.FirstOrDefault();
                if (found == null)
                    return NotFound();

                if(!Logic.WalletTransactService.IsCancellable(found))
                {
                    return NotFound();
                }

                var e = Logic.WalletTransactService.Get(found.Id);

                e.AuthDate = Util.CurrentDateTime();
                e.AuthId = LoggedInUser.User.Id;
                e.AuthStatusId = 8; //user cancelled
                Logic.WalletTransactService.Update(e);
                return Ok("Transaction cancelled successfully");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("ForwardPayment")]
        [HttpPost]
        [ResponseType(typeof(string))]
        public IHttpActionResult ForwardPayment(WalletPayForm form)
        {
            try
            {
                var found = Logic.WalletTransactService.SearchView(txId: form.TxId, authStatusId: 1, page: 0).Items.FirstOrDefault();
                if (found == null)
                    return NotFound();


                var e = Logic.WalletTransactService.Get(found.Id);

                e.AuthDate = Util.CurrentDateTime();
                e.AuthId = LoggedInUser.User.Id;
                e.AuthStatusId = 2; //forwarding to gateway
                Logic.WalletTransactService.Update(e);
                return Ok("Transaction flagged as forwarded");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("Revalidate")]
        [HttpPost]
        [ResponseType(typeof(string))]
        public IHttpActionResult Revalidate(WalletPayForm form)
        {
            try
            {
                var found = Logic.WalletTransactService.SearchView(txId: form.TxId, authStatusId: 2, page: 0).Items.FirstOrDefault();
                if (found == null)
                    return NotFound();

                var e = Logic.WalletTransactService.Get(found.Id);
                e.GateWayReference = form.GatewayRef;
                e.AuthDate = Util.CurrentDateTime();
                e.AuthId = LoggedInUser.User.Id;
                e.AuthStatusId = 3; //calledback
                Logic.WalletTransactService.Update(e);

                //
                return Ok("Transaction callback received successfully");
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
         
        [Route("PostTransact")]
        [HttpPost]
        [ResponseType(typeof(int))]
        public IHttpActionResult PostTransact(int id)
        {
            try
            { 
                return Ok(Logic.WalletTransactService.Post(id));
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }


        [Route("GiveChange")]
        [HttpPost]
        [ResponseType(typeof(WalletTransactModel))]
        public IHttpActionResult GiveChange(GiveChangeForm form)
        {
            try
            {
                form.AuthId = LoggedInUser.User.Id;
                form.TransactionCodeId = 3;
                var model = Logic.WalletTransactService.GiveChange(form);
                if (model.AuthStatusId == 6)
                {
                    return Ok(model);
                }
                else
                {
                    return BadRequest("Transaction failed: " + model.AuthStatusId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }


        [Route("RequestChange")]
        [HttpPost]
        [ResponseType(typeof(WalletTransactModel))]
        public IHttpActionResult RequestChange(RequestChangeForm form)
        {
            try
            { 
                form.TransactionCodeId = 3;
                var model = Logic.WalletTransactService.RequestChange(form);
                if (model.AuthStatusId == 6)
                {
                    return Ok(model);
                }
                else
                {
                    return BadRequest("Transaction failed: " + model.AuthStatusId);
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }
}