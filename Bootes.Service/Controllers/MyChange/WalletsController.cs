﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model.MyChange;
using Bootes.Core.UI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.MyChange
{
    /// <summary>
    /// Wallets CRUD
    /// </summary>
    [RoutePrefix("api/MyChange/Wallets")]
    public class WalletsController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped Wallets
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="walletTypeId"></param>
        /// <param name="currentBalance"></param>
        /// <param name="openingBalance"></param>
        /// <param name="balanceLimit"></param>
        /// <param name="heldBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="ownerId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(IEnumerable<WalletModel>))]
        [Route("Search", Name = "WalletApi")]
        [HttpGet]
        public IHttpActionResult Get(string sort = "id", string name = "", string code = "", int walletTypeId = 0, decimal currentBalance = 0, decimal openingBalance = 0, decimal balanceLimit = 0, decimal heldBalance = 0, int currencyId = 0, int ownerId = 0, long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.WalletService.SearchView(name, code, walletTypeId, currentBalance, openingBalance, balanceLimit, heldBalance, currencyId, ownerId, page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("name", name);
                jo.Add("code", code);
                jo.Add("walletTypeId", walletTypeId);
                jo.Add("currentBalance", currentBalance);
                jo.Add("openingBalance", openingBalance);
                jo.Add("balanceLimit", balanceLimit);
                jo.Add("heldBalance", heldBalance);
                jo.Add("currencyId", currencyId);
                jo.Add("ownerId", ownerId);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "WalletApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<WalletModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }



        /// <summary>
        /// Get Wallet by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(WalletModel))]
        public IHttpActionResult Get(long id)
        {
            try
            {
                var item = Logic.WalletService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Add Wallet
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Create")]
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(WalletModel))]
        public IHttpActionResult Create(WalletForm form)
        {
            try
            {
                var model = Logic.WalletService.Create(form);
                var check = Logic.WalletService.CreateExists(model);
                if (check)
                {
                    return BadRequest("Wallet already exists");
                }
                var dto = Logic.WalletService.Insert(model);
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Update Wallet
        /// </summary>
        /// <param name="id"></param>
        /// <param name="form"></param>
        /// <returns></returns>
        [Route("Update")]
        [HttpPost]
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(WalletModel))]
        public IHttpActionResult Update(long id, WalletForm form)
        {
            try
            {
                var model = Logic.WalletService.Create(form);
                if (id != model.Id)
                    return BadRequest("Route Parameter does mot match model ID");
                var found = Logic.WalletService.Get(id);
                if (found == null)
                    return NotFound();
                var check = Logic.WalletService.UpdateExists(model);
                if (Logic.WalletService.UpdateExists(model))
                    return BadRequest("Wallet configuration already exists");
                var dto = Logic.WalletService.Update(found, model,
                    "Name,RecordStatus");
                return Ok(dto);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Delete Wallet
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Route("Delete")]
        [ApiExplorerSettings(IgnoreApi = true)]
        [HttpPost]
        [ResponseType(typeof(WalletModel))]
        public IHttpActionResult Delete(long id)
        {
            try
            {
                var found = Logic.WalletService.Get(id);
                if (found == null)
                    return NotFound();
                Logic.WalletService.Delete(found);
                return Content(HttpStatusCode.NoContent, found);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }

        [Route("Merchant")]
        [HttpGet]
        [ResponseType(typeof(List<WalletModel>))]
        public IHttpActionResult Merchant(long id)
        {
            try
            {
                var wallets = Logic.WalletService.SearchView(walletTypeId: 2, ownerId: id, page: 0).Items;
                return Ok(wallets);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }
}