using Bootes.Core.Common;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model.MyChange;
using Bootes.Core.UI;
using System;
using System.Collections.Generic;
using System.Net;
using System.Web.Http;
using System.Web.Http.Description;
using System.Web.Http.Routing;

namespace Bootes.Service.Controllers.MyChange
{
/// <summary>
    /// WalletStatements CRUD
    /// </summary>
    [RoutePrefix("api/MyChange/WalletStatements")]
    public class WalletStatementsController : BaseApiController
    {
        /// <summary>
        /// Search, Page, filter and Shaped WalletStatements
        /// </summary>
        /// <param name="sort"></param>
        /// <param name="walletTransactId"></param>
        /// <param name="walletId"></param>
        /// <param name="amount"></param>
        /// <param name="currentBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="drCr"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactionCodeId"></param>
        /// <param name="page"></param>
        /// <param name="pageSize"></param>
        /// <param name="fields"></param>
        /// <param name="draw"></param>
        /// <returns></returns>
        [ResponseType(typeof(IEnumerable<WalletStatementModel>))]
        [Route("Search", Name = "WalletStatementApi")]
        [AllowAnonymous]
        [HttpGet]
        public IHttpActionResult Get(string sort = "-id", long walletTransactId = 0, long walletId = 0, decimal amount = 0, 
            decimal currentBalance = 0, int currencyId = 0, int drCr = 0, DateTime? valueDate = null, string paymentReference = "", 
            string narration = "", string sourceId = "", int transactionCodeId = 0, long ownerId = 0, int walletTypeId = 0,
            long page = 1, long pageSize = 10, string fields = "", int draw = 1)
        {
            try
            {
                var items = Logic.WalletStatementService.SearchView(walletTransactId, walletId, amount, currentBalance, 
                    currencyId, drCr, valueDate, paymentReference, narration, sourceId, transactionCodeId, ownerId, walletTypeId,
                    page, pageSize, sort);

                if (page > items.TotalPages) page = items.TotalPages;
                var jo = new JObjectHelper();
                jo.Add("walletTransactId", walletTransactId);
                jo.Add("walletId", walletId);
                jo.Add("amount", amount);
                jo.Add("currentBalance", currentBalance);
                jo.Add("currencyId", currencyId);
                jo.Add("drCr", drCr);
                jo.Add("valueDate", valueDate);
                jo.Add("paymentReference", paymentReference);
                jo.Add("narration", narration);
                jo.Add("sourceId", sourceId);
                jo.Add("transactionCodeId", transactionCodeId);
                jo.Add("ownerId", ownerId);
                jo.Add("walletTypeId", walletTypeId);

                jo.Add("fields", fields);
                jo.Add("sort", sort);
                var urlHelper = new UrlHelper(Request);
                var linkBuilder = new PageLinkBuilder(urlHelper, "WalletStatementApi", jo, page, pageSize, items.TotalItems, draw);
                AddHeader("X-Pagination", linkBuilder.PaginationHeader);
                var dto = new List<WalletStatementModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }



        /// <summary>
        /// Get WalletStatement by ID
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet]
        [Route("Detail")]
        [ResponseType(typeof(WalletStatementModel))]
        public IHttpActionResult Get(long id)
        {
            try
            {
                var item = Logic.WalletStatementService.GetModel(id);
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
         
        [ResponseType(typeof(IEnumerable<WalletStatementModel>))]
        [Route("Merchant")]
        [HttpGet]
        public IHttpActionResult Merchant(long Id = 0, int records = 10, string fields = "")
        {
            try
            {
                int walletTypeid = 2;
                var items = Logic.WalletStatementService.SearchView(0, 0, 0, 0,
                    0, 0, null, "", "", "", 0, Id, walletTypeid, 1, records, "-ValueDate,-Id");
                  
                var dto = new List<WalletStatementModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
         
        [ResponseType(typeof(IEnumerable<WalletStatementModel>))]
        [Route("Subscriber")]
        [HttpGet]
        public IHttpActionResult Subscriber(long Id = 0, int records = 10, string fields = "")
        {
            try
            {
                int walletTypeid = 3;
                var items = Logic.WalletStatementService.SearchView(0, 0, 0, 0,
                    0, 0, null, "", "", "", 0, Id, walletTypeid, 1, records, "-ValueDate,-Id");

                var dto = new List<WalletStatementModel>();
                if (items.TotalItems <= 0) return Ok(dto);
                var dtos = items.Items.ShapeList(fields);
                return Ok(dtos);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }
}