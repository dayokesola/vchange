using Bootes.Core.Common;
using Bootes.Core.Domain.Model.MyChange;
using System;
using System.Web.Http;
using System.Web.Http.Description;

namespace Bootes.Service.Controllers.MyChange
{
    [RoutePrefix("api/MyChange/Dashboard")]
    public class DashboardController : BaseApiController
    {
        [HttpGet]
        [Route("Home")]
        [AllowAnonymous]
        [ApiExplorerSettings(IgnoreApi = true)]
        [ResponseType(typeof(DashboardHomeModel))]
        public IHttpActionResult Home()
        {
            try
            {
                var item = Logic.DashboardService.HomePage();
                if (item == null)
                {
                    return NotFound();
                }
                return Ok(item);
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return BadRequest(ex.Message);
            }
        }
    }
}