﻿using Bootes.Core.Common;
using Bootes.Core.Logic.Module;
using System.Linq;
using System.Threading;
using System.Web.Mvc;
using System.Web.Routing;

namespace Bootes.Web.Helpers
{
    public class CustomAuthAttribute : ActionFilterAttribute
    { 
        public string Permissions { get; set; }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            string permlist = Permissions;
            if (string.IsNullOrEmpty(permlist))
            {
                base.OnActionExecuting(filterContext);
            }
            var logic = new LogicModule();

            var userprofile = logic.FactoryModule.SiteUsers.Profile(); 
            if (userprofile.Permissions == null)
            { 
                //user needs to re-login
                filterContext.Result = new RedirectToRouteResult(
                       new RouteValueDictionary(
                           new
                           {
                               controller = "Accounts",
                               action = "LogOff",
                               id = 902
                           })
                       );
                return;
            }

            string[] bits = Util.Explode(permlist);
            bool isOk = false;
            if (userprofile.Permissions != null)
            {
                foreach (var bit in bits)
                {
                    if (userprofile.Permissions.Contains(bit))
                    {
                        isOk = true;
                        break;
                    }
                }
            }
            if (!isOk)
            { 
                filterContext.Result = new RedirectToRouteResult(
                           new RouteValueDictionary(
                               new
                               {
                                   controller = "Accounts",
                                   action = "LogOff",
                                   id = 901
                               })
                           );
            }
            base.OnActionExecuting(filterContext);
        }
         
    }

}