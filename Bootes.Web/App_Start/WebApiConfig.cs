﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;

namespace Bootes.Web
{
    public static class WebApiConfig
    {
        /// <summary>
        /// 
        /// </summary>
        public static string UrlPrefix { get { return "api"; } }
        /// <summary>
        /// 
        /// </summary>
        public static string UrlPrefixRelative { get { return "~/api"; } }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="config"></param>
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: UrlPrefix + "/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );


            config.Formatters.XmlFormatter.SupportedMediaTypes.Clear();


        }
    }
}
