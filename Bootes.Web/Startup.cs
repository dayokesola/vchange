﻿using System;
using System.Threading.Tasks;
using System.Web.Helpers;
using System.Web.Http;
using Bootes.Core.Common;
using Bootes.Core.Logic.Module;
using Microsoft.Owin;
using Microsoft.Owin.Security.Cookies;
using Owin;

[assembly: OwinStartup(typeof(Bootes.Web.Startup))]

namespace Bootes.Web
{
    public class Startup
    {
        /// <summary>
        /// 
        /// </summary>
        public static HttpConfiguration HttpConfiguration { get; set; }

        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration = new HttpConfiguration();
            AntiForgeryConfig.UniqueClaimTypeIdentifier = "unique_user_key";
            WebApiConfig.Register(HttpConfiguration);
            Core.Data.DB.BootesPoco.Setup(); 
            AutoMapperConfig.RegisterMappings();
            
            app.UseCookieAuthentication(new CookieAuthenticationOptions
            {
                AuthenticationType = "Cookies",
                LoginPath = new PathString("/Accounts/SignIn"),
                ExpireTimeSpan = TimeSpan.FromMinutes(30),
                SlidingExpiration = true
            });
            //var Logic = new LogicModule();
            //Logic.CommonService.LoadCache();
            app.UseWebApi(HttpConfiguration);
        }
    }
}
