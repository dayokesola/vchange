using Bootes.Core.Common;
using Bootes.Web.Helpers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{

    public class AppsController : BaseController
    {
        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string name = "", string code = "", int? runtimeStatusId = 0, DateTime? availableAt = null, string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.AppService.SearchView(name, code, runtimeStatusId.GetValueOrDefault(), availableAt, page: 0);
                Download(data);
            }
            ViewBag.RuntimeStatus = Util.SerializeJSON(Logic.EntitySettingService.SelectList("RUNTIMESTATUS").ToList());
            return View();
        }
    }


}