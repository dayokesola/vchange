using Bootes.Core.Common;
using Bootes.Web.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{

    public class LocationsController : BaseController
    {

        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string name = "", string code = "", string info = "", int parentId = 0, int locationTypeId = 0, string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.LocationService.SearchView(name, code, info, parentId, locationTypeId, page: 0);
                Download(data);
            }

            ViewBag.LocationTypes = Util.SerializeJSON(Logic.EntitySettingService.SelectList("LOCATIONTYPE").ToList());
            return View();
        }

    }

}