using Bootes.Core.Common;
using Bootes.Core.Domain.Helper;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Logic.Module;
using Bootes.Core.UI;
using Microsoft.Owin.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{

    /// <summary>
    /// 
    /// </summary> 
    public class BaseController : Controller
    {
        private LogicModule _module;

        /// <summary>
        /// 
        /// </summary>
        public LogicModule Logic
        {
            get
            {
                if (_module == null)
                {
                    _module = new LogicModule();
                }
                return _module;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="modelState"></param>
        /// <returns></returns>
        public string ModelError(ModelStateDictionary modelState)
        {
            string error = "";
            foreach (var state in modelState.Values)
            {
                foreach (var msg in state.Errors)
                {
                    error += msg.ErrorMessage + "<br />";
                }
            }
            return error;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="dismissable"></param>
        public void AlertSuccess(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Success, message, dismissable);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="dismissable"></param>
        public void AlertInfo(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Info, message, dismissable);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="dismissable"></param>
        public void AlertWarning(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Warning, message, dismissable);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="message"></param>
        /// <param name="dismissable"></param>
        public void AlertDanger(string message, bool dismissable = true)
        {
            AddAlert(AlertStyles.Danger, message, dismissable);
        }


        private void AddAlert(string alertStyle, string message, bool dismissable)
        {
            var alerts = TempData.ContainsKey(Alert.TempDataKey)
                ? (List<Alert>)TempData[Alert.TempDataKey]
                : new List<Alert>();

            alerts.Add(new Alert
            {
                AlertStyle = alertStyle,
                Message = message,
                Dismissable = dismissable
            });

            TempData[Alert.TempDataKey] = alerts;
        }


        private LoggedInUserModel _loggedInUser;

        /// <summary>
        /// 
        /// </summary>
        public LoggedInUserModel LoggedInUser
        {
            get
            {
                if (_loggedInUser == null)
                {
                    _loggedInUser = Logic.FactoryModule.SiteUsers.Profile();
                }
                return _loggedInUser;
            }
        }

        protected void Download<T>(Page<T> data)
        {
            var n = typeof(T).Name;
            var fi = DataExport.ListToCSV(data.Items);
            byte[] bt = Util.ReadBytesOfFile(fi.FullName, delete: true);
            Response.AppendHeader("content-disposition", "attachment; filename=" + n + "_" + fi.Name);
            Response.ContentType = "text/csv";
            Response.BinaryWrite(bt);
            Response.Flush();
            Response.End();
        }


        protected ActionResult DoLogin(SiteUserModel user, string returnUrl)
        {
            var newIdentity = new ClaimsIdentity("Cookies", ClaimTypes.Email, ClaimTypes.Role);
            newIdentity.AddClaim(new Claim(ClaimTypes.GivenName, user.FirstName));
            newIdentity.AddClaim(new Claim(ClaimTypes.Surname, user.LastName));
            newIdentity.AddClaim(new Claim(ClaimTypes.Name, user.GetName()));
            newIdentity.AddClaim(new Claim(ClaimTypes.Email, user.Email));
            newIdentity.AddClaim(new Claim(ClaimTypes.MobilePhone, user.Mobile));
            newIdentity.AddClaim(new Claim(ClaimTypes.Role, "Subscriber"));
            newIdentity.AddClaim(new Claim("id", user.Id.ToString()));
            newIdentity.AddClaim(new Claim("unique_user_key", "bootes_web_" + user.Id));
            var liu = Logic.FactoryModule.SiteUsers.Profile(user); 
            var accounts = Logic.CorporateUserService.SearchView(userId: user.Id, userStatusId: 1, isLockedOut: false, page: 0).Items;
            int c = 0;
            var perms = new List<string>();
            perms.Add("Subscriber");
             

            if (accounts != null)
            {
                
                liu.AddCorporateUserAccounts(accounts);
                foreach(var acct in accounts)
                { 

                    if (!perms.Contains(acct.CorporateRoleTypeName))
                    {
                        newIdentity.AddClaim(new Claim(ClaimTypes.Role, acct.CorporateRoleTypeName));
                        perms.Add(acct.CorporateRoleTypeName);
                        c++;
                    }

                }
            } 

            liu.UseSelector = c > 1;
            liu.Permissions = perms;
            newIdentity.AddClaim(new Claim("profile", Util.SerializeJSON(liu))); 
            var tk = Logic.SiteUserService.CreateToken(newIdentity);
            var cookie = new HttpCookie("usertoken", tk)
            {
                Expires = DateTime.Now.AddMonths(2),
                Domain = Constants.APP_DOMAIN
            };
            Response.Cookies.Add(cookie);

            //cookie = new HttpCookie("roles", Util.SerializeJSON(perms))
            //{
            //    Expires = DateTime.Now.AddMonths(2),
            //    Domain = Constants.APP_DOMAIN
            //};
            //Response.Cookies.Add(cookie);
            //get permissions
            //var permissions = Logic.RoleService.GetPermissions(user.RoleId);
            //newIdentity.AddClaim(new Claim("permissions", Util.SerializeJSON(permissions)));
            Session["RunSession"] = "1";
            Request.RequestContext.HttpContext.GetOwinContext().Authentication.SignIn(new AuthenticationProperties()
            {
                AllowRefresh = true,
                IsPersistent = false,
                ExpiresUtc = DateTime.UtcNow.AddDays(7)
            }, newIdentity);
            //Lets Audit 
            //Auditor("Account.Login", userModel.LogMe(), user.UserName, corp.Id); 
            //if (liu.UseSelector)
            //{
            //    return RedirectToAction("Selector", "Dashboard");
            //}
            return RedirectToAction("Index", "Dashboard");
        }
    }
}