using Bootes.Core.Common;
using Bootes.Web.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{

    public class SiteRolesController : BaseController
    {

        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string name = "", string code = "", int? departmentId = 0, int? roleTypeId = 0, string info = "", string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.SiteRoleService.SearchView(name, code, departmentId.GetValueOrDefault(), roleTypeId.GetValueOrDefault(), info, page: 0);
                Download(data);
            }

            ViewBag.RoleTypes = Util.SerializeJSON(Logic.EntitySettingService.SelectList("ROLETYPE").ToList());
            ViewBag.Departments = Util.SerializeJSON(Logic.EntitySettingService.SelectList("DEPARTMENT").ToList());
            return View();
        }

    }


}