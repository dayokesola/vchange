using Bootes.Web.Helpers;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{

    public class ClientsController : BaseController
    {

        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string clientId = "", string clientSecret1 = "", string clientSecret2 = "", int? clientTypeId = null, int? clientStatusId = null, string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.ClientService.SearchView(clientId, clientSecret1, clientSecret2, clientTypeId.GetValueOrDefault(), clientStatusId.GetValueOrDefault(), page: 0);
                Download(data);
            }
            return View();
        }

    }

}