﻿using Bootes.Web.Helpers;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
    public class ArticlesController : BaseController
    {
        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string name = "", string title = "", string tags = "",
            string content = "", int editorType = 0, string altContents = "", string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.ArticleService.SearchView(name, title, tags, content, editorType, altContents, page: 0);
                Download(data);
            }
            return View();
        }
    }


}