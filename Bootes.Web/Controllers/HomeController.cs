﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Page(string id = "")
        { 
            var article = Logic.ArticleService.SearchView(name: id, page: 0).Items.FirstOrDefault();
            if(article == null)
            {
                return HttpNotFound();
            } 
            ViewBag.Article = article; 
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Features()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }


}