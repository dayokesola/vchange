using Bootes.Core.Common;
using Bootes.Web.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
    public class MerchantsController : BaseController
    {

        [HttpGet]
        public ActionResult Index()
        {
            return View();
        }

        [HttpGet]
        [CustomAuth(Permissions = "Merchant")]
        public ActionResult Home(string id)
        {
            var corporate = Logic.CorporateService.SearchView(code: id, corporatestatus: 2, page: 0).Items.FirstOrDefault();
            if (corporate == null)
            {
                AlertWarning("Invalid corporate code");
                return RedirectToAction("Index");
            }

            if (!LoggedInUser.UserInCorporate(corporate.Id))
            {
                AlertWarning("Invalid Access");
                return RedirectToAction("Index");
            }


            ViewBag.Corporate = Util.SerializeJSON(corporate);
            ViewBag.CorporateId = corporate.Id;

            var wallets = Logic.WalletService.SearchView(walletTypeId: 2, ownerId: corporate.Id, page: 0).Items;

            if (wallets == null)
            {
                var wallet = Logic.WalletService.CreateMerchantWallet(corporate);
                wallets.Add(wallet);
            }
            if (wallets.Count <= 0)
            {
                var wallet = Logic.WalletService.CreateMerchantWallet(corporate);
                wallets.Add(wallet);
            }
            ViewBag.Wallets = Util.SerializeJSON(wallets);

            ViewBag.MerchantName = corporate.PublicName;

            return View();
        }
    }
}