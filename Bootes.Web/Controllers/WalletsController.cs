using Bootes.Core.Common;
using Bootes.Core.Domain.Helper;
using Bootes.Web.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
    public class WalletsController : BaseController
    {

        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string name = "", string code = "", int walletTypeId = 0, decimal currentBalance = 0, decimal openingBalance = 0, decimal balanceLimit = 0, decimal heldBalance = 0, int currencyId = 0, int ownerId = 0, string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.WalletService.SearchView(name, code, walletTypeId, currentBalance, openingBalance, balanceLimit, heldBalance, currencyId, ownerId, page: 0);
                Download(data);
            }
            return View();
        }

        [CustomAuth(Permissions = "Merchant")]
        public ActionResult Fund(string id)
        {
            //get wallet
            var wallet = Logic.WalletService.SearchView(code: id, page: 0).Items.FirstOrDefault();
            if (wallet == null)
            {
                return HttpNotFound("Invalid Wallet");
            }

            if (!LoggedInUser.UserInCorporate(wallet.OwnerId))
            {
                AlertWarning("Invalid Access");
                return RedirectToAction("Index");
            }
            //create wallet fund model
            var form = Logic.FactoryModule.WalletTransacts.WalletFund(wallet, LoggedInUser.User);

            var settings = Logic.EntitySettingService.SearchView(entityType: "RAVE", entityRef: Constants.RAVE_ID).Items;
            var eshelper = new EntitySettingHelper(settings);
            form.PublicKey = eshelper.GetValue("PUBLIC_KEY");
            ViewBag.Wallet = Util.SerializeJSON(wallet);
            ViewBag.WalletFund = Util.SerializeJSON(form);
            ViewBag.RaveJS = eshelper.GetValue("GATEWAY_URL");
            return View();
        }

        [CustomAuth(Permissions = "Merchant")]
        public ActionResult Withdraw(string id)
        {
            return View();
        }

        public ActionResult Statement(string id)
        {
            var wallet = Logic.WalletService.SearchView(code: id, page: 0).Items.FirstOrDefault();
            if (wallet == null)
            {
                return HttpNotFound("Invalid Wallet");
            }
            if (wallet.WalletTypeId == 2)
            {
                if (!LoggedInUser.UserInCorporate(wallet.OwnerId))
                {
                    AlertWarning("Invalid Access");
                    return RedirectToAction("Index");
                }
            }

            if(wallet.WalletTypeId == 3)
            {
                if (LoggedInUser.User.Id != wallet.OwnerId)
                {
                    AlertWarning("Invalid Access");
                    return RedirectToAction("Index");
                }
            }
            ViewBag.Wallet = Util.SerializeJSON(wallet);
            return View();
        }
         

    }
}