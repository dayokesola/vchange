using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Account;
using Bootes.Web.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
public class CorporateRolesController : BaseController
    {

        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string name = "", int corporateId = 0, bool? isAdmin = null, int roleTypeId = 0, string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.CorporateRoleService.SearchView(name, corporateId, isAdmin, roleTypeId, page: 0);
                Download(data);
            }
            return View();
        }

    }
}