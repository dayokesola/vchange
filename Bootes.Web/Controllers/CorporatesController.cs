using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Account;
using Bootes.Web.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
    public class CorporatesController : BaseController
    {
        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string name = "", string code = "", int countryId = 0, string regId = "", string publicName = "", int appId = 0, string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.CorporateService.SearchView(name, code, countryId, regId, publicName, appId, page: 0);
                Download(data);
            }
            ViewBag.Apps = Util.SerializeJSON(Logic.AppService.Search().ToList().ShapeList("Id,Name"));
            return View();
        }
         
        public ActionResult GettingStarted(int id)
        {
            ViewBag.Form = Util.SerializeJSON(Logic.FactoryModule.Corporates.DefaultForm());
            ViewBag.Countries = Util.SerializeJSON(Logic.LocationService.SearchView(locationTypeId: 2, sort: "Name", page: 0)
                .Items.ShapeList("Id,Name"));
            return View();
        }

        [HttpPost]
        public ActionResult GettingStarted(int id, CorporateForm form)
        {
            ViewBag.Countries = Util.SerializeJSON(Logic.LocationService.SearchView(locationTypeId: 2, sort: "Name", page: 0)
                .Items.ShapeList("Id,Name"));
            ViewBag.Form = Util.SerializeJSON(form);
            if (!ModelState.IsValid)
            {
                AlertDanger(ModelError(ModelState));
                return View();
            }

            //for now users can only create one merchnat

            var msg = "";
            msg = Logic.CorporateService.ValidateSignUp(form);
            if (!string.IsNullOrEmpty(msg))
            {
                AlertDanger(msg);
                return View();
            }
            var code = "";
            var signupStatus = Logic.CorporateService.SignUp(form, LoggedInUser.User, out code);
            switch (signupStatus)
            {
                case SignUpStatus.Ok:
                    msg = "Sign up was successful!";
                    break;
                case SignUpStatus.MobileExists:
                    msg = "Account already exists, please reset your account";
                    break;
            }
            if (signupStatus != SignUpStatus.Ok)
            {
                AlertDanger(msg);
                return View();
            }
            AlertSuccess(msg);
            return RedirectToAction("Update", new { Id = code });
        }

        [HttpGet]
        [CustomAuth(Permissions = "Subscriber")]
        public ActionResult Update(string id)
        {
            //get corporate by code
            var corporate = Logic.CorporateService.SearchView(code: id).Items.FirstOrDefault();
            if (corporate == null)
            {
                AlertWarning("Invalid corporate code");
                return RedirectToAction("Home");
            }

            var corpForm = Logic.FactoryModule.Corporates.CreateFrom(corporate);
            var tags = Logic.TagService.SearchView("merchant", corpForm.Id).Items; 
            corpForm.Tags = string.Join(",", tags.Select(x => x.TagName).ToArray());
            ViewBag.Form = Util.SerializeJSON(corpForm); 
            ViewBag.Countries = Util.SerializeJSON(Logic.LocationService.SearchView(locationTypeId: 2, sort: "Name", page: 0)
                .Items.ShapeList("Id,Name"));
            ViewBag.MerchantName = corpForm.PublicName;
            return View();
        }

        [HttpGet]
        [CustomAuth(Permissions = "Subscriber")]
        public ActionResult Home()
        {
            ViewBag.UserId = LoggedInUser.User.Id;
            return View();
        }

        [HttpGet]
        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult ApproveList()
        {
            return View();
        }

        [HttpGet]
        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Approve(string id)
        {
            //get corporate by code
            var corporate = Logic.CorporateService.SearchView(code: id, corporatestatus: 1).Items.FirstOrDefault();
            if (corporate == null)
            {
                AlertWarning("Invalid corporate code");
                return RedirectToAction("Home");
            }
            var corpForm = new CorporateApprovalForm()
            {
                CorporateId = corporate.Id
            };

            ViewBag.Model = Util.SerializeJSON(corporate);
            ViewBag.Form = Util.SerializeJSON(corpForm);
            return View();
        }

    }
}