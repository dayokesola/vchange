using Bootes.Core.Common;
using Bootes.Core.Domain.Helper;
using Bootes.Core.Domain.Model.MyChange;
using Bootes.Core.Domain.Model.Rave;
using Bootes.Web.Helpers;
using System;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
    public class TransactionsController : BaseController
    {

        [CustomAuth(Permissions = "Subscriber")]
        public async Task<ActionResult> Revalidate(string id)
        { 
            var tx = Logic.WalletTransactService.SearchView(txId: id, page: 0).Items.FirstOrDefault();
            if (tx == null)
            {
                return HttpNotFound("Invalid Transaction");
            } 
            if(!Logic.WalletTransactService.IsValidatable(tx))
            {
                return HttpNotFound("Invalid Transaction");
            }
            if (tx.TransactCodeId == 1) //fund wallet
            {
                return await RevalidateFund(tx); 
            }
            return View();
        }

        private async Task<ActionResult> RevalidateFund(WalletTransactModel tx)
        {
            //let's call Rave to revalidate this transaction
            var settings = Logic.EntitySettingService.SearchView(entityType: "RAVE", entityRef: Constants.RAVE_ID, page: 0).Items;
            var eshelper = new EntitySettingHelper(settings);
            var data = new
            {
                txref = tx.TxId,
                SECKEY = eshelper.GetValue("SECRET_KEY")
            };
            var client = new HttpClient();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var responseMessage = await client.PostAsJsonAsync(eshelper.GetValue("GATEWAY_VERIFY"), data);
            var responseStr = await responseMessage.Content.ReadAsStringAsync();
            Log.Info(responseStr);
            var response = Util.DeserializeJSON<RaveResponseModel>(responseStr);
            if (response.data.status == "successful" && response.data.amount == tx.Amount && response.data.chargecode == "00")
            {
                var transact = Logic.WalletTransactService.Get(tx.Id);
                transact.AuthStatusId = 5;
                transact.AuthDate = Util.CurrentDateTime();
                transact.GateWayRemark = response.data.chargemessage;
                transact.ValueDate = Util.GetDate(response.data.created);
                Logic.WalletTransactService.Update(transact);
               
                //let's post the transaction
                var postId = Logic.WalletTransactService.Post(transact.Id);

                if(postId == 1)
                {
                    transact.AuthStatusId = 6;
                    Logic.WalletTransactService.Update(transact);
                }
            }
            switch (tx.CrWalletTypeId)
            {
                case 1: // system wallet

                    break;
                case 2: //merchant wallet
                    //redirect to merchant home page
                    var wallet = Logic.WalletService.GetModel(tx.CrWalletId);
                    var merchant = Logic.CorporateService.GetModel(wallet.OwnerId);
                    return RedirectToAction("Home", "Merchants", new { id = merchant.Code });
                case 3: //customer wallet
                 
                    break;
            } 
            return View("Revalidate");
        }
    }
}