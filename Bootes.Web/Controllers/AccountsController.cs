﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Accounts;
using System.Web;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
    [AllowAnonymous]
    public class AccountsController : BaseController
    {
        public ActionResult SetAdmin()
        {
            Logic.SiteUserService.MakeAdmin();
            return RedirectToAction("LogOff");
        }

        public ActionResult LogOff(int id = 0)
        {
            //Auditor("Account.logout", new { session = Session.SessionID });
            Session.Abandon();
            Request.RequestContext.HttpContext.GetOwinContext().Authentication.SignOut();
            Response.Cookies.Clear();
            return RedirectToAction("SignIn", "Accounts", new { id = id });
        }

        public ActionResult SignIn()
        {
            ViewBag.Form = Util.SerializeJSON(new SignInForm());
            return View();
        }

        [HttpPost]
        public ActionResult SignIn(SignInForm form)
        {
            ViewBag.Form = Util.SerializeJSON(form);

            var user = Logic.FactoryModule.SiteUsers.DefaultModel();
            var msg = "";
            msg = Logic.SiteUserService.ValidateSignIn(form);
            if (msg == "undef")
            {
                AlertDanger("Sign in mode could not be determined");
                return View();
            }
            var signinStatus = Logic.SiteUserService.SignIn(form, msg, out user);
            switch (signinStatus)
            {
                case SignInStatus.Ok:
                    msg = "Sign In was successful!";
                    break;
                case SignInStatus.UserLockedOut:
                    msg = "Your account is locked out, contact administrator";
                    break;
                default:
                    msg = "Invalid Credentials";
                    break;
            }
            if (signinStatus != SignInStatus.Ok)
            {
                AlertDanger(msg);
                return View();
            }
            return DoLogin(user, "");
        }


        public ActionResult SignUp()
        {
            ViewBag.Countries = Util.SerializeJSON(Logic.LocationService.SearchView(locationTypeId: 2, sort: "Name", page: 0)
                .Items.ShapeList("Id,Name"));
            ViewBag.Form = Util.SerializeJSON(new SignUpForm());
            return View();
        }

        [HttpPost]
        public ActionResult SignUp(SignUpForm form)
        {
            var user = Logic.FactoryModule.SiteUsers.DefaultModel();
            ViewBag.Countries = Util.SerializeJSON(Logic.LocationService.SearchView(locationTypeId: 2, sort: "Name", page: 0)
                .Items.ShapeList("Id,Name"));
            ViewBag.Form = Util.SerializeJSON(form);
            if (!ModelState.IsValid)
            {
                AlertDanger(ModelError(ModelState));
                return View();
            }
            var msg = "";
            msg = Logic.SiteUserService.ValidateSignUp(form);
            if (!string.IsNullOrEmpty(msg))
            {
                AlertDanger(msg);
                return View();
            }
            var signupStatus = Logic.SiteUserService.SignUp(form, out user);
            switch (signupStatus)
            {
                case SignUpStatus.Ok:
                    msg = "Sign up was successful!";
                    break;
                case SignUpStatus.MobileExists:
                    msg = "Mobile already exists, please reset your account";
                    break;
                case SignUpStatus.EmailExists:
                    msg = "Email already exists, please reset your account";
                    break;
            }
            if (signupStatus != SignUpStatus.Ok)
            {
                AlertDanger(msg);
                return View();
            }
            AlertSuccess(msg);
            return DoLogin(user, "");
        }


        public ActionResult ChangePwd(string email, string token)
        {
            var form = new ChangePwdForm()
            {
                Email = email,
                Password = "",
                Password2 = "",
                Token = token
            };

            var changePwdStatus = Logic.SiteUserService.ValidateResetPwd(form);
            var msg = "";
            switch (changePwdStatus)
            {
                case ChangePwdStatus.Ok:
                    msg = "Please enter new passwords for your account";
                    break;

                case ChangePwdStatus.UserLockedOut:
                    msg = "Your account is locked out, pls contact administrator";
                    break;

                case ChangePwdStatus.InvalidToken:
                    msg = "Token supplied is not valid";
                    break;

                default:
                    msg = "Invalid Credentials";
                    break;
            }

            ViewBag.Form = Util.SerializeJSON(form);
            if (changePwdStatus == ChangePwdStatus.Ok)
            {
                AlertSuccess(msg);
                return View();
            }
            else
            {
                AlertDanger(msg);
                return RedirectToAction("ResetPwd");
            }
        }

        [HttpPost]
        public ActionResult ChangePwd(ChangePwdForm form)
        {
            ViewBag.Form = Util.SerializeJSON(form);
            var user = Logic.FactoryModule.SiteUsers.DefaultModel();
            var msg = "";
            var changePwdStatus = Logic.SiteUserService.ChangePwd(form, out user);
            switch (changePwdStatus)
            {
                case ChangePwdStatus.Ok:
                    msg = "Your password has been reset";
                    break;

                case ChangePwdStatus.InvalidPassword:
                    msg = "Invalid passwords";
                    break;

                case ChangePwdStatus.UserLockedOut:
                    msg = "Your account is locked out, pls contact administrator";
                    break;

                case ChangePwdStatus.InvalidToken:
                    msg = "Token supplied is not valid";
                    break;

                default:
                    msg = "Invalid Credentials";
                    break;
            }

            if (changePwdStatus == ChangePwdStatus.Ok)
            {
                AlertSuccess(msg);
                return DoLogin(user, "");
            }
            else
            {
                AlertDanger(msg);
            }
            return View();
        }


        public ActionResult ResetPwd()
        {
            ViewBag.Form = Util.SerializeJSON(new ResetPwdForm());
            return View();
        }

        [HttpPost]
        public ActionResult ResetPwd(ResetPwdForm form, string ReturnUrl = "")
        {
            ViewBag.Form = Util.SerializeJSON(form);
            var user = Logic.FactoryModule.SiteUsers.DefaultModel();
            var msg = "";
            var resetPwdStatus = Logic.SiteUserService.ResetPwd(form);
            switch (resetPwdStatus)
            {
                case ResetPwdStatus.Ok:
                    msg = "A new password reset mail has been sent to your email";
                    break;

                case ResetPwdStatus.EmailDoesNotExists:
                    msg = "Invalid Email address please sign up";
                    break;

                case ResetPwdStatus.UserLockedOut:
                    msg = "Your account is locked out, pls contact administrator";
                    break;

                default:
                    msg = "Invalid Credentials";
                    break;
            }

            if (resetPwdStatus == ResetPwdStatus.Ok)
            {
                AlertSuccess(msg);
            }
            else
            {
                AlertDanger(msg);
            }
            return View();
        }


    }
}