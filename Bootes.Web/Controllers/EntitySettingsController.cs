using Bootes.Web.Helpers;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{

    public class EntitySettingsController : BaseController
    {

        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string entityType = "", long? entityRef = 0, string sKey = "",
            string sVal = "", string paramType = "", string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.EntitySettingService.SearchView(entityType, entityRef.GetValueOrDefault(), sKey, sVal, paramType, page: 0);
                Download(data);
            }
            return View();
        }

    }

}