﻿using Bootes.Core.Common;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
    public class DashboardController : BaseController
    {
        // GET: Dashboard
        public ActionResult Index()
        {
            ViewBag.UserId = LoggedInUser.User.Id;
            ViewBag.User = Util.SerializeJSON(LoggedInUser.User);
            //get user wallets 
            var wallets = Logic.WalletService.SearchView(walletTypeId: 3, ownerId: LoggedInUser.User.Id).Items;

            if (wallets == null)
            {
                var wallet = Logic.WalletService.CreateCustomerWallet(LoggedInUser.User);
                wallets.Add(wallet);
            }
            if (wallets.Count <= 0)
            {
                var wallet = Logic.WalletService.CreateCustomerWallet(LoggedInUser.User);
                wallets.Add(wallet);
            }
            ViewBag.Wallets = Util.SerializeJSON(wallets);



            return View();
        }
    }
}