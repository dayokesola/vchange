using Bootes.Core.Common;
using Bootes.Core.Domain.Helper;
using Bootes.Web.Helpers;
using System;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
public class WalletStatementsController : BaseController
    {

        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(long walletTransactId = 0, long walletId = 0, decimal amount = 0, decimal currentBalance = 0, int currencyId = 0, int drCr = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "", string sourceId = "", int transactionCodeId = 0, string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.WalletStatementService.SearchView(walletTransactId, walletId, amount, currentBalance, currencyId, drCr, valueDate, paymentReference, narration, sourceId, transactionCodeId, page: 0);
                Download(data);
            }
            return View();
        }

    }
}