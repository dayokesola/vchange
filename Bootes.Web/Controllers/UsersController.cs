using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Account;
using Bootes.Web.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{
public class UsersController : BaseController
    {

        [HttpGet]
        [CustomAuth(Permissions = "Merchant")]
        public ActionResult Admins(string id)
        {
            var corporate = Logic.CorporateService.SearchView(code: id, corporatestatus: 2, page: 0).Items.FirstOrDefault();
            if (corporate == null)
            {
                AlertWarning("Invalid corporate code");
                return RedirectToAction("Index");
            }

            if (!LoggedInUser.UserInCorporate(corporate.Id))
            {
                AlertWarning("Invalid Access");
                return RedirectToAction("Index");
            }


            ViewBag.Corporate = Util.SerializeJSON(corporate);
            ViewBag.CorporateId = corporate.Id;
            ViewBag.CorporateName = corporate.PublicName;
            ViewBag.CorporateRoles = Util.SerializeJSON(Logic
               .CorporateRoleService.SearchView(corporateId: corporate.Id , page: 0)
               .Items.ShapeList("Id,Name"));
            ViewBag.UserStatuses = Util.SerializeJSON(Logic.EntitySettingService.SelectList("USERSTATUS").ToList());
             
            return View();
        }
    }
}