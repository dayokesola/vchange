using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;
using Bootes.Web.Helpers;
using System.Linq;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{


    public class SiteUsersController : BaseController
    {

        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string userName = "", string firstName = "", string lastName = "", string email = "",
            string mobile = "",
            string token = "", int appId = 0, UserStatus? userStatusId = null,
            bool? mobileValidated = null, bool? emailValidated = null, bool? isLookedOut = null,
            string userStamp = "", int countryId = 0, string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.SiteUserService.SearchView(userName, firstName, lastName, email, mobile,
                    token, appId, userStatusId, mobileValidated, emailValidated, isLookedOut, userStamp, countryId, page: 0);
                Download(data);
            }
            ViewBag.Apps = Util.SerializeJSON(Logic.AppService.Search().ToList().ShapeList("Id,Name"));
            ViewBag.UserStatus = Util.SerializeJSON(Logic.EntitySettingService.SelectList("USERSTATUS").ToList());
            return View();
        }

    }



}