using Bootes.Web.Helpers;
using System;
using System.Web.Mvc;

namespace Bootes.Web.Controllers
{

    public class OutBoxesController : BaseController
    {

        [CustomAuth(Permissions = "SysAdmin")]
        public ActionResult Index(string toAddress = "", string fromAddress = "", string subject = "", string messageCode = "", int messageTypeId = 0, DateTime? scheduledAt = null, string contents = "", string attachments = "", bool? isSent = null, DateTime? sentAt = null, string sentRemarks = "", string act = "")
        {
            if (act.ToLower() == "export")
            {
                var data = Logic.OutBoxService.SearchView(toAddress, fromAddress, subject, messageCode, messageTypeId, scheduledAt, contents, attachments, isSent, sentAt, sentRemarks, page: 0);
                Download(data);
            }
            return View();
        }

    }

}