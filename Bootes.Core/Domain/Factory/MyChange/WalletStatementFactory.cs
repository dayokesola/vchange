using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model.MyChange;

namespace Bootes.Core.Domain.Factory.MyChange
{
/// <summary>
    /// WalletStatement Factory
    /// </summary>
    public class WalletStatementFactory : BaseFactory<WalletStatement, WalletStatementModel, WalletStatementForm, long>
    {

    }
}