using System;
using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Domain.Model.MyChange;

namespace Bootes.Core.Domain.Factory.MyChange
{
    /// <summary>
    /// WalletTransact Factory
    /// </summary>
    public class WalletTransactFactory : BaseFactory<WalletTransact, WalletTransactModel, WalletTransactForm, long>
    {
        public WalletTransactModel CreateModel(WalletPayForm form)
        {
            return new WalletTransactModel()
            {
                Amount = form.Amount,
                AuthDate = Util.DefaultDate(),
                AuthId = 0,
                CreatedAt = Util.CurrentDateTime(),
                UpdatedAt = Util.CurrentDateTime(),
                CrWalletId = form.WalletId,
                DrWalletId = 0,
                CurrencyId = form.WalletCurrencyId,
                InitDate = Util.CurrentDateTime(),
                InitId = form.UserId,
                Narration = form.Remarks,
                PaymentReference = form.PaymentReference,
                RecordStatus = Enum.RecordStatus.Active,
                SourceId = form.SourceId,
                TransactCodeId = form.TransactionCodeId,
                ValueDate = Util.DefaultDate(),
                AuthStatusId = 1,
                GateWayReference = "",
                GateWayRemark = "",
                TxId = Util.TimeStampCode("TRX")
            };
        }

        public WalletTransactModel CreateModel(GiveChangeForm form, WalletModel merchantWallet, WalletModel benefWallet)
        {
            return new WalletTransactModel()
            {
                Amount = form.Amount,
                AuthDate = Util.DefaultDate(),
                AuthId = form.AuthId,
                CreatedAt = Util.CurrentDateTime(),
                UpdatedAt = Util.CurrentDateTime(),
                CrWalletId = benefWallet.Id,
                DrWalletId = merchantWallet.Id,
                CurrencyId =  merchantWallet.CurrencyId,
                InitDate = Util.CurrentDateTime(),
                InitId = form.AuthId,
                Narration = "Give Change " +  form.Mobile + " " + form.PaymentReference,
                PaymentReference = form.PaymentReference,
                RecordStatus = Enum.RecordStatus.Active,
                SourceId = form.SourceId,
                TransactCodeId = form.TransactionCodeId,
                ValueDate = Util.DefaultDate(),
                AuthStatusId = 1,
                GateWayReference = "",
                GateWayRemark = "",
                TxId = Util.TimeStampCode("TRX")
            };
        }

        public WalletTransactModel CreateModel(RequestChangeForm form, WalletModel merchantWallet, WalletModel benefWallet)
        {
            return new WalletTransactModel()
            {
                Amount = form.Amount,
                AuthDate = Util.DefaultDate(),
                AuthId = merchantWallet.OwnerId,
                CreatedAt = Util.CurrentDateTime(),
                UpdatedAt = Util.CurrentDateTime(),
                CrWalletId = benefWallet.Id,
                DrWalletId = merchantWallet.Id,
                CurrencyId = merchantWallet.CurrencyId,
                InitDate = Util.CurrentDateTime(),
                InitId = benefWallet.OwnerId,
                Narration = "Request Change " + form.Mobile + " " + form.PaymentReference,
                PaymentReference = form.PaymentReference,
                RecordStatus = Enum.RecordStatus.Active,
                SourceId = form.SourceId,
                TransactCodeId = form.TransactionCodeId,
                ValueDate = Util.DefaultDate(),
                AuthStatusId = 1,
                GateWayReference = "",
                GateWayRemark = "",
                TxId = Util.TimeStampCode("TRX")
            };
        }

        public WalletPayForm WalletFund(WalletModel wallet, SiteUserModel user)
        {
            return new WalletPayForm()
            {
                Amount = 0,
                PaymentReference = "",
                Remarks = "Fund Wallet",
                SourceId = "WEB",
                TransactionCodeId = 1,
                UserId = user.Id,
                WalletCurrencyId = wallet.CurrencyId,
                WalletId = wallet.Id,
                Email = user.Email,
                TxId = "",
                PublicKey = ""
            };
        }

    }
}