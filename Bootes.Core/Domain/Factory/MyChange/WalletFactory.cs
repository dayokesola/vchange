﻿using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model.MyChange;

namespace Bootes.Core.Domain.Factory.MyChange
{

    /// <summary>
    /// Wallet Factory
    /// </summary>
    public class WalletFactory : BaseFactory<Wallet, WalletModel, WalletForm, long>
    {

    }
}
