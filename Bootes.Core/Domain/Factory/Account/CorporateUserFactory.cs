using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Domain.Factory.Account
{

    /// <summary>
    /// CorporateUser Factory
    /// </summary>
    public class CorporateUserFactory : BaseFactory<CorporateUser, CorporateUserModel, CorporateUserForm, int>
    {
        public CorporateUserModel CreateModel(SiteUserModel user, CorporateRoleModel corprole)
        {
            return new CorporateUserModel()
            {
                UserId = user.Id,
                UserStatusId = (int)user.UserStatusId,
                CorporateId = corprole.CorporateId,
                CorporateRoleId = corprole.Id,
                IsLockedOut = false
            };
        }
    }

}