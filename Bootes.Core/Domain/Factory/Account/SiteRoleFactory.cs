using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Domain.Factory.Account
{

    /// <summary>
    /// SiteRole Factory
    /// </summary>
    public class SiteRoleFactory : BaseFactory<SiteRole, SiteRoleModel, SiteRoleForm, int>
    {

    }

}