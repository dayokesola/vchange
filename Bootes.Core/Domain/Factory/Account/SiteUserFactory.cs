﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Form.Accounts;
using Bootes.Core.Domain.Model.Account;
using System;
using System.Security.Claims;
using System.Web;

namespace Bootes.Core.Domain.Factory.Account
{
    /// <summary>
    /// SiteUser Factory
    /// </summary>
    public class SiteUserFactory : BaseFactory<SiteUser, SiteUserModel, SiteUserForm, long>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="user"></param>
        /// <returns></returns>
        public LoggedInUserModel Profile(SiteUserModel user)
        {
            user.Hide();
            var liu = new LoggedInUserModel
            {
                Id = user.Id,
                User = new SiteUserModel()
               
            };
            
            try
            {
                user.Password = "";
                user.PIN = "";
                user.Token = "";
                liu.Id = user.Id;
                liu.User = user;
            }
            catch { }
            return liu;
        }

        /// <summary>
        /// Read profile from claims in session
        /// </summary>
        /// <returns></returns>
        public LoggedInUserModel Profile()
        {
            var liu = new LoggedInUserModel
            {
                Id = 0,
                //Corporate = new Model.WareHouse.DimProviderModel(),
                //Role = new SiteRoleModel(),
                User = new SiteUserModel()
            };
            try
            {

                var id = HttpContext.Current.User.Identity as ClaimsIdentity;
                var json = id.FindFirst("profile").Value;
                liu = Util.DeserializeJSON<LoggedInUserModel>(json);
            }
            catch { }
            return liu;
        }


        

        public SiteUserModel CreateModel(CorporateForm form)
        {
            var user = new SiteUserModel()
            {
                AppId = Constants.APP_ID,
                CreatedAt = Util.CurrentDateTime(),
                PasswordChangedAt = Util.CurrentDateTime(),
                UpdatedAt = Util.CurrentDateTime(),
                Email = form.AdminEmail,
                FirstName = "Corporate",
                LastName = "Admin",
                Mobile = form.AdminEmail,
                Password = "",
                PIN = "",
                RecordStatus = RecordStatus.Active,
                Token = "",
                UserName = form.PublicName,
                UserStatusId = UserStatus.RequireValidation,
                MobileValidated = false,
                EmailValidated = false,
                IsLockedOut = false,
                UserStamp = Guid.NewGuid(),
                CountryId = form.CountryId
            };
            user.Password = MakePassword(form.AdminPassword, user.UserStamp);
            user.PIN = MakePassword(form.AdminPassword, user.UserStamp);

            return user;
        }

        public SiteUserModel CreateModel(SignUpForm form)
        {
            var user = new SiteUserModel()
            {
                AppId = Constants.APP_ID,
                CreatedAt = Util.CurrentDateTime(),
                PasswordChangedAt = Util.CurrentDateTime(),
                UpdatedAt = Util.CurrentDateTime(),
                Email = form.Email,
                FirstName = form.FirstName,
                LastName = form.LastName,
                Mobile = form.Mobile,
                Password = "",
                PIN = "",
                RecordStatus = RecordStatus.Active,
                Token = "",
                UserName = form.PublicName,
                UserStatusId = UserStatus.RequireValidation,
                MobileValidated = false,
                EmailValidated = false,
                IsLockedOut = false,
                UserStamp = Guid.NewGuid(),
                CountryId = form.CountryId
            };
            user.Password = MakePassword(form.Password, user.UserStamp);
            user.PIN = MakePassword(form.Pin, user.UserStamp);

            return user;
        }

        public SignUpForm CreateSignUpForm(CorporateUserForm form)
        {
            var pwd = Util.RandomChars(20);
            var pin = Util.RandomNumber(4);

            return new SignUpForm()
            {
                CountryId = 2,
                Email = form.Email,
                FirstName = form.FirstName,
                LastName = form.LastName,
                Mobile = form.Mobile,
                Password = pwd,
                Password2 = pwd,
                Pin = pin,
                Pin2 = pin,
                PublicName = form.FirstName + " " + form.LastName,
                CorporateRoleId = form.CorporateRoleId
            };
        }

        public string MakePassword(string secret, Guid userStamp)
        {
            return CryptUtil.enkrypt(string.Format("{0}~{1}~{2}", userStamp.ToString(), secret, userStamp.ToString()));
        }
    }
}
