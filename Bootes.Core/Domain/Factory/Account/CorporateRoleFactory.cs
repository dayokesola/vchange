using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Domain.Factory.Account
{

    /// <summary>
    /// CorporateRole Factory
    /// </summary>
    public class CorporateRoleFactory : BaseFactory<CorporateRole, CorporateRoleModel, CorporateRoleForm, int>
    {
        public CorporateRoleModel CreateModel(Corporate corp, SiteRole role, bool isAdmin = false)
        {
            return new CorporateRoleModel()
            {
                CorporateId = corp.Id,
                IsAdmin = isAdmin,
                Name = corp.Name + "." + role.Code,
                RecordStatus = Enum.RecordStatus.Active
            };
        }
    }


}