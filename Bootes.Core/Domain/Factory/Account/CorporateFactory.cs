﻿using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Domain.Factory.Account
{

    /// <summary>
    /// Corporate Factory
    /// </summary>
    public class CorporateFactory : BaseFactory<Corporate, CorporateModel, CorporateForm, int>
    {

    }



}
