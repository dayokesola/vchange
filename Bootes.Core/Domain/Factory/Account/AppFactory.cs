﻿using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Domain.Factory.Account
{
    /// <summary>
    /// App Factory
    /// </summary>
    public class AppFactory : BaseFactory<App, AppModel, AppForm, int>
    {

    }


}
