using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Form.Content;
using Bootes.Core.Domain.Model.Content;

namespace Bootes.Core.Domain.Factory.Content
{

    /// <summary>
    /// Tag Factory
    /// </summary>
    public class TagFactory : BaseFactory<Tag, TagModel, TagForm, long>
    {

    }

}