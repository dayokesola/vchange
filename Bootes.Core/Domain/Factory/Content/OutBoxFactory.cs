using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Form.Content;
using Bootes.Core.Domain.Model.Content;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Common;
using System.Text;

namespace Bootes.Core.Domain.Factory.Content
{

    /// <summary>
    /// OutBox Factory
    /// </summary>
    public class OutBoxFactory : BaseFactory<OutBox, OutBoxModel, OutBoxForm, long>
    {
        public OutBoxModel CreateMailActivationModel(SiteUserModel user, string token, ArticleModel article)
        {
            var mail = new StringBuilder(article.Content);
            mail.Replace("{TOKEN}", token);
            mail.Replace("{NAME}", user.GetName());


            return new OutBoxModel()
            {
                Attachments = "",
                ScheduledAt = Util.CurrentDateTime(),
                SentAt = Util.DefaultDate(),
                Contents = mail.ToString(),
                CreatedAt = Util.CurrentDateTime(),
                SentRemarks = "",
                Subject = Constants.APP_NAME + " Email Activation",
                FromAddress = "noreply@" + Constants.APP_DOMAIN,
                ToAddress = user.Email,
                UpdatedAt = Util.CurrentDateTime(),
                IsSent = false,
                MessageTypeId = 1, //Email
                MessageCode = "ACTIVATION_EMAIL",
                RecordStatus = Enum.RecordStatus.Pending
            };
        }

        public OutBoxModel CreateSMSActivationModel(SiteUserModel user, string token, ArticleModel article)
        {
            var mail = new StringBuilder(article.Content);
            mail.Replace("{TOKEN}", token);
            mail.Replace("{NAME}", user.GetName());


            return new OutBoxModel()
            {
                Attachments = "",
                ScheduledAt = Util.CurrentDateTime(),
                SentAt = Util.DefaultDate(),
                Contents = mail.ToString(),
                CreatedAt = Util.CurrentDateTime(),
                SentRemarks = "",
                Subject = Constants.APP_NAME + " SMS Activation",
                FromAddress = "noreply@" + Constants.APP_DOMAIN,
                ToAddress = user.Mobile,
                UpdatedAt = Util.CurrentDateTime(),
                IsSent = false,
                MessageTypeId = 2, //SMS
                MessageCode = "ACTIVATION_SMS",
                RecordStatus = Enum.RecordStatus.Pending
            };
        }

        public OutBoxModel CreateMailResetPassword(SiteUserModel user, string token, ArticleModel article)
        {
            var url = Constants.SITE_URL + "/Accounts/ChangePwd?email=" + user.Email + "&token=" + token;
            var mail = new StringBuilder(article.Content);
            mail.Replace("{URL}", url);
            mail.Replace("{NAME}", user.GetName());
            return new OutBoxModel()
            {
                Attachments = "",
                ScheduledAt = Util.CurrentDateTime(),
                SentAt = Util.DefaultDate(),
                Contents = mail.ToString(),
                CreatedAt = Util.CurrentDateTime(),
                SentRemarks = "",
                Subject = article.Title,
                FromAddress = Constants.APP_SUPPORT,
                ToAddress = user.Email,
                UpdatedAt = Util.CurrentDateTime(),
                IsSent = false,
                MessageTypeId = 1, //Email
                MessageCode = "RESETPWD_EMAIL",
                RecordStatus = Enum.RecordStatus.Pending
            };
        }

    }

}