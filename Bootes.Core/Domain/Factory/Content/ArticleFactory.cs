﻿using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Form.Content;
using Bootes.Core.Domain.Model.Content;

namespace Bootes.Core.Domain.Factory.Content
{
    /// <summary>
    /// Article Factory
    /// </summary>
    public class ArticleFactory : BaseFactory<Article, ArticleModel, ArticleForm, int>
    {

    }



}
