namespace Bootes.Core.Domain.Enum
{

    public enum SignInStatus
    {
        Ok = 1,
        InvalidEmailUser = 2,
        InvalidMobileUser = 3,
        InvalidMobileAuth = 4,
        InvalidEmailAuth = 5,
        UserLockedOut = 6
    }
}