using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.Domain.Enum
{

    public enum SignUpStatus
    {
        Ok = 1,
        OkDoReset = 2,
        MobileExists = 3,
        EmailExists = 4,
        CreationFailed = 5,
        CorporateAccountFailed = 6
    }
}