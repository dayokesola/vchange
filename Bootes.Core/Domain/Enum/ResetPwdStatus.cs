using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.Domain.Enum
{
public enum ResetPwdStatus
    {
        Ok = 1,
        EmailDoesNotExists = 2,
        UserLockedOut = 3,
        InvalidToken = 4,
    }
}