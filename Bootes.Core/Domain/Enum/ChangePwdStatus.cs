namespace Bootes.Core.Domain.Enum
{
public enum ChangePwdStatus
    {
        Ok = 1,
        UserLockedOut = 2,
        InvalidToken = 3,
        InvalidPassword = 4,
        EmailDoesNotExists = 5
    }
}