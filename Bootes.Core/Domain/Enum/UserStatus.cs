using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.Domain.Enum
{

    public enum UserStatus
    {
        Active = 1,
        RequireValidation = 2
    }

}