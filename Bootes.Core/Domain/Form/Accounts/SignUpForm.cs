﻿using System.ComponentModel.DataAnnotations;

namespace Bootes.Core.Domain.Form.Accounts
{
    public class SignUpForm
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public int CountryId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        [EmailAddress]
        public string Email { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string FirstName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string LastName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Mobile { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Password { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Password2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        //[Required]
        public string Pin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        //[Required]
        public string Pin2 { get; set; }

        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string PublicName { get; set; }
        public int CorporateRoleId { get; set; }
    }
}
