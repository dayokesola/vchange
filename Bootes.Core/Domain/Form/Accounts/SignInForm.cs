using System.ComponentModel.DataAnnotations;

namespace Bootes.Core.Domain.Form.Accounts
{
public class SignInForm
    {
        /// <summary>
        /// 
        /// </summary> 
        public string Email { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 
        /// </summary> 
        public string Password { get; set; }
        /// <summary>
        /// 
        /// </summary> 
        public string Pin { get; set; }
    }
}