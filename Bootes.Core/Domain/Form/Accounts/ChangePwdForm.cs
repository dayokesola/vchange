using System.ComponentModel.DataAnnotations;

namespace Bootes.Core.Domain.Form.Accounts
{
    public class ChangePwdForm
    {
        [Required]
        public string Password { get; set; }
        [Required]
        public string Password2 { get; set; }

        public string Email { get; set; }
        public string Token { get; set; }
    }
}