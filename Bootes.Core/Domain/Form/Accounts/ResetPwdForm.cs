using System.ComponentModel.DataAnnotations;

namespace Bootes.Core.Domain.Form.Accounts
{
public class ResetPwdForm
    {
        [Required]
        public string Email { get; set; }
    }
}