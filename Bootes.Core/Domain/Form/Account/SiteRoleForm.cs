using System;
using System.Linq;
using System.Collections.Generic;

namespace Bootes.Core.Domain.Form.Account
{

    /// <summary>
    /// SiteRole Form
    /// </summary>
    public class SiteRoleForm : BaseForm<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int DepartmentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RoleTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Info { get; set; }

    }

}