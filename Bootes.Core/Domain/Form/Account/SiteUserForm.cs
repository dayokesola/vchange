﻿using Bootes.Core.Domain.Enum;
using System;

namespace Bootes.Core.Domain.Form.Account
{
    /// <summary>
    /// SiteUser Form
    /// </summary>
    public class SiteUserForm : BaseForm<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PIN { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Token { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AppId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime PasswordChangedAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UserStatus UserStatusId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool MobileValidated { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool EmailValidated { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsLockedOut { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid UserStamp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CountryId { get; set; }

    }
}
