using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.Domain.Form.Account
{

    /// <summary>
    /// EntitySetting Form
    /// </summary>
    public class EntitySettingForm : BaseForm<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string EntityType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long EntityRef { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ParamType { get; set; }

    }

}