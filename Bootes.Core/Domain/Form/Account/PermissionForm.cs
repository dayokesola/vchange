using System;
using System.Linq;
using System.Collections.Generic;

namespace Bootes.Core.Domain.Form.Account
{

    /// <summary>
    /// Permission Form
    /// </summary>
    public class PermissionForm : BaseForm<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Info { get; set; }

    }

}