using System.ComponentModel.DataAnnotations;

namespace Bootes.Core.Domain.Form.Account
{
    public class CorporateApprovalForm
    {
        [Required]
        public int CorporateId { get; set; }
        [Required]
        public string Remarks { get; set; }
        [Required]
        public string Verdict { get; set; }

        public int ApprovedBy { get; set; }
    }
}