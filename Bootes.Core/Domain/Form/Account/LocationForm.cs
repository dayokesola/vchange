using System;
using System.Linq;
using System.Collections.Generic;

namespace Bootes.Core.Domain.Form.Account
{

    /// <summary>
    /// Location Form
    /// </summary>
    public class LocationForm : BaseForm<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Info { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LocationTypeId { get; set; }

    }

}