﻿using System;

namespace Bootes.Core.Domain.Form.Account
{
    /// <summary>
    /// App Form
    /// </summary>
    public class AppForm : BaseForm<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RuntimeStatusId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime AvailableAt { get; set; }

    }



}
