﻿using System.ComponentModel.DataAnnotations;

namespace Bootes.Core.Domain.Form.Account
{
    /// <summary>
    /// Corporate Form
    /// </summary>
    public class CorporateForm : BaseForm<int>
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public int AppId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public int CountryId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string RegId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string PublicName { get; set; }

        public string AdminEmail { get; set; }
        public string AdminPassword { get; set; }
        public string AdminPassword2 { get; set; }

        public int CorporateStatus { get; set; }
        public string Tags { get; set; }
    }
}
