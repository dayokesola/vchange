using System;
using System.Linq;
using System.Collections.Generic;

namespace Bootes.Core.Domain.Form.Account
{

    /// <summary>
    /// Client Form
    /// </summary>
    public class ClientForm : BaseForm<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string ClientId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ClientSecret1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ClientSecret2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ClientTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ClientStatusId { get; set; }

    }

}