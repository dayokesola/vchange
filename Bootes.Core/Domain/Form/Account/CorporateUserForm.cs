using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.Domain.Form.Account
{

    /// <summary>
    /// CorporateUser Form
    /// </summary>
    public class CorporateUserForm : BaseForm<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CorporateId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CorporateRoleId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserStatusId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsLockedOut { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AuthPin { get; set; }


        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
    }

}