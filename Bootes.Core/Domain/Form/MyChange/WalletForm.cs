﻿namespace Bootes.Core.Domain.Form.MyChange
{

    /// <summary>
    /// Wallet Form
    /// </summary>
    public class WalletForm : BaseForm<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int WalletTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal CurrentBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal OpeningBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal BalanceLimit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal HeldBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurrencyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long OwnerId { get; set; }

    }
}
