namespace Bootes.Core.Domain.Form.MyChange
{
    public class GiveChangeForm
    {
        public int MerchantId { get; set; }
        public string Mobile { get; set; }
        public decimal Amount { get; set; }
        public string PaymentReference { get; set; }
        public bool SendAlert { get; set; }
        public string Beneficiary { get; set; }
        public long AuthId { get;  set; }
        public string SourceId { get;  set; }
        public int TransactionCodeId { get;  set; }
    }

    public class RequestChangeForm
    {
        public int MerchantId { get; set; }
        public string Mobile { get; set; }
        public decimal Amount { get; set; }
        public string PaymentReference { get; set; }  
        public string SourceId { get; set; }
        public int TransactionCodeId { get; set; }
    }
}