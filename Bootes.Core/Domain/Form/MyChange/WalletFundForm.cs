namespace Bootes.Core.Domain.Form.MyChange
{
    public class WalletPayForm
    {
        public decimal Amount { get; set; }
        public long WalletId { get; set; }
        public int WalletCurrencyId { get; set; }
        public long UserId { get; set; }
        public string Remarks { get; set; }
        public string PaymentReference { get; set; }
        public string SourceId { get; set; }
        public int TransactionCodeId { get; set; }
        public string Email { get; set; }
        public string TxId { get; set; }
        public string PublicKey { get; set; }
        public string GatewayRef { get; set; }
    }
}