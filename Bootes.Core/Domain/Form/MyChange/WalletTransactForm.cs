using System;

namespace Bootes.Core.Domain.Form.MyChange
{
    /// <summary>
    /// WalletTransact Form
    /// </summary>
    public class WalletTransactForm : BaseForm<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public long DrWalletId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long CrWalletId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurrencyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ValueDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TxId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string GateWayReference { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string PaymentReference { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Narration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long InitId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime InitDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long AuthId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime AuthDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SourceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TransactCodeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AuthStatusId { get; set; }
        public string GateWayRemark { get; set; }

    }



}