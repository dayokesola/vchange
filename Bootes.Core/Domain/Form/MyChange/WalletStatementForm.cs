using System;

namespace Bootes.Core.Domain.Form.MyChange
{
/// <summary>
    /// WalletStatement Form
    /// </summary>
    public class WalletStatementForm : BaseForm<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public long WalletTransactId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long WalletId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal CurrentBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurrencyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int DrCr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ValueDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PaymentReference { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Narration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SourceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TransactionCodeId { get; set; }

    }
}