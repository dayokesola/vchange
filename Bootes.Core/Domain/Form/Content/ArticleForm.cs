﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.Domain.Form.Content
{
    /// <summary>
    /// Article Form
    /// </summary>
    public class ArticleForm : BaseForm<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Tags { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int EditorType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AltContents { get; set; }

    }



}
