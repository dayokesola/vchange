using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.Domain.Form.Content
{

    /// <summary>
    /// Tag Form
    /// </summary>
    public class TagForm : BaseForm<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string ObjectType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long ObjectId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TagName { get; set; }

    }

}