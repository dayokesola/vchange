using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.Domain.Form.Content
{

    /// <summary>
    /// OutBox Form
    /// </summary>
    public class OutBoxForm : BaseForm<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string ToAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FromAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MessageCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int MessageTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ScheduledAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Contents { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Attachments { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsSent { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime SentAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SentRemarks { get; set; }

    }

}