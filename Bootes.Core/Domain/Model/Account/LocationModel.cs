using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using NPoco;

namespace Bootes.Core.Domain.Model.Account
{
    /// <summary>
    /// Location View Model
    /// </summary>
    [TableName("bts_locationmodel")]
    [PrimaryKey("Id")]
    public class LocationModel : BaseModel<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Info { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LocationTypeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string ParentName { get; set; }


        /// <summary>
        /// 
        /// </summary>
        public string LocationTypeName { get; set; }
        [Ignore]
        public string CodeLower
        {
            get
            {
                return Code.ToLower();
            }
        }
    }

}