﻿using Bootes.Core.Common;
using NPoco;
using System;

namespace Bootes.Core.Domain.Model.Account
{
    /// <summary>
    /// App View Model
    /// </summary>
    [TableName("bts_appmodel")]
    [PrimaryKey("Id")]
    public class AppModel : BaseModel<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RuntimeStatusId { get; set; }

        public string RuntimeStatusName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime AvailableAt { get; set; }

        [Ignore]
        public string AvailableAtText
        {
            get
            {
                return AvailableAt.ToString(Constants.DEF_DATE);
            }
        }


    }


}
