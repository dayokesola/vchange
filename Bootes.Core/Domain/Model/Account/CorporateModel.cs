﻿using Bootes.Core.Common;
using NPoco;

namespace Bootes.Core.Domain.Model.Account
{
    /// <summary>
    /// Corporate View Model
    /// </summary>
    [TableName("bts_corporatemodel")]
    [PrimaryKey("Id")]
    public class CorporateModel : BaseModel<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AppId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CountryId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string RegId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PublicName { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string AppName { get; set; }

        public string CountryName { get; set; }

        public string CountryCode { get; set; }

        public int CorporateStatus { get; set; }

        public string CorporateStatusText { get; set; }

        [Ignore]
        public string CorporateStatusColor
        {
            get
            {
                var c = "";
                switch (CorporateStatus)
                {
                    case 0: c = "inverse"; break;
                    case 1: c = "warning"; break;
                    case 2: c = "success"; break;
                    case 3: c = "danger"; break;
                }
                return c;
            }
        }

        [Ignore]
        public string NameLogo
        {
            get
            {
                if (!string.IsNullOrEmpty(Name))
                    return Name.TransformInitials();
                else
                    return "";

            }
        }

        [Ignore]
        public string CountryCodeLower
        {
            get
            {
                if (!string.IsNullOrEmpty(CountryCode))
                    return CountryCode.ToLower();
                else
                    return "";

            }
        }

    }


}
