using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using NPoco;

namespace Bootes.Core.Domain.Model.Account
{

    /// <summary>
    /// Permission View Model
    /// </summary>
    [TableName("bts_permissionmodel")]
    [PrimaryKey("Id")]
    public class PermissionModel : BaseModel<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Info { get; set; }

    }

}