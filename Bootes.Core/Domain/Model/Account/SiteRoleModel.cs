using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using NPoco;

namespace Bootes.Core.Domain.Model.Account
{

    /// <summary>
    /// SiteRole View Model
    /// </summary>
    [TableName("bts_siterolemodel")]
    [PrimaryKey("Id")]
    public class SiteRoleModel : BaseModel<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int DepartmentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RoleTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Info { get; set; }
        public string RoleTypeName { get; set; }
        public string DepartmentName { get; set; }

    }

}