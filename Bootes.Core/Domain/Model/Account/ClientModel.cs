using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using NPoco;

namespace Bootes.Core.Domain.Model.Account
{

    /// <summary>
    /// Client View Model
    /// </summary>
    [TableName("bts_clientmodel")]
    [PrimaryKey("Id")]
    public class ClientModel : BaseModel<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string ClientId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ClientSecret1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ClientSecret2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ClientTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ClientStatusId { get; set; }

    }

}