using NPoco;
using System;

namespace Bootes.Core.Domain.Model.Account
{

    /// <summary>
    /// CorporateUser View Model
    /// </summary>
    [TableName("bts_corporateusermodel")]
    [PrimaryKey("Id")]
    public class CorporateUserModel : BaseModel<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CorporateId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CorporateRoleId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserStatusId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsLockedOut { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AuthPin { get; set; }


        public string CorporateName { get; set; }

        public string CorporatePublicName { get; set; }
        public string CorporateRoleName { get; set; }
        public bool CorporateRoleIsAdmin { get; set; }

        public string CorporateRoleTypeName { get; set; }
        public int CorporateRoleTypeId { get; set; }



        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }

        public Guid UserStamp { get; set; }
    }

}