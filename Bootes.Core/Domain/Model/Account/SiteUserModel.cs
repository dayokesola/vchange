﻿using Bootes.Core.Domain.Enum;
using System;
using NPoco;
using Bootes.Core.Common;

namespace Bootes.Core.Domain.Model.Account
{

    /// <summary>
    /// SiteUser View Model
    /// </summary>
    [TableName("bts_siteusermodel")]
    [PrimaryKey("Id")]
    public class SiteUserModel : BaseModel<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FirstName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string LastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Email { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Mobile { get; set; }

        public string GetName()
        {
            return UserName;
        }

        /// <summary>
        /// 
        /// </summary>
        public string Password { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PIN { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Token { get; set; }
      
        /// <summary>
        /// 
        /// </summary>
        public int AppId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime PasswordChangedAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UserStatus UserStatusId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool MobileValidated { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool EmailValidated { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsLockedOut { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid UserStamp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CountryId { get; set; }


        [Ignore]
        public string UserStampText
        {
            get
            {
                return UserStamp.ToString();
            }
        }


        [Ignore]
        public string PasswordChangedAtText
        {
            get
            {
                return PasswordChangedAt.ToString(Constants.DEF_DATE);
            }
        }


        [Ignore]
        public string UserStatusText
        {
            get
            {
                return UserStatusId.ToString();
            }
        }

        public void Hide()
        {
            Password = "password";
            PIN = "pin";
            Token = "token";
        }
    }

}
