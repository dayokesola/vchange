using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace Bootes.Core.Domain.Model.Account
{

    /// <summary>
    /// EntitySetting View Model
    /// </summary>
    [TableName("bts_entitysettingmodel")]
    [PrimaryKey("Id")]
    public class EntitySettingModel : BaseModel<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string EntityType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long EntityRef { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SKey { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SVal { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string ParamType { get; set; }

    }

}