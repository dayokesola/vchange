﻿using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Domain.Model.Account
{
    public class LoggedInUserModel
    {
        public long Id { get; set; }
        public List<CorporateUserModel> Accounts { get; set; }
        public SiteUserModel User { get; set; }

        public bool UseSelector { get; set; }
        public bool IsGuest { get; set; }
        public List<string> Permissions { get; set; }

        public void AddCorporateUserAccounts(List<CorporateUserModel> accounts)
        {
            Accounts = new List<CorporateUserModel>();
            foreach (var account in accounts)
            {  
                Accounts.Add(account);
            }
        }

        public void SetUser(SiteUserModel user)
        {
            User = new SiteUserModel()
            {
                UserName = user.UserName,
                FirstName = user.FirstName,
                LastName = user.LastName,
                Email = user.Email,
                Mobile = user.Mobile
            };
        }


        public bool UserInCorporate(long corporateId)
        {
            var ac = Accounts.FirstOrDefault(x => x.CorporateId == corporateId);
            if(ac == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        //public bool IsClient
        //{
        //    get
        //    {
        //        return !Role.Name.StartsWith("Admin");
        //    }
        //}


        /// <summary>
        /// 
        /// </summary>
        //public bool IsAdmin
        //{
        //    get
        //    {
        //        return Role.Name.StartsWith("Admin");
        //    }
        //}

    }
}
