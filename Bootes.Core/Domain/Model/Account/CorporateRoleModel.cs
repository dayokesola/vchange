using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace Bootes.Core.Domain.Model.Account
{

    /// <summary>
    /// CorporateRole View Model
    /// </summary>
    [TableName("bts_corporaterolemodel")]
    [PrimaryKey("Id")]
    public class CorporateRoleModel : BaseModel<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CorporateId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsAdmin { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RoleTypeId { get; set; }

        public string CorporateName { get; set; }
        public string CorporateCode { get; set; }
        public string RoleTypeName { get; set; }

    }


}