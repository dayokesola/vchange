using System;

namespace Bootes.Core.Domain.Model.Rave
{
    public class RavePaymentModel
    {
        public int txid { get; set; }
        public string txref { get; set; }
        public string flwref { get; set; }
        public string devicefingerprint { get; set; }
        public string cycle { get; set; }
        public decimal amount { get; set; }
        public string currency { get; set; }
        public decimal chargedamount { get; set; }
        public decimal appfee { get; set; }
        public decimal merchantfee { get; set; }
        public int merchantbearsfee { get; set; }
        public string narration { get; set; }
        public string status { get; set; }
        public string chargecode { get; set; }
        public DateTime? created { get; set; }
        public DateTime? custcreated { get; set; }
        public string chargemessage { get; set; }
    }
}