﻿using System;

namespace Bootes.Core.Domain.Model.Rave
{

    public class RaveResponseModel
    {
        public string status { get; set; }
        public string message { get; set; }
        public RavePaymentModel data { get; set; }
    }
}
