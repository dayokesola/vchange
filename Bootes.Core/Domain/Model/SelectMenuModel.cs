using System;
using System.Linq;
using System.Collections.Generic;

namespace Bootes.Core.Domain.Model
{

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class SelectMenuModel<T>
    {
        /// <summary>
        /// 
        /// </summary>
        public T Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; } 
    }

}