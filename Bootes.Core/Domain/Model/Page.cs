﻿using System.Collections.Generic;

namespace Bootes.Core.Domain.Model
{


    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class Page<T>
    {
        /// <summary>
        /// 
        /// </summary>
        public Page()
        {

        }
        /// <summary>
        /// 
        /// </summary>
        public long CurrentPage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long TotalPages { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long TotalItems { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long ItemsPerPage { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public List<T> Items { get; set; }

    }
}
