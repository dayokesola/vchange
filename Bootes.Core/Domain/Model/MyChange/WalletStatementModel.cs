using NPoco;
using System;

namespace Bootes.Core.Domain.Model.MyChange
{
/// <summary>
    /// WalletStatement View Model
    /// </summary>
    [TableName("bts_walletstatementmodel")]
    [PrimaryKey("Id")]
    public class WalletStatementModel : BaseModel<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public long WalletTransactId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long WalletId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal CurrentBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal CurrentBalanceBF { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurrencyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int DrCr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ValueDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PaymentReference { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Narration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SourceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TransactionCodeId { get; set; }


        public string WalletName { get; set; }
        public string WalletBalance { get; set; }
        public string WalletBalanceLimit { get; set; }
        public string WalletOpeningBalance { get; set; }
        public string WalletHeldBalance { get; set; }
        public string WalletOwnerId { get; set; }
        public string WalletCode { get; set; }
        public string WalletTypeId { get; set; }
        public string WalletTypeName { get; set; }
        public string CurrencyName { get; set; }
        public string TransactCodeName { get; set; }


        [Ignore]
        public string DrCrText
        {
            get
            {
                return (DrCr == 1) ? "DR" : "CR";
            }
        }


        [Ignore]
        public string PaymentReferenceText
        {
            get
            {
                return (PaymentReference == null) ? "" : PaymentReference;
            }
        }

    }
}