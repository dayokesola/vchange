namespace Bootes.Core.Domain.Model.MyChange
{
    public class DashboardHomeModel
    {
        public int MerchantsCount { get; set; }
        public int SubscribersCount { get;  set; }
    }
}