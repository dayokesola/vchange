﻿using NPoco;

namespace Bootes.Core.Domain.Model.MyChange
{

    /// <summary>
    /// Wallet View Model
    /// </summary>
    [TableName("bts_walletmodel")]
    [PrimaryKey("Id")]
    public class WalletModel : BaseModel<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int WalletTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal CurrentBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal OpeningBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal BalanceLimit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal HeldBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurrencyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long OwnerId { get; set; }

        public string WalletTypeName { get; set; }

        public string CurrencyName { get; set; }

        [Ignore]
        public decimal AvailableBalance
        {
            get
            {
                return CurrentBalance - BalanceLimit - HeldBalance;
            }
        }

    }
}
