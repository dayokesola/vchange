using NPoco;
using System;

namespace Bootes.Core.Domain.Model.MyChange
{
    /// <summary>
    /// WalletTransact View Model
    /// </summary>
    [TableName("bts_wallettransactmodel")]
    [PrimaryKey("Id")]
    public class WalletTransactModel : BaseModel<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public long DrWalletId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long CrWalletId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurrencyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ValueDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TxId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string GateWayReference { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PaymentReference { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Narration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long InitId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime InitDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long AuthId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime AuthDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SourceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TransactCodeId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int AuthStatusId { get; set; }
        public string GateWayRemark { get; set; }


        public string CurrencyName { get; set; }
        public string TransactionCodeName { get; set; }
        public string AuthStatusName { get; set; }
        public string DrWalletname { get; set; }
        public string DrWalletCode { get; set; }
        public int DrWalletTypeId { get; set; }
        public string DrWalletTypeName { get; set; }
        public string CrWalletName { get; set; }
        public string CrWalletCode { get; set; }
        public int CrWalletTypeId { get; set; }
        public string CrWalletTypeName { get; set; }

    }
}