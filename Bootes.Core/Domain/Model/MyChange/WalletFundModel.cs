namespace Bootes.Core.Domain.Model.MyChange
{
    public class WalletFundModel
    {
        public string GatewayUrl { get; set; }
        public string CallbackUrl { get; set; }
        public string PublicKey { get; set; }
        public string TxId { get; set; }
    }
}