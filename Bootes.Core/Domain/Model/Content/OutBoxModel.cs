using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace Bootes.Core.Domain.Model.Content
{

    /// <summary>
    /// OutBox View Model
    /// </summary>
    [TableName("bts_outboxmodel")]
    [PrimaryKey("Id")]
    public class OutBoxModel : BaseModel<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string ToAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string FromAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Subject { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string MessageCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int MessageTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ScheduledAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Contents { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Attachments { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsSent { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime SentAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string SentRemarks { get; set; }

    }

}