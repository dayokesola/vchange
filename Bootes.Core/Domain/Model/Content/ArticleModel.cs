﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NPoco;

namespace Bootes.Core.Domain.Model.Content
{
    /// <summary>
    /// Article View Model
    /// </summary>
    [TableName("bts_articlemodel")]
    [PrimaryKey("Id")]
    public class ArticleModel : BaseModel<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Tags { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int EditorType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string AltContents { get; set; }

    }



}
