using NPoco;

namespace Bootes.Core.Domain.Model.Content
{

    /// <summary>
    /// Tag View Model
    /// </summary>
    [TableName("bts_tagmodel")]
    [PrimaryKey("Id")]
    public class TagModel : BaseModel<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public string ObjectType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long ObjectId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string TagName { get; set; }

    }

    public class TagFrequency
    {

        public string TagName { get; set; }
        public long Frequency { get; set; }
    }

}