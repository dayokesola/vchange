namespace Bootes.Core.Domain.Model.USSD
{
    public class ParamData
    {
        public string SKey { get; set; }
        public string SVal { get; set; }
    }
}