namespace Bootes.Core.Domain.Model.USSD
{
    public static class HashStatus
    {
        #region ErrorCode

        public const string SUCCESSFUL = "00";
        public const string SUCCESSFUL_CONTINUE = "0A";
        public const string SUCCESSFUL_TERMINATE = "0Z";

        public const string ERROR = "99";
        public const string CORE_COMM_ERROR = "98";
        public const string FAILED = "97";
        public const string FAILED_CONTINUE = "9A";

        public const string IN_PROGRESS = "88";
        public const string INVALID_BILLER = "87";

        public const string INVALID_AUTH = "79";
        public const string USER_CANCELLATION = "78";
        public const string INVALID_INPUT = "77";
        public const string PIN_MISMATCH = "76";

        #endregion ErrorCode

        public static string GetDescription(string resp, string errorinfo = "")
        {
            var txt = "ERROR";
            switch (resp)
            {
                case "00": txt = "SUCCESSFUL"; break;
                case "00A": txt = "SUCCESSFUL SECURITY CODE NEEDS SETTING"; break;

                case "99": txt = "ERROR"; break;
                case "98": txt = "CORE_ERROR"; break;
                case "97": txt = "FAILED"; break;

                case "88": txt = "IN_PROGRESS"; break;
                case "87": txt = "INVALID_BILLER"; break;

                case "79": txt = "INVALID_AUTH"; break;
                case "78": txt = "USER_CANCELLATION"; break;
                case INVALID_INPUT: txt = "INVALID_INPUT"; break;
                case "76": txt = "PIN_MISMATCH"; break;
            }
            return txt.Replace('_', ' ') + (string.IsNullOrEmpty(errorinfo) ? "" : ": " + errorinfo);
        }

        public static bool IsFinal(string resp)
        {
            bool verdict = true;
            switch (resp)
            {
                case IN_PROGRESS:
                    verdict = false;
                    break;
            }
            return verdict;
        }

        public static bool IsNotFinal(string resp)
        {
            return !IsFinal(resp);
        }

        public static bool IsNotSuccessful(string resp)
        {
            return !IsSuccessful(resp);
        }

        public static bool IsSuccessful(string resp)
        {
            bool verdict = false;
            switch (resp)
            {
                case SUCCESSFUL:
                case SUCCESSFUL_CONTINUE:
                    verdict = true;
                    break;
            }
            return verdict;
        }
    }
}