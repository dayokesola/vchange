﻿using Bootes.Core.Common;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Domain.Model.USSD
{
    public class Relay
    {
        private List<ParamData> _data;
        private List<ParamData> _data_add;
        private string _message;

        public object data { get; set; }
        public object data_add { get; set; }
        public string hash { get; set; }

        public string message
        {
            get { return _message; }
            set
            {
                _message = value;
            }
        }

        public string mobile { get; set; }
        public string network { get; set; }
        public string serviceCode { get; set; }
        public string session { get; set; }
        public string status { get; set; }
        public string log { get; set; }
        public string apicode { get; set; }
        public string apitext { get; set; }

        public void AddData(string skey, string sval)
        {
            try
            {
                if (_data_add == null)
                {
                    _data_add = new List<ParamData>();
                }
                _data_add.Add(new ParamData() { SKey = skey, SVal = sval });
                data_add = _data_add;
            }
            catch { }
        }

        public string GetValue(string key)
        {
            try
            {
                if (_data == null)
                {
                    _data = Util.DeserializeJSON<List<ParamData>>(data.ToString());
                }
                var k = _data.FirstOrDefault(x => x.SKey.ToUpper() == key.ToUpper());
                return (k == null) ? string.Empty : k.SVal;
            }
            catch
            {
                return "";
            }
        }

        public bool KeyExists(string key)
        {
            try
            {
                if (_data == null)
                {
                    _data = Util.DeserializeJSON<List<ParamData>>(data.ToString());
                }

                var k = _data.FirstOrDefault(x => x.SKey.ToUpper() == key.ToUpper());
                return (k == null) ? false : true;
            }
            catch
            {
                return false;
            }
        }
    }
}