using Bootes.Core.Common;
using NPoco;
using System;
using System.IO;

namespace Bootes.Core.Domain.Model
{
    public class BasePhotoModel<T> : BaseModel<T>
    {
        [Ignore]
        public string Image { get; set; }
        [Ignore]
        public bool IsPermanent { get; set; }

        public string PhotoFileName { get; set; }
        public int PhotoFileTypeId { get; set; }


        [Ignore]
        public string FileNameUrl
        {
            get
            {
                SetPhotoName();
                return Settings.Get("media.baseurl") + PhotoFileTypeId + "/" + PhotoFileName;
            }
        }

        public void ImageToBase64()
        {

            SetPhotoName();

            string path = GetFullPath();
            using (System.Drawing.Image image = System.Drawing.Image.FromFile(path))
            {
                using (MemoryStream m = new MemoryStream())
                {
                    image.Save(m, image.RawFormat);
                    byte[] imageBytes = m.ToArray();
                    Image = Convert.ToBase64String(imageBytes);
                }
            }
            if (PhotoFileName.EndsWith(".png"))
            {
                Image = "data:image/png;base64," + Image;
            }
            else if (PhotoFileName.EndsWith(".jpg"))
            {
                Image = "data:image/jpeg;base64," + Image;
            }
            else
            {

            }
        }

        public string GetFullPath()
        {
            SetPhotoName();
            return Settings.Get("media.path") + PhotoFileTypeId + "/" + PhotoFileName;
        }

        public string SetPhotoName()
        {
            if (string.IsNullOrEmpty(PhotoFileName))
            {
                PhotoFileName = "noimage.png";
            }

            if (PhotoFileName == "noimage.png")
            {
                IsPermanent = true;
            }
            return PhotoFileName;
        }
    }
}