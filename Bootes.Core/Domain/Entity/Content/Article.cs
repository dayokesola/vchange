﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.Content
{
    /// <summary>
    /// Article Class
    /// </summary>
    public class Article : BaseEntity<int>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Title { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string Tags { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")] 
        public string Content { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int EditorType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string AltContents { get; set; }

    }



}
