using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.Content
{

    /// <summary>
    /// OutBox Class
    /// </summary>
    public class OutBox : BaseEntity<long>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string ToAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string FromAddress { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Subject { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(32)]
        public string MessageCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int MessageTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ScheduledAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")] 
        public string Contents { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string Attachments { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsSent { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime SentAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string SentRemarks { get; set; }

    }

}