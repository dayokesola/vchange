using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.Content
{

    /// <summary>
    /// Tag Class
    /// </summary>
    public class Tag : BaseEntity<long>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(32)]
        public string ObjectType { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long ObjectId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(64)]
        public string TagName { get; set; }

    }

}