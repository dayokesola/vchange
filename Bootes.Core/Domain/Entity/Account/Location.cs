using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.Account
{

    /// <summary>
    /// Location Class
    /// </summary>
    public class Location : BaseEntity<int>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(10)]
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Info { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ParentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int LocationTypeId { get; set; }
  
    }

}