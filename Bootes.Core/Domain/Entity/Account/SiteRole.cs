﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.Account
{

    /// <summary>
    /// SiteRole Class
    /// </summary>
    public class SiteRole : BaseEntity<int>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(32)]
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int DepartmentId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RoleTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string Info { get; set; }

    }
}
