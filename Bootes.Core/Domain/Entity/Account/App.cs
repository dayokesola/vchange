﻿
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.Account
{  
    /// <summary>
    /// App Class
    /// </summary>
    public class App : BaseEntity<int>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int RuntimeStatusId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime AvailableAt { get; set; }

    }


}
