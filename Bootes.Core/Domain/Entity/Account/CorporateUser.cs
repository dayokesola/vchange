using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.Account
{

    /// <summary>
    /// CorporateUser Class
    /// </summary>
    public class CorporateUser : BaseEntity<int>
    {
        /// <summary>
        /// 
        /// </summary>
        public long UserId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CorporateId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CorporateRoleId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int UserStatusId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsLockedOut { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string AuthPin { get; set; }

    }

}