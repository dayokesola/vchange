using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.Account
{

    /// <summary>
    /// Client Class
    /// </summary>
    public class Client : BaseEntity<long>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string ClientId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string ClientSecret1 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [MaxLength(256)]
        public string ClientSecret2 { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ClientTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int ClientStatusId { get; set; }

    }

}