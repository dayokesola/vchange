﻿using Bootes.Core.Domain.Enum;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.Account
{
    /// <summary>
    /// SiteUser Class
    /// </summary>
    public class SiteUser : BaseEntity<long>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string UserName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string FirstName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string LastName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Email { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(32)]
        public string Mobile { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string Password { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string PIN { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string Token { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AppId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime PasswordChangedAt { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public UserStatus UserStatusId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool MobileValidated { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool EmailValidated { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool IsLockedOut { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid UserStamp { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CountryId { get; set; }

    }

 
}
