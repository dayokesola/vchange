using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.MyChange
{
    /// <summary>
    /// WalletTransact Class
    /// </summary>
    public class WalletTransact : BaseEntity<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public long DrWalletId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long CrWalletId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurrencyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ValueDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(64)]
        public string TxId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(64)]
        public string GateWayReference { get; set; }

        [Column(TypeName = "varchar")]
        [MaxLength(256)]
        public string GateWayRemark { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(64)]
        public string PaymentReference { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Narration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long InitId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime InitDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long AuthId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime AuthDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int AuthStatusId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(32)]
        public string SourceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TransactCodeId { get; set; }



    }
}