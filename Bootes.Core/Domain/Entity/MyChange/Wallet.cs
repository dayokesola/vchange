﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.MyChange
{
    /// <summary>
    /// Wallet Class
    /// </summary>
    public class Wallet : BaseEntity<long>
    {
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(128)]
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int WalletTypeId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal CurrentBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal OpeningBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal BalanceLimit { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal HeldBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurrencyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long OwnerId { get; set; }

    }
}
