using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Bootes.Core.Domain.Entity.MyChange
{
    /// <summary>
    /// WalletStatement Class
    /// </summary>
    public class WalletStatement : BaseEntity<long>
    {
        /// <summary>
        /// 
        /// </summary>
        public long WalletTransactId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public long WalletId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public decimal CurrentBalance { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int CurrencyId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int DrCr { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public DateTime ValueDate { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(64)]
        public string PaymentReference { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(192)]
        public string Narration { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Column(TypeName = "varchar")]
        [MaxLength(64)]
        public string SourceId { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int TransactionCodeId { get; set; }

    }
}