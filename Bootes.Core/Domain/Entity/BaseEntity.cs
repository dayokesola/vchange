﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.Domain.Entity
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class BaseEntity<T>
    {
        /// <summary>
        /// 
        /// </summary>
        public BaseEntity()
        {
            CreatedAt = Util.CurrentDateTime();
            UpdatedAt = Util.CurrentDateTime();
        }
        /// <summary>
        /// 
        /// </summary>
        [Key]
        public T Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public RecordStatus RecordStatus { get; set; }


        /// <summary>
        /// Date Record was created
        /// </summary> 
        public DateTime CreatedAt { get; set; }

        /// <summary>
        /// Last updated date
        /// </summary>
        public DateTime UpdatedAt { get; set; }
    }
}
