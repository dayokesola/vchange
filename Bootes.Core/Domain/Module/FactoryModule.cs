﻿using Bootes.Core.Domain.Factory.Account;
using Bootes.Core.Domain.Factory.Content;
using Bootes.Core.Domain.Factory.MyChange;
using Bootes.Core.Domain.Model;
using System.Collections.Generic;

namespace Bootes.Core.Domain.Module
{
    public class FactoryModule
    {
        private AppFactory _app;
        private ArticleFactory _article;
        private ClientFactory _client;
        private CorporateFactory _corporate;
        private CorporateRoleFactory _corporaterole;
        private CorporateUserFactory _corporateuser;
        private EntitySettingFactory _entitysetting;
        private LocationFactory _location;
        private OutBoxFactory _outbox;
        private PermissionFactory _permission;
        private SiteRoleFactory _siterole;
        private SiteUserFactory _siteuser;
        private TagFactory _tag;
        private WalletFactory _wallet;
        private WalletStatementFactory _walletstatement;
        private WalletTransactFactory _wallettransact;
        /// <summary>
        /// App Factory Module
        /// </summary>
        public AppFactory Apps { get { if (_app == null) { _app = new AppFactory(); } return _app; } }

        /// <summary>
        /// Article Factory Module
        /// </summary>
        public ArticleFactory Articles { get { if (_article == null) { _article = new ArticleFactory(); } return _article; } }

        /// <summary>
        /// Client Factory Module
        /// </summary>
        public ClientFactory Clients { get { if (_client == null) { _client = new ClientFactory(); } return _client; } }

        /// <summary>
        /// CorporateRole Factory Module
        /// </summary>
        public CorporateRoleFactory CorporateRoles { get { if (_corporaterole == null) { _corporaterole = new CorporateRoleFactory(); } return _corporaterole; } }

        /// <summary>
        /// Corporate Factory Module
        /// </summary>
        public CorporateFactory Corporates { get { if (_corporate == null) { _corporate = new CorporateFactory(); } return _corporate; } }

        /// <summary>
        /// CorporateUser Factory Module
        /// </summary>
        public CorporateUserFactory CorporateUsers { get { if (_corporateuser == null) { _corporateuser = new CorporateUserFactory(); } return _corporateuser; } }

        /// <summary>
        /// EntitySetting Factory Module
        /// </summary>
        public EntitySettingFactory EntitySettings { get { if (_entitysetting == null) { _entitysetting = new EntitySettingFactory(); } return _entitysetting; } }

        /// <summary>
        /// Location Factory Module
        /// </summary>
        public LocationFactory Locations { get { if (_location == null) { _location = new LocationFactory(); } return _location; } }

        /// <summary>
        /// OutBox Factory Module
        /// </summary>
        public OutBoxFactory OutBoxes { get { if (_outbox == null) { _outbox = new OutBoxFactory(); } return _outbox; } }

        /// <summary>
        /// Permission Factory Module
        /// </summary>
        public PermissionFactory Permissions { get { if (_permission == null) { _permission = new PermissionFactory(); } return _permission; } }

        /// <summary>
        /// SiteRole Factory Module
        /// </summary>
        public SiteRoleFactory SiteRoles { get { if (_siterole == null) { _siterole = new SiteRoleFactory(); } return _siterole; } }

        /// <summary>
        /// SiteUser Factory Module
        /// </summary>
        public SiteUserFactory SiteUsers { get { if (_siteuser == null) { _siteuser = new SiteUserFactory(); } return _siteuser; } }

        /// <summary>
        /// Tag Factory Module
        /// </summary>
        public TagFactory Tags { get { if (_tag == null) { _tag = new TagFactory(); } return _tag; } }

        /// <summary>
        /// Wallet Factory Module
        /// </summary>
        public WalletFactory Wallets { get { if (_wallet == null) { _wallet = new WalletFactory(); } return _wallet; } }

        /// <summary>
        /// WalletStatement Factory Module
        /// </summary>
        public WalletStatementFactory WalletStatements { get { if (_walletstatement == null) { _walletstatement = new WalletStatementFactory(); } return _walletstatement; } }
        /// <summary>
        /// WalletTransact Factory Module
        /// </summary>
        public WalletTransactFactory WalletTransacts { get { if (_wallettransact == null) { _wallettransact = new WalletTransactFactory(); } return _wallettransact; } }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public List<SelectMenuModel<int>> GetEnumList<T>()
        {
            var list = new List<SelectMenuModel<int>>();
            foreach (var e in System.Enum.GetValues(typeof(T)))
            {
                list.Add(new SelectMenuModel<int>()
                {
                    Id = (int)e,
                    Name = e.ToString()
                });
            }
            return list;
        }
    }
}
