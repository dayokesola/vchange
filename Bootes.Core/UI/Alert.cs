﻿using Bootes.Core.Common;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bootes.Core.UI
{
    /// <summary>
    /// 
    /// </summary>
    public class Alert
    {
        /// <summary>
        /// 
        /// </summary>
        public const string TempDataKey = "TempDataAlerts";
        /// <summary>
        /// 
        /// </summary>
        public string AlertStyle { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public bool Dismissable { get; set; }
        private string _message;
        /// <summary>
        /// 
        /// </summary>
        public string Message
        {
            get { return Util.JavaScriptSafeText(_message); }
            set { _message = value; }
        }
    }
}
