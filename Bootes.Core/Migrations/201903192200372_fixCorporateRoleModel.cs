namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixCorporateRoleModel : DbMigration
    {
        public override void Up()
        {
            var txt = @"alter view dbo.bts_corporaterolemodel as
select x.* , c.Name as CorporateName, c.Code as CorporateCode,
rt.SVal as RoleTypeName
from bts_corporaterole x
left join bts_corporate c on x.CorporateId = c.Id
left join bts_entitysetting rt on rt.EntityType = 'ROLETYPE'
and rt.EntityRef = x.RoleTypeId and rt.SKey = 'NAME'
where x.RecordStatus != 3 and x.RecordStatus != 4

go"; 
            Sql(txt);
        }
        
        public override void Down()
        {
        }
    }
}
