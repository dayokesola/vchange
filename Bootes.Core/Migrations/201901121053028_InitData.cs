namespace Bootes.Core.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitData : DbMigration
    {
        public override void Up()
        {
            var txt = @"
SET IDENTITY_INSERT [dbo].[bts_app] ON 

GO
INSERT [dbo].[bts_app] ([Id], [Name], [Code], [RuntimeStatusId], [AvailableAt], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (1, N'Bootes', N'BOOTES', 1, CAST(0x0000A94F00000000 AS DateTime), 1, CAST(0x0000A94F008F4478 AS DateTime), CAST(0x0000A94F00C9C4B5 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[bts_app] OFF
GO
SET IDENTITY_INSERT [dbo].[bts_article] ON 

GO
INSERT [dbo].[bts_article] ([Id], [Name], [Title], [Tags], [Content], [EditorType], [AltContents], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (1, N'About', N'About Us', N'about', N'Hello World', 1, N'hello world', 1, CAST(0x0000A97E009CAB38 AS DateTime), CAST(0x0000A97E00A077DF AS DateTime))
GO
INSERT [dbo].[bts_article] ([Id], [Name], [Title], [Tags], [Content], [EditorType], [AltContents], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (2, N'ACTIVATION_SMS', N'Account activation', N'account,activate', N'Dear {NAME}, Your verification code is: {TOKEN}', 2, N'Dear {NAME}, Your verification code is: {TOKEN}', 1, CAST(0x0000A97E01305587 AS DateTime), CAST(0x0000A97E01305587 AS DateTime))
GO
INSERT [dbo].[bts_article] ([Id], [Name], [Title], [Tags], [Content], [EditorType], [AltContents], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (3, N'ACTIVATION_EMAIL', N'Account activation', N'account,activate', N'Dear {NAME}, Your verification code is: {TOKEN}', 2, N'Dear {NAME}, Your verification code is: {TOKEN}', 1, CAST(0x0000A97E01308F5E AS DateTime), CAST(0x0000A97E01308F5E AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[bts_article] OFF
GO
SET IDENTITY_INSERT [dbo].[bts_corporate] ON 

GO
INSERT [dbo].[bts_corporate] ([Id], [Name], [Code], [AppId], [RecordStatus], [CreatedAt], [UpdatedAt], [CountryId], [RegId], [PublicName]) VALUES (1, N'Bootes', N'SITE', 1, 1, CAST(0x0000A94F00E7D836 AS DateTime), CAST(0x0000A98101370918 AS DateTime), 2, N'0', N'Site')
GO
SET IDENTITY_INSERT [dbo].[bts_corporate] OFF
GO
SET IDENTITY_INSERT [dbo].[bts_entitysetting] ON 

GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (1, N'RUNTIMESTATUS', 1, N'NAME', N'Active', N'string', 1, CAST(0x0000A94F00A17C26 AS DateTime), CAST(0x0000A94F01532810 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (2, N'RUNTIMESTATUS', 2, N'NAME', N'Maintenance', N'string', 1, CAST(0x0000A94F00A18C3A AS DateTime), CAST(0x0000A94F00A18C3A AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (3, N'RUNTIMESTATUS', 3, N'NAME', N'Restarting', N'string', 1, CAST(0x0000A94F00A1A172 AS DateTime), CAST(0x0000A94F00A746E3 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (4, N'DEPARTMENT', 1, N'NAME', N'Technology', N'string', 1, CAST(0x0000A94F015987D6 AS DateTime), CAST(0x0000A94F015987D6 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (5, N'ROLETYPE', 1, N'NAME', N'Super Admin', N'string', 1, CAST(0x0000A94F0159D489 AS DateTime), CAST(0x0000A94F0159D489 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (6, N'ROLETYPE', 2, N'NAME', N'Corporate Admin', N'string', 1, CAST(0x0000A94F0159EB28 AS DateTime), CAST(0x0000A94F0159EB28 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (7, N'ROLETYPE', 3, N'NAME', N'Corporate User', N'string', 1, CAST(0x0000A94F0159F952 AS DateTime), CAST(0x0000A94F0159F952 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (8, N'DEPARTMENT', 2, N'NAME', N'Operations', N'string', 1, CAST(0x0000A94F015A1587 AS DateTime), CAST(0x0000A94F015A1587 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (9, N'USERSTATUS', 1, N'NAME', N'Active', N'string', 1, CAST(0x0000A94F017335F2 AS DateTime), CAST(0x0000A94F017335F2 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (10, N'USERSTATUS', 2, N'NAME', N'Email Validation', N'string', 1, CAST(0x0000A94F01735238 AS DateTime), CAST(0x0000A97E00F3DBF5 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (11, N'LOCATIONTYPE', 1, N'NAME', N'Continent', N'string', 1, CAST(0x0000A97E00CA81E4 AS DateTime), CAST(0x0000A97E00CF7DFA AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (12, N'LOCATIONTYPE', 2, N'NAME', N'Country', N'string', 1, CAST(0x0000A97E00CA91BB AS DateTime), CAST(0x0000A97E00CA91BB AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (13, N'LOCATIONTYPE', 3, N'NAME', N'Region', N'string', 1, CAST(0x0000A97E00CAA7B2 AS DateTime), CAST(0x0000A97E00CAA7B2 AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (14, N'LOCATIONTYPE', 4, N'NAME', N'Locale', N'string', 1, CAST(0x0000A97E00CAB74D AS DateTime), CAST(0x0000A97E00CB18DC AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (15, N'LOCATIONTYPE', 5, N'NAME', N'Suburb', N'string', 1, CAST(0x0000A97E00CAD6E8 AS DateTime), CAST(0x0000A97E00CB10FC AS DateTime))
GO
INSERT [dbo].[bts_entitysetting] ([Id], [EntityType], [EntityRef], [SKey], [SVal], [ParamType], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (16, N'LOCATIONTYPE', 6, N'NAME', N'Street', N'string', 1, CAST(0x0000A97E00CAF426 AS DateTime), CAST(0x0000A97E00CB06A9 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[bts_entitysetting] OFF
GO
SET IDENTITY_INSERT [dbo].[bts_location] ON 

GO
INSERT [dbo].[bts_location] ([Id], [Name], [Code], [Info], [ParentId], [LocationTypeId], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (1, N'Africa', N'F', N'Africa', 0, 1, 1, CAST(0x0000A97E00CA5C1A AS DateTime), CAST(0x0000A97E00CF9DC6 AS DateTime))
GO
INSERT [dbo].[bts_location] ([Id], [Name], [Code], [Info], [ParentId], [LocationTypeId], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (2, N'Nigeria', N'NG', N'Nigeria', 1, 2, 1, CAST(0x0000A97E00CF22AB AS DateTime), CAST(0x0000A97E00CF22AB AS DateTime))
GO

SET IDENTITY_INSERT [dbo].[bts_location] OFF
GO
SET IDENTITY_INSERT [dbo].[bts_siterole] ON 

GO
INSERT [dbo].[bts_siterole] ([Id], [Name], [Code], [DepartmentId], [RoleTypeId], [Info], [RecordStatus], [CreatedAt], [UpdatedAt]) VALUES (1, N'Bootes.SuperAdmin', N'SADM', 1, 1, N'Super Admin', 1, CAST(0x0000A94F015D74DF AS DateTime), CAST(0x0000A94F015D74DF AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[bts_siterole] OFF
GO
";
            Sql(txt);
        }

        public override void Down()
        {
        }
    }
}
