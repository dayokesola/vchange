namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateOwnerIdtoLong : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.bts_wallet", "OwnerId", c => c.Long(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.bts_wallet", "OwnerId", c => c.Int(nullable: false));
        }
    }
}
