namespace Bootes.Core.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateWalletTransactModel : DbMigration
    {
        public override void Up()
        {

            var txt = @"create view dbo.bts_wallettransactmodel as 
select x.* from bts_wallettransact x where x.RecordStatus != 3 and x.RecordStatus != 4
go";
            Sql(txt);

        }

        public override void Down()
        {


            var txt = @"drop view bts_wallettransactmodel
go";
            Sql(txt);
        }
    }
}
