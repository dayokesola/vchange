namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCorpStatusToCorporate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.bts_corporate", "CorporateStatus", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.bts_corporate", "CorporateStatus");
        }
    }
}
