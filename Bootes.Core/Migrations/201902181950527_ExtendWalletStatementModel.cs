namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ExtendWalletStatementModel : DbMigration
    {
        public override void Up()
        {
            var txt = @"ALTER view [dbo].[bts_walletstatementmodel] as
select x.*, w.Name as WalletName, w.CurrentBalance as WalletBalance, w.BalanceLimit as WalletBalanceLimit,
w.OpeningBalance as WalletOpeningBalance, w.HeldBalance as WalletHeldBalance, w.OwnerId as WalletOwnerId,
w.Code as WalletCode, w.WalletTypeId, w.WalletTypeName, w.CurrencyName,tc.SVal as TransactCodeName
from bts_walletstatement x 
left join bts_walletmodel w on x.WalletId = w.Id

left join bts_entitysetting tc on tc.EntityType = 'TRANSACTIONCODE' and tc.EntityRef = x.TransactionCodeId and tc.SKey = 'NAME'
where x.RecordStatus != 3 and x.RecordStatus != 4
GO";
            Sql(txt);

        }
        
        public override void Down()
        {
        }
    }
}
