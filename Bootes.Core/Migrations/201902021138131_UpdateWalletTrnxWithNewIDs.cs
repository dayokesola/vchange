namespace Bootes.Core.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class UpdateWalletTrnxWithNewIDs : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.bts_wallettransact", "TxId", c => c.String(maxLength: 64, unicode: false));
            AddColumn("dbo.bts_wallettransact", "GateWayReference", c => c.String(maxLength: 64, unicode: false));
            AddColumn("dbo.bts_wallettransact", "GateWayRemark", c => c.String(maxLength: 256, unicode: false));
            AddColumn("dbo.bts_wallettransact", "AuthStatusId", c => c.Int(nullable: false));
            AlterColumn("dbo.bts_wallettransact", "PaymentReference", c => c.String(maxLength: 64, unicode: false));
            AlterColumn("dbo.bts_wallettransact", "Narration", c => c.String(maxLength: 128, unicode: false));
            AlterColumn("dbo.bts_wallettransact", "SourceId", c => c.String(maxLength: 32, unicode: false));


            var TXT = @"ALTER view [dbo].[bts_wallettransactmodel] as 
select x.*, cn.SVal as CurrencyName, tc.SVal as TransactionCodeName, av.SVal as AuthStatusName,
dr.Name as DrWalletname, dr.Code as DrWalletCode, dr.WalletTypeId as DrWalletTypeId, dr.WalletTypeName as DrWalletTypeName,
cr.Name as CrWalletname, cr.Code as CrWalletCode, cr.WalletTypeId as CrWalletTypeId, cr.WalletTypeName as CrWalletTypeName

from bts_wallettransact x 

left join bts_walletmodel dr on x.DrWalletId = dr.Id
left join bts_walletmodel cr on x.CrWalletId = cr.Id
left join bts_entitysetting cn on cn.EntityType = 'CURRENCY' and cn.EntityRef = x.CurrencyId and cn.SKey = 'CODE'
left join bts_entitysetting tc on tc.EntityType = 'TRANSACTIONCODE' and tc.EntityRef = x.TransactCodeId and tc.SKey = 'NAME'
left join bts_entitysetting av on av.EntityType = 'AUTHSTATUS' and av.EntityRef = x.AuthStatusId and av.SKey = 'NAME'
where x.RecordStatus != 3 and x.RecordStatus != 4

GO";
            Sql(TXT);
        }

        public override void Down()
        {
            AlterColumn("dbo.bts_wallettransact", "SourceId", c => c.String());
            AlterColumn("dbo.bts_wallettransact", "Narration", c => c.String());
            AlterColumn("dbo.bts_wallettransact", "PaymentReference", c => c.String());
            DropColumn("dbo.bts_wallettransact", "AuthStatusId");
            DropColumn("dbo.bts_wallettransact", "GateWayRemark");
            DropColumn("dbo.bts_wallettransact", "GateWayReference");
            DropColumn("dbo.bts_wallettransact", "TxId");
        }
    }
}
