namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateWalletTransact : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.bts_wallettransact",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DrWalletId = c.Long(nullable: false),
                        CrWalletId = c.Long(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrencyId = c.Int(nullable: false),
                        ValueDate = c.DateTime(nullable: false),
                        PaymentReference = c.String(),
                        Narration = c.String(),
                        InitId = c.Long(nullable: false),
                        InitDate = c.DateTime(nullable: false),
                        AuthId = c.Long(nullable: false),
                        AuthDate = c.DateTime(nullable: false),
                        SourceId = c.String(),
                        TransactCodeId = c.Int(nullable: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.bts_wallettransact");
        }
    }
}
