namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWalletTab : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.bts_wallet",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(maxLength: 128, unicode: false),
                        Code = c.String(maxLength: 128, unicode: false),
                        WalletTypeId = c.Int(nullable: false),
                        CurrentBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OpeningBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BalanceLimit = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HeldBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                        CurrencyId = c.Int(nullable: false),
                        OwnerId = c.Int(nullable: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.bts_wallet");
        }
    }
}
