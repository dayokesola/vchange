namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateWalletModel : DbMigration
    {
        public override void Up()
        {
            var txt = @"ALTER view [dbo].[bts_walletmodel] as 

select x.*, wt.SVal as WalletTypeName, cr.SVal as CurrencyName
from bts_wallet x 
left join bts_entitysetting wt on wt.EntityType = 'WALLETTYPE' and wt.SKey = 'NAME' and wt.EntityRef = x.WalletTypeId
left join bts_entitysetting cr on cr.EntityType = 'CURRENCY' and cr.SKey = 'CODE' and cr.EntityRef = x.CurrencyId
where x.RecordStatus != 3 and x.RecordStatus != 4

GO";
            Sql(txt);
        }
        
        public override void Down()
        {
        }
    }
}
