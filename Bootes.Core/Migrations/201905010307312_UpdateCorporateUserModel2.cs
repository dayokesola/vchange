namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateCorporateUserModel2 : DbMigration
    {
        public override void Up()
        {
            var txt = @"
ALTER view [dbo].[bts_corporateusermodel] as 
select x.*, c.Name as CorporateName, c.PublicName as CorporatePublicName,
r.Name as CorporateRoleName, r.isadmin as CorporateRoleIsAdmin,
r.roleTypeId as CorporateRoleTypeId,
r.roleTypeName as CorporateRoleTypeName,
s.FirstName, s.LastName, s.Mobile, s.Email, s.UserStamp
from bts_corporateuser x 
inner join bts_corporate c on x.CorporateId = c.Id 
inner join bts_corporaterolemodel r on x.CorporateRoleId = r.Id
inner join bts_siteuser s on x.UserId = s.Id
where x.RecordStatus != 3 and x.RecordStatus != 4
GO
";

            Sql(txt);
        }
        
        public override void Down()
        {
        }
    }
}
