namespace Bootes.Core.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class InitViews : DbMigration
    {
        public override void Up()
        {
            var txt = @" 
/****** Object:  View [dbo].[bts_entitysettingmodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[bts_entitysettingmodel] as select x.* from bts_entitysetting x where x.RecordStatus != 3 and x.RecordStatus != 4
GO
/****** Object:  View [dbo].[bts_appmodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[bts_appmodel] as select x.*, r.SVal as RuntimeStatusName
from bts_app x
left
join bts_entitysettingmodel r on r.EntityType = 'RUNTIMESTATUS'
and r.EntityRef = x.RuntimeStatusId and r.SKey = 'NAME'
where x.RecordStatus != 3 and x.RecordStatus != 4

GO
/****** Object:  View [dbo].[bts_siteusermodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[bts_siteusermodel] as 
select x.* , a.Name as AppName, r.SVal as UserStatusName
from bts_siteuser x 

left
join bts_app a on x.AppId = a.Id

left
join bts_entitysettingmodel r on r.EntityType = 'USERSTATUS'
and r.EntityRef = x.UserStatusId and r.SKey = 'NAME'

where x.RecordStatus != 3 and x.RecordStatus != 4

GO
/****** Object:  View [dbo].[bts_siterolemodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[bts_siterolemodel] as 
select x.* , d.SVal as DepartmentName, r.SVal as RoleTypeName

from bts_siterole x 
left
join bts_entitysettingmodel d on d.EntityType = 'DEPARTMENT'
and d.EntityRef = x.DepartmentId and d.SKey = 'NAME'

left
join bts_entitysettingmodel r on r.EntityType = 'ROLETYPE'
and r.EntityRef = x.RoleTypeId and r.SKey = 'NAME'

where x.RecordStatus != 3 and x.RecordStatus != 4

GO
/****** Object:  View [dbo].[bts_locationmodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[bts_locationmodel] as 
select x.*, a.Name as ParentName, r.SVal as LocationTypeName
from bts_location x
left
join bts_location a on x.ParentId = a.Id

left
join bts_entitysettingmodel r on r.EntityType = 'LOCATIONTYPE'
and r.EntityRef = x.LocationTypeId and r.SKey = 'NAME'

 where x.RecordStatus != 3 and x.RecordStatus != 4

GO
/****** Object:  View [dbo].[bts_articlemodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[bts_articlemodel] as select x.* from bts_article x where x.RecordStatus != 3 and x.RecordStatus != 4
GO
/****** Object:  View [dbo].[bts_clientmodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[bts_clientmodel] as select x.* from bts_client x where x.RecordStatus != 3 and x.RecordStatus != 4
GO
/****** Object:  View [dbo].[bts_corporatemodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[bts_corporatemodel] as 
select x.*, a.Name as AppName
from bts_corporate x 
left join bts_app a on x.AppId = a.Id
where x.RecordStatus != 3 and x.RecordStatus != 4
GO
/****** Object:  View [dbo].[bts_corporaterolemodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[bts_corporaterolemodel] as select x.* from bts_corporaterole x where x.RecordStatus != 3 and x.RecordStatus != 4
GO
/****** Object:  View [dbo].[bts_corporateusermodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE view [dbo].[bts_corporateusermodel] as 
select x.*, c.Name as CorporateName, c.PublicName as CorporatePublicName,
r.Name as CorporateRoleName, r.isadmin as CorporateRoleIsAdmin
from bts_corporateuser x 
inner join bts_corporate c on x.CorporateId = c.Id
inner join bts_corporaterole r on x.CorporateRoleId = r.Id
where x.RecordStatus != 3 and x.RecordStatus != 4

GO
/****** Object:  View [dbo].[bts_outboxmodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[bts_outboxmodel] as select x.* from bts_outbox x where x.RecordStatus != 3 and x.RecordStatus != 4
GO
/****** Object:  View [dbo].[bts_permissionmodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[bts_permissionmodel] as select x.* from bts_permission x where x.RecordStatus != 3 and x.RecordStatus != 4
GO
/****** Object:  View [dbo].[bts_tagmodel]    Script Date: 12-Jan-19 11:45:01 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
create view [dbo].[bts_tagmodel] as select x.* from bts_tag x where x.RecordStatus != 3 and x.RecordStatus != 4
GO
";
            Sql(txt);
        }

        public override void Down()
        {
            var txt = @"/****** Object:  View [dbo].[bts_tagmodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_tagmodel]
GO
/****** Object:  View [dbo].[bts_permissionmodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_permissionmodel]
GO
/****** Object:  View [dbo].[bts_outboxmodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_outboxmodel]
GO
/****** Object:  View [dbo].[bts_corporateusermodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_corporateusermodel]
GO
/****** Object:  View [dbo].[bts_corporaterolemodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_corporaterolemodel]
GO
/****** Object:  View [dbo].[bts_corporatemodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_corporatemodel]
GO
/****** Object:  View [dbo].[bts_clientmodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_clientmodel]
GO
/****** Object:  View [dbo].[bts_articlemodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_articlemodel]
GO
/****** Object:  View [dbo].[bts_locationmodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_locationmodel]
GO
/****** Object:  View [dbo].[bts_siterolemodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_siterolemodel]
GO
/****** Object:  View [dbo].[bts_siteusermodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_siteusermodel]
GO
/****** Object:  View [dbo].[bts_appmodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_appmodel]
GO
/****** Object:  View [dbo].[bts_entitysettingmodel]    Script Date: 12-Jan-19 11:47:50 AM ******/
DROP VIEW [dbo].[bts_entitysettingmodel]
GO";
            Sql(txt);
        }
    }
}
