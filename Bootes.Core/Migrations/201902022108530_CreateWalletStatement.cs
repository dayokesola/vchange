namespace Bootes.Core.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateWalletStatement : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.bts_walletstatement",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    WalletTransactId = c.Long(nullable: false),
                    WalletId = c.Long(nullable: false),
                    Amount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    CurrentBalance = c.Decimal(nullable: false, precision: 18, scale: 2),
                    CurrencyId = c.Int(nullable: false),
                    DrCr = c.Int(nullable: false),
                    ValueDate = c.DateTime(nullable: false),
                    PaymentReference = c.String(maxLength: 64, unicode: false),
                    Narration = c.String(maxLength: 192, unicode: false),
                    SourceId = c.String(maxLength: 64, unicode: false),
                    TransactionCodeId = c.Int(nullable: false),
                    RecordStatus = c.Int(nullable: false),
                    CreatedAt = c.DateTime(nullable: false),
                    UpdatedAt = c.DateTime(nullable: false),
                })
                .PrimaryKey(t => t.Id);

            var txt = @"create view bts_walletstatementmodel as
select x.* from bts_walletstatement x where x.RecordStatus != 3 and x.RecordStatus != 4

GO";
            Sql(txt);




        }

        public override void Down()
        {

            var txt = @"drop view bts_walletstatementmodel
go";
            Sql(txt);
            DropTable("dbo.bts_walletstatement");
        }
    }
}
