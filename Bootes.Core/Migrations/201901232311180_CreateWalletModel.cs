namespace Bootes.Core.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateWalletModel : DbMigration
    {
        public override void Up()
        {
            var txt = @"create view dbo.bts_walletmodel as select x.* from bts_wallet x where x.RecordStatus != 3 and x.RecordStatus != 4
GO";
            Sql(txt);


        }

        public override void Down()
        {

            var txt = @"drop view bts_walletmodel 
GO";
            Sql(txt);
        }
    }
}
