namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitTables : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.bts_app",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 128, unicode: false),
                        Code = c.String(maxLength: 128, unicode: false),
                        RuntimeStatusId = c.Int(nullable: false),
                        AvailableAt = c.DateTime(nullable: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_article",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 128, unicode: false),
                        Title = c.String(maxLength: 128, unicode: false),
                        Tags = c.String(maxLength: 256, unicode: false),
                        Content = c.String(maxLength: 8000, unicode: false),
                        EditorType = c.Int(nullable: false),
                        AltContents = c.String(maxLength: 256, unicode: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_client",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ClientId = c.String(maxLength: 128, unicode: false),
                        ClientSecret1 = c.String(maxLength: 256, unicode: false),
                        ClientSecret2 = c.String(maxLength: 256),
                        ClientTypeId = c.Int(nullable: false),
                        ClientStatusId = c.Int(nullable: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_corporaterole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 64, unicode: false),
                        CorporateId = c.Int(nullable: false),
                        IsAdmin = c.Boolean(nullable: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_corporate",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 128, unicode: false),
                        Code = c.String(maxLength: 32, unicode: false),
                        AppId = c.Int(nullable: false),
                        CountryId = c.Int(nullable: false),
                        RegId = c.String(maxLength: 64, unicode: false),
                        PublicName = c.String(maxLength: 32, unicode: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_corporateuser",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.Long(nullable: false),
                        CorporateId = c.Int(nullable: false),
                        CorporateRoleId = c.Int(nullable: false),
                        UserStatusId = c.Int(nullable: false),
                        IsLockedOut = c.Boolean(nullable: false),
                        AuthPin = c.String(maxLength: 256, unicode: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_entitysetting",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        EntityType = c.String(maxLength: 128, unicode: false),
                        EntityRef = c.Long(nullable: false),
                        SKey = c.String(maxLength: 64, unicode: false),
                        SVal = c.String(maxLength: 128, unicode: false),
                        ParamType = c.String(maxLength: 32, unicode: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_location",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 128, unicode: false),
                        Code = c.String(maxLength: 10, unicode: false),
                        Info = c.String(maxLength: 128, unicode: false),
                        ParentId = c.Int(nullable: false),
                        LocationTypeId = c.Int(nullable: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_outbox",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ToAddress = c.String(maxLength: 128, unicode: false),
                        FromAddress = c.String(maxLength: 128, unicode: false),
                        Subject = c.String(maxLength: 128, unicode: false),
                        MessageCode = c.String(maxLength: 32, unicode: false),
                        MessageTypeId = c.Int(nullable: false),
                        ScheduledAt = c.DateTime(nullable: false),
                        Contents = c.String(maxLength: 8000, unicode: false),
                        Attachments = c.String(maxLength: 256, unicode: false),
                        IsSent = c.Boolean(nullable: false),
                        SentAt = c.DateTime(nullable: false),
                        SentRemarks = c.String(maxLength: 256, unicode: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_permission",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 64, unicode: false),
                        Code = c.String(maxLength: 64, unicode: false),
                        Info = c.String(maxLength: 128, unicode: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_siterole",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 128, unicode: false),
                        Code = c.String(maxLength: 32, unicode: false),
                        DepartmentId = c.Int(nullable: false),
                        RoleTypeId = c.Int(nullable: false),
                        Info = c.String(maxLength: 256, unicode: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_siteuser",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        UserName = c.String(maxLength: 128, unicode: false),
                        FirstName = c.String(maxLength: 128, unicode: false),
                        LastName = c.String(maxLength: 128, unicode: false),
                        Email = c.String(maxLength: 128, unicode: false),
                        Mobile = c.String(maxLength: 32, unicode: false),
                        Password = c.String(maxLength: 256, unicode: false),
                        PIN = c.String(maxLength: 256, unicode: false),
                        Token = c.String(maxLength: 256, unicode: false),
                        AppId = c.Int(nullable: false),
                        PasswordChangedAt = c.DateTime(nullable: false),
                        UserStatusId = c.Int(nullable: false),
                        MobileValidated = c.Boolean(nullable: false),
                        EmailValidated = c.Boolean(nullable: false),
                        IsLockedOut = c.Boolean(nullable: false),
                        UserStamp = c.Guid(nullable: false),
                        CountryId = c.Int(nullable: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.bts_tag",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        ObjectType = c.String(maxLength: 32, unicode: false),
                        ObjectId = c.Long(nullable: false),
                        TagName = c.String(maxLength: 64, unicode: false),
                        RecordStatus = c.Int(nullable: false),
                        CreatedAt = c.DateTime(nullable: false),
                        UpdatedAt = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.bts_tag");
            DropTable("dbo.bts_siteuser");
            DropTable("dbo.bts_siterole");
            DropTable("dbo.bts_permission");
            DropTable("dbo.bts_outbox");
            DropTable("dbo.bts_location");
            DropTable("dbo.bts_entitysetting");
            DropTable("dbo.bts_corporateuser");
            DropTable("dbo.bts_corporate");
            DropTable("dbo.bts_corporaterole");
            DropTable("dbo.bts_client");
            DropTable("dbo.bts_article");
            DropTable("dbo.bts_app");
        }
    }
}
