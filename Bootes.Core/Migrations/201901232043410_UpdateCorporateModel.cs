namespace Bootes.Core.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class UpdateCorporateModel : DbMigration
    {
        public override void Up()
        {
            var txt = @"ALTER view [dbo].[bts_corporatemodel] as 
select x.*, a.Name as AppName, l.Name as CountryName, l.Code as CountryCode, s.SVal as CorporateStatusText
from bts_corporate x 
left join bts_app a on x.AppId = a.Id
left join bts_location l on l.LocationTypeId = 2 and x.CountryId = l.Id
left join bts_entitysetting s on s.EntityType = 'CORPORATESTATUS' and s.EntityRef = x.CorporateStatus
where x.RecordStatus != 3 and x.RecordStatus != 4

GO";
            Sql(txt);
        }

        public override void Down()
        {
        }
    }
}
