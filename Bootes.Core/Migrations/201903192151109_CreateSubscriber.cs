namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateSubscriber : DbMigration
    {
        public override void Up()
        {
            var txt = @" SET IDENTITY_INSERT [dbo].[bts_corporaterole] ON 
 INSERT [dbo].[bts_corporaterole] ([Id], [Name], [CorporateId], [IsAdmin], [RecordStatus], [CreatedAt], [UpdatedAt]) 
 VALUES (2, 'Subscriber', 1, 0, 1, getdate(), getdate())
SET IDENTITY_INSERT [dbo].[bts_corporaterole] OFF";

            Sql(txt);
        }
        
        public override void Down()
        {
        }
    }
}
