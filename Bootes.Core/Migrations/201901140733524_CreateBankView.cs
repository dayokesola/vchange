namespace Bootes.Core.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class CreateBankView : DbMigration
    {
        public override void Up()
        {
            var txt = @"CREATE VIEW [dbo].[bts_bankmodel]
AS
SELECT        nom.EntityRef AS Id, nom.SVal AS Name, cod.SVal AS Code, cty.SVal AS CountryId
FROM            dbo.bts_entitysetting AS nom LEFT OUTER JOIN
                         dbo.bts_entitysetting AS cod ON nom.EntityRef = cod.EntityRef AND cod.SKey = 'CODE' LEFT OUTER JOIN
                         dbo.bts_entitysetting AS cty ON nom.EntityRef = cty.EntityRef AND cty.SKey = 'COUNTRY'
WHERE        (nom.EntityType = 'BANK') AND (nom.SKey = 'NAME')

GO";

            Sql(txt);
        }

        public override void Down()
        {
        }
    }
}
