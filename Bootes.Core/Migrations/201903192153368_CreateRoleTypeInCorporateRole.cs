namespace Bootes.Core.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateRoleTypeInCorporateRole : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.bts_corporaterole", "RoleTypeId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.bts_corporaterole", "RoleTypeId");
        }
    }
}
