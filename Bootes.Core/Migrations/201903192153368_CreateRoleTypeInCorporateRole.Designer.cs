// <auto-generated />
namespace Bootes.Core.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class CreateRoleTypeInCorporateRole : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(CreateRoleTypeInCorporateRole));
        
        string IMigrationMetadata.Id
        {
            get { return "201903192153368_CreateRoleTypeInCorporateRole"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
