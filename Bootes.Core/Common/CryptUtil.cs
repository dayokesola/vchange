﻿using System.Security.Cryptography;
using System.Text;

namespace Bootes.Core.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class CryptUtil
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string HashSHA1(string data)
        {
            string hash = "";
            using (SHA1 alg = new SHA1Managed())
            {
                byte[] result = alg.ComputeHash(Encoding.UTF8.GetBytes(data));
                hash = HexStrFromBytes(result);
            }
            return hash;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string HashMD5(string data)
        {
            string hash = "";
            using (MD5 alg = MD5.Create())
            {
                byte[] result = alg.ComputeHash(Encoding.UTF8.GetBytes(data));
                hash = HexStrFromBytes(result);
            }
            return hash;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="text"></param>
        /// <returns></returns>
        public static string Hash512(string text)
        {
            string hash = "";
            using (SHA512 alg = new SHA512Managed())
            {
                byte[] result = alg.ComputeHash(Encoding.UTF8.GetBytes(text));
                hash = HexStrFromBytes(result).ToUpper();
            }
            return hash;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        private static string HexStrFromBytes(byte[] bytes)
        {
            var sb = new StringBuilder();
            foreach (byte b in bytes)
            {
                var hex = b.ToString("x2");
                sb.Append(hex);
            }
            return sb.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strEnk"></param>
        /// <returns></returns>
        public static string enkrypt(string strEnk)
        {
            SymCryptography cryptic = new SymCryptography();
            cryptic.Key = "wqdj*84j47fj9fmwlfd7431%p$#=@hd+&";
            return cryptic.Encrypt(strEnk);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="strEnk"></param>
        /// <returns></returns>
        public static string dekrypt(string strEnk)
        {
            SymCryptography cryptic = new SymCryptography();
            cryptic.Key = "wqdj*84j47fj9fmwlfd7431%p$#=@hd+&";
            return cryptic.Decrypt(strEnk);
        }


        public static string Hash256(string text)
        {
            string hash = "";
            using (SHA256 alg = new SHA256Managed())
            {
                byte[] result = alg.ComputeHash(Encoding.UTF8.GetBytes(text));
                hash = HexStrFromBytes(result).ToUpper();
            }
            return hash;
        }

    }
}