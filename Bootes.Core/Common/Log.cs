﻿using Serilog;
using Serilog.Core;
using System;

namespace Bootes.Core.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class Log
    {

        private static string conn = Constants.LOG_PATH;

        private static Logger logger = new LoggerConfiguration()
            .WriteTo.File(conn + "log.txt", rollingInterval: RollingInterval.Minute)
            .CreateLogger();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ex"></param>
        public static void Error(Exception ex)
        {
            SeriX("ERROR", ex.Message, ex);
        }

        private static void SeriX(string level, string message, Exception ex, params object[] obj)
        {
            
            var lev = Serilog.Events.LogEventLevel.Information;
            switch (level)
            {
                case "INFO": lev = Serilog.Events.LogEventLevel.Information; break;
                case "ERROR": lev = Serilog.Events.LogEventLevel.Error; break;
                case "WARNING": lev = Serilog.Events.LogEventLevel.Warning; break;
            }
            logger.Write(lev, ex, message, obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        /// <param name="obj"></param>
        private static void Seri(string level, string message, Exception ex, params object[] obj)
        {
            if (!Settings.IsDebug) return;

            var lev = Serilog.Events.LogEventLevel.Information;
            switch (level)
            {
                case "INFO": lev = Serilog.Events.LogEventLevel.Information; break;
                case "ERROR": lev = Serilog.Events.LogEventLevel.Error; break;
                case "WARNING": lev = Serilog.Events.LogEventLevel.Warning; break;
            }
            logger.Write(lev, ex, message, obj);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="level"></param>
        /// <param name="message"></param>
        /// <param name="ex"></param>
        /// <param name="obj"></param>
        private static void SeriOk(string level, string message, Exception ex, params object[] obj)
        {

            var lev = Serilog.Events.LogEventLevel.Information;
            switch (level)
            {
                case "INFO": lev = Serilog.Events.LogEventLevel.Information; break;
                case "ERROR": lev = Serilog.Events.LogEventLevel.Error; break;
                case "WARNING": lev = Serilog.Events.LogEventLevel.Warning; break;
            }
            logger.Write(lev, ex, message, obj);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="logMessage"></param>
        /// <param name="loc"></param>
        /// <param name="title"></param>
        public static void Write(string logMessage, string loc = "", string title = "Default")
        {
            Seri("INFO", title, new Exception(logMessage));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        public static void Info(string v)
        {
            Seri("INFO", v, null);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="v"></param>
        public static void Write(object data, string v)
        {
            Seri("INFO", v, new Exception(Util.SerializeJSON(data)));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="data"></param>
        /// <param name="v"></param>
        public static void Writeall(object data, string v)
        {
            SeriOk("INFO", v, new Exception(Util.SerializeJSON(data)));
        }
    }


}
