using System;
using System.Collections.Generic;
using System.Net.Mail;

namespace Bootes.Core.Common
{
public class MailUtil
    {
        public static bool SendMail(string email, string subject, string body, List<string> attachments = null)
        {
            try
            {
                SmtpClient smtpClient = new SmtpClient();
                MailMessage mailMessage = new MailMessage();
                mailMessage.To.Add(new MailAddress(email));
                mailMessage.Subject = subject;
                mailMessage.Body = body;
                mailMessage.IsBodyHtml = true;

                if (attachments != null)
                {
                    foreach (var str in attachments)
                    {
                        Attachment attachment;
                        attachment = new Attachment(str);
                        mailMessage.Attachments.Add(attachment);
                    }
                }
                smtpClient.Send(mailMessage);
                return true;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return false;
            }
        }
    }
}