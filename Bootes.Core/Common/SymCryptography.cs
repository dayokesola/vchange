﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace Bootes.Core.Common
{
    /// <summary>
    /// 
    /// </summary>
    public class SymCryptography
    {
        /// <summary>
        /// 
        /// </summary>
        public enum ServiceProviderEnum
        {
            /// <summary>
            /// 
            /// </summary>
            Rijndael,
            /// <summary>
            /// 
            /// </summary>
            RC2,
            /// <summary>
            /// 
            /// </summary>
            DES,
            /// <summary>
            /// 
            /// </summary>
            TripleDES
        }
        private string mKey = string.Empty;
        private string mSalt = string.Empty;
        private SymCryptography.ServiceProviderEnum mAlgorithm;
        private SymmetricAlgorithm mCryptoService;
        /// <summary>
        /// 
        /// </summary>
        public string Key
        {
            get
            {
                return this.mKey;
            }
            set
            {
                this.mKey = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Salt
        {
            get
            {
                return this.mSalt;
            }
            set
            {
                this.mSalt = value;
            }
        }
        private void SetLegalIV()
        {
            SymCryptography.ServiceProviderEnum serviceProviderEnum = this.mAlgorithm;
            if (serviceProviderEnum != SymCryptography.ServiceProviderEnum.Rijndael)
            {
                this.mCryptoService.IV = new byte[]
                                         {
                                             15,
                                             111,
                                             19,
                                             46,
                                             53,
                                             194,
                                             205,
                                             249
                                         };
            }
            else
            {
                this.mCryptoService.IV = new byte[]
                                         {
                                             15,
                                             111,
                                             19,
                                             46,
                                             53,
                                             194,
                                             205,
                                             249,
                                             5,
                                             70,
                                             156,
                                             234,
                                             168,
                                             75,
                                             115,
                                             204
                                         };
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public SymCryptography()
        {
            this.mCryptoService = new RijndaelManaged();
            this.mCryptoService.Mode = CipherMode.CBC;
            this.mAlgorithm = SymCryptography.ServiceProviderEnum.Rijndael;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProvider"></param>
        public SymCryptography(SymCryptography.ServiceProviderEnum serviceProvider)
        {
            switch (serviceProvider)
            {
                case SymCryptography.ServiceProviderEnum.Rijndael:
                    this.mCryptoService = new RijndaelManaged();
                    this.mAlgorithm = SymCryptography.ServiceProviderEnum.Rijndael;
                    break;
                case SymCryptography.ServiceProviderEnum.RC2:
                    this.mCryptoService = new RC2CryptoServiceProvider();
                    this.mAlgorithm = SymCryptography.ServiceProviderEnum.RC2;
                    break;
                case SymCryptography.ServiceProviderEnum.DES:
                    this.mCryptoService = new DESCryptoServiceProvider();
                    this.mAlgorithm = SymCryptography.ServiceProviderEnum.DES;
                    break;
                case SymCryptography.ServiceProviderEnum.TripleDES:
                    this.mCryptoService = new TripleDESCryptoServiceProvider();
                    this.mAlgorithm = SymCryptography.ServiceProviderEnum.TripleDES;
                    break;
            }
            this.mCryptoService.Mode = CipherMode.CBC;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="serviceProviderName"></param>
        public SymCryptography(string serviceProviderName)
        {
            try
            {
                string text = serviceProviderName.ToLower();
                if (text != null)
                {
                    if (!(text == "rijndael"))
                    {
                        if (!(text == "rc2"))
                        {
                            if (!(text == "des"))
                            {
                                if (text == "tripledes")
                                {
                                    serviceProviderName = "TripleDES";
                                    this.mAlgorithm = SymCryptography.ServiceProviderEnum.TripleDES;
                                }
                            }
                            else
                            {
                                serviceProviderName = "DES";
                                this.mAlgorithm = SymCryptography.ServiceProviderEnum.DES;
                            }
                        }
                        else
                        {
                            serviceProviderName = "RC2";
                            this.mAlgorithm = SymCryptography.ServiceProviderEnum.RC2;
                        }
                    }
                    else
                    {
                        serviceProviderName = "Rijndael";
                        this.mAlgorithm = SymCryptography.ServiceProviderEnum.Rijndael;
                    }
                }
                this.mCryptoService = (SymmetricAlgorithm)CryptoConfig.CreateFromName(serviceProviderName);
                this.mCryptoService.Mode = CipherMode.CBC;
            }
            catch
            {
                throw;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public virtual byte[] GetLegalKey()
        {
            if (this.mCryptoService.LegalKeySizes.Length > 0)
            {
                int num = this.mKey.Length * 8;
                int minSize = this.mCryptoService.LegalKeySizes[0].MinSize;
                int maxSize = this.mCryptoService.LegalKeySizes[0].MaxSize;
                int skipSize = this.mCryptoService.LegalKeySizes[0].SkipSize;
                if (num > maxSize)
                {
                    this.mKey = this.mKey.Substring(0, maxSize / 8);
                }
                else
                {
                    if (num < maxSize)
                    {
                        int num2 = (num <= minSize) ? minSize : (num - num % skipSize + skipSize);
                        if (num < num2)
                        {
                            this.mKey = this.mKey.PadRight(num2 / 8, '*');
                        }
                    }
                }
            }
            PasswordDeriveBytes passwordDeriveBytes = new PasswordDeriveBytes(this.mKey, Encoding.ASCII.GetBytes(this.mSalt));
            return passwordDeriveBytes.GetBytes(this.mKey.Length);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="plainText"></param>
        /// <returns></returns>
        public virtual string Encrypt(string plainText)
        {
            byte[] bytes = Encoding.ASCII.GetBytes(plainText);
            byte[] legalKey = this.GetLegalKey();
            this.mCryptoService.Key = legalKey;
            this.SetLegalIV();
            ICryptoTransform transform = this.mCryptoService.CreateEncryptor();
            MemoryStream memoryStream = new MemoryStream();
            CryptoStream cryptoStream = new CryptoStream(memoryStream, transform, CryptoStreamMode.Write);
            cryptoStream.Write(bytes, 0, bytes.Length);
            cryptoStream.FlushFinalBlock();
            byte[] array = memoryStream.ToArray();
            return Convert.ToBase64String(array, 0, array.GetLength(0));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cryptoText"></param>
        /// <returns></returns>
        public virtual string Decrypt(string cryptoText)
        {
            byte[] array = Convert.FromBase64String(cryptoText);
            byte[] legalKey = this.GetLegalKey();
            this.mCryptoService.Key = legalKey;
            this.SetLegalIV();
            ICryptoTransform transform = this.mCryptoService.CreateDecryptor();
            string result;
            try
            {
                MemoryStream stream = new MemoryStream(array, 0, array.Length);
                CryptoStream stream2 = new CryptoStream(stream, transform, CryptoStreamMode.Read);
                StreamReader streamReader = new StreamReader(stream2);
                result = streamReader.ReadToEnd();
            }
            catch
            {
                result = null;
            }
            return result;
        }



    }


}
