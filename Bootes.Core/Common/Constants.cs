﻿namespace Bootes.Core.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class Constants
    {
        public static string FilesRoot => Settings.Get("files.root", "c:/temp/Files/Bootes/");
        public static string LOG_PATH => Settings.Get("log.path", "c:/temp/Files/Bootes/logs/");
        public static string DBPrefix => Settings.Get("db.prefix", "bts_");
        public static string DBName => Settings.Get("db.name", "bootesdb"); 
        public static string API_URL => Settings.Get("site.baseapi", "http://service.vchange.local");
        public static string SITE_URL => Settings.Get("site.baseurl", "http://www.vchange.local");
        public static long SYSTEM_VERSION => Settings.GetLong("sys.version", 0);
        public static string DEF_DATE => Settings.Get("def.date", "dd-MMM-yyyy");
        public static int APP_ID => Settings.GetInt("app.id", 1);
        public static string APP_NAME => Settings.Get("app.name", "MyChange");
        public static string APP_SUPPORT => Settings.Get("app.support", "info@mychange.com.ng");
        public static string APP_DOMAIN => Settings.Get("app.domain", "vchange.local");
        public static string APP_JWT => Settings.Get("app.jwt", "965b09eab3c013d4ca56922bb802bec8fd5318192b0a75f201d8b3729829090fb337591abd3e44453b954555b7a0812e1081c39b740293f765eae711f5a65ed1");
        public static long RAVE_ID => Settings.GetLong("rave_id", 1);
        public static string HASH_KEY => Settings.Get("hashkey", "276e1140-706f-4f9f-9e8b-75a7005df67a");
    }

}
