﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Bootes.Core.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class Util
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="chk"></param>
        /// <returns></returns>
        public static string Checkable(this bool chk)
        { 
            if (chk)
            {
                return "checked";
            }
            return "";
        }

        public static string TransformInitials(this string txt)
        {
            var resp = "";
            try
            {
                if (!string.IsNullOrEmpty(txt))
                {
                    char[] sep = { ' ' };
                    var bits = txt.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                    foreach (var b in bits)
                    {
                        if (b.Length > 0)
                        {
                            resp += b.Substring(0, 1).ToUpper();
                        }
                    }

                    if (resp.Length >= 2)
                    {
                        resp = resp.Substring(0, 2);
                    }
                }
            }
            catch { }
            return resp;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="month"></param>
        /// <param name="fiscaloffset"></param>
        /// <returns></returns>
        public static int MonthQuarter(int month, int fiscaloffset = 0)
        {
            if (month < 1) month = 1;
            if (month > 12) month = 12;
            var k = (month / 12.0) * 4.0;
            var l = (int)Math.Ceiling(k);
            return l + fiscaloffset;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="month"></param>
        /// <param name="fiscaloffset"></param>
        /// <returns></returns>
        public static int MonthSemester(int month, int fiscaloffset = 0)
        {
            if (month < 1) month = 1;
            if (month > 12) month = 12;
            var k = (month / 12.0) * 2.0;
            var l = (int)Math.Ceiling(k);
            return l + fiscaloffset;
        }

        public static DateTime GetDate(DateTime? created)
        {
            var dtm = Util.DefaultDate();
            if(created.HasValue)
            {
                dtm = created.GetValueOrDefault();
            }
            return dtm.Date;
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="minute"></param>
        /// <returns></returns>
        public static int MinuteHalf(int minute)
        {
            if (minute < 1) minute = 60;
            if (minute > 59) minute = 60;
            var k = (minute / 60.0) * 2.0;
            var l = (int)Math.Ceiling(k);
            return l;
        }

        public static bool IsPIN(string pin)
        {
            string patternStrict = @"^(?!(.)\1{3})(?!19|20)(?!0123|1234|2345|3456|4567|5678|6789|7890|0987|9876|8765|7654|6543|5432|4321|3210)\d{4}$";
            Regex reStrict = new Regex(patternStrict);
            bool isStrictMatch = reStrict.IsMatch(pin);
            return isStrictMatch;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="minute"></param>
        /// <returns></returns>
        public static int MinuteThird(int minute)
        {
            if (minute < 1) minute = 60;
            if (minute > 59) minute = 60;
            var k = (minute / 60.0) * 3.0;
            var l = (int)Math.Ceiling(k);
            return l;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="minute"></param>
        /// <returns></returns>
        public static int MinuteSixth(int minute)
        {
            if (minute < 1) minute = 60;
            if (minute > 59) minute = 60;
            var k = (minute / 60.0) * 6.0;
            var l = (int)Math.Ceiling(k);
            return l;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="minute"></param>
        /// <returns></returns>
        public static int MinuteQuarter(int minute)
        {
            if (minute < 1) minute = 60;
            if (minute > 59) minute = 60;
            var k = (minute / 60.0) * 4.0;
            var l = (int)Math.Ceiling(k);
            return l;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="phone"></param>
        /// <returns></returns>
        public static string CleanPhone(string phone)
        {
            phone = phone.Replace("+", "");
            return phone.Trim();
        }

        /// <summary>
        /// Generate Datetime Key for CUBE
        /// </summary>
        /// <param name="dtm"></param>
        /// <returns></returns>
        public static long ToDateTimeKey(DateTime dtm)
        {
            return ToInt64(dtm.ToString("yyyyMMddHHmm"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtm"></param>
        /// <returns></returns>
        public static long ToDateKey(DateTime dtm)
        {
            return ToInt64(dtm.ToString("yyyyMMdd"));
        }

        /// <summary>
        /// /
        /// </summary>
        /// <param name="bit"></param>
        /// <returns></returns>
        public static string ConvertToRegex(string bit)
        {
            //[D:11:11]
            //[R:100:50000]
            //[L:A:Z]

            char[] trima = { '[', ']' };
            var txt = bit.Trim(trima);
            char[] sep = { ':' };

            string[] bits = txt.ToUpper().Split(sep);
            if (bits.Length != 3)
            {
                throw new Exception("Regex build parts is not 3 characters");
            }
            var resp = "";
            var n1 = 0;
            var n2 = 0;
            switch (bits[0])
            {
                case "D":
                    n1 = ToInt32(bits[1]);
                    n2 = ToInt32(bits[2]);
                    resp = @"\d{" + n1 + "," + n2 + "}";
                    break;

                case "R":
                    break;

                case "A":
                    resp = @"\w{" + n1 + "," + n2 + "}";
                    break;
            }
            return resp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DateTime CurrentDateTime()
        {
            return DateTime.UtcNow;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static bool DBCheckString(string txt)
        {
            Regex r = new Regex("^[a-zA-Z0-9 ]*$");
            return r.IsMatch(txt);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="decimalNumber"></param>
        /// <param name="radix"></param>
        /// <returns></returns>
        public static string DecimalToArbitrarySystem(long decimalNumber, int radix)
        {
            const int BitsInLong = 64;
            const string Digits = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            if (radix < 2 || radix > Digits.Length)
                throw new ArgumentException("The radix must be >= 2 and <= " + Digits.Length.ToString());

            if (decimalNumber == 0)
                return "0";

            int index = BitsInLong - 1;
            long currentNumber = Math.Abs(decimalNumber);
            char[] charArray = new char[BitsInLong];

            while (currentNumber != 0)
            {
                int remainder = (int)(currentNumber % radix);
                charArray[index--] = Digits[remainder];
                currentNumber = currentNumber / radix;
            }

            string result = new string(charArray, index + 1, BitsInLong - index - 1);
            if (decimalNumber < 0)
            {
                result = "-" + result;
            }

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static DateTime DefaultDate()
        {
            return new DateTime(1970, 1, 1);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectData"></param>
        /// <returns></returns>
        public static T DeserializeJSON<T>(string objectData)
        {
            return JsonConvert.DeserializeObject<T>(objectData);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="objectData"></param>
        /// <returns></returns>
        public static T DeserializeXML<T>(string objectData)
        {
            var serializer = new XmlSerializer(typeof(T));
            object result;
            using (TextReader reader = new StringReader(objectData))
            {
                result = serializer.Deserialize(reader);
            }
            return (T)result;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="txt"></param>
        /// <param name="delim"></param>
        /// <param name="opt"></param>
        /// <returns></returns>
        public static string[] Explode(string txt, string delim = ",", int opt = 1)
        {
            string[] arr = null;
            txt = txt.Trim();
            if (txt == "") return arr;
            if (delim.Length != 1) return arr;
            char[] sep = delim.ToCharArray();
            try
            {
                if (opt == 1)
                    arr = txt.Split(sep, StringSplitOptions.None);
                else
                    arr = txt.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            }
            catch
            {
                //Log.Write("StringSplit: " + txt + " " + delim + " " + ex.Message);
            }
            return arr;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="destFile0"></param>
        /// <param name="destFile1"></param>
        public static void FileCopy(string destFile0, string destFile1)
        {
            File.Copy(destFile0, destFile1);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="destFile0"></param>
        public static void FileDelete(string destFile0)
        {
            File.Delete(destFile0);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="destFile0"></param>
        /// <returns></returns>
        public static bool FileExists(string destFile0)
        {
            return File.Exists(destFile0);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GenerateFileName()
        {
            string[] key = new string[]
            {
                "b",
                "c",
                "d",
                "f",
                "g",
                "h",
                "j",
                "k",
                "m",
                "n",
                "p",
                "q",
                "r",
                "t",
                "v",
                "w",
                "x",
                "y",
                "z"
            };
            Random rand = new Random();
            string nme = "";
            nme += DateTime.Now.ToString("yyMMddHHmmss");
            nme += key[rand.Next(0, 18)];
            nme += key[rand.Next(0, 18)];
            return nme + key[rand.Next(0, 18)];
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="newObject"></param>
        /// <param name="oldObject"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        public static string GetDifferenceInData(object newObject, object oldObject, object id)
        {
            string resp = "{'id':" + id.ToString();
            PropertyInfo[] properties = newObject.GetType().GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                string oldpp = GetPropValue(oldObject, pi.Name);
                string newpp = GetPropValue(newObject, pi.Name);

                if (newpp != oldpp)
                {
                    resp += ", '" + pi.Name + "': [{'_old':'" + oldpp + "'},{'_new':'" + newpp + "'}]";
                }
            }
            resp += "}";
            return resp;
        }

        public static bool IsMobile(string mobile, string countryCode)
        {
            var isOk = true;
            mobile = CleanPhone(mobile);
            switch(countryCode.ToLower())
            {
                case "ng":
                    if(mobile.Length != 13)
                    {
                        isOk = false;
                    }
                    if(!mobile.StartsWith("234"))
                    {
                        isOk = false;
                    }
                    break;
                default:
                    isOk = false;
                    break;
            }
            return isOk;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="src"></param>
        /// <param name="propName"></param>
        /// <returns></returns>
        public static string GetPropValue(object src, string propName)
        {
            string resp = "";
            try
            {
                resp = src.GetType().GetProperty(propName).GetValue(src, null).ToString();
            }
            catch
            {
                resp = "";
            }
            return resp;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        public static string GetString(object k)
        {
            if (k == null) return "";
            return k.ToString();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="k"></param>
        /// <returns></returns>
        public static string ToString(object k)
        {
            if (k == null) return "";
            return k.ToString().Trim();
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetUserIP()
        {
            try
            {
                string ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_CLUSTER_CLIENT_IP"];

                if (!string.IsNullOrEmpty(ipList))
                {
                    return ipList.Split(',')[0];
                }
                ipList = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

                if (!string.IsNullOrEmpty(ipList))
                {
                    return ipList.Split(',')[0];
                }

                return HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            catch
            {
                return "ERR";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetUserSession()
        {
            try
            {
                return HttpContext.Current.Session.SessionID;
            }
            catch
            {
                return "ERR";
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="emailAddress"></param>
        /// <returns></returns>
        public static bool IsEmail(string emailAddress)
        {
            string patternStrict = @"^(([^<>()[\]\\.,;:\s@\""]+"
                 + @"(\.[^<>()[\]\\.,;:\s@\""]+)*)|(\"".+\""))@"
                 + @"((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}"
                 + @"\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+"
                 + @"[a-zA-Z]{2,}))$";
            Regex reStrict = new Regex(patternStrict);
            bool isStrictMatch = reStrict.IsMatch(emailAddress);
            return isStrictMatch;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static string JavaScriptSafeText(string txt)
        {
            return txt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="pattern"></param>
        /// <param name="text"></param>
        /// <returns></returns>
        public static bool MatchPattern(string pattern, string text)
        {
            Regex reStrict = new Regex(pattern);
            bool isStrictMatch = reStrict.IsMatch(text);
            return isStrictMatch;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="obj"></param>
        /// <returns></returns>
        public static string ObjectToString(object obj)
        {
            string resp = "";
            PropertyInfo[] properties = obj.GetType().GetProperties();
            foreach (PropertyInfo pi in properties)
            {
                string oldpp = GetPropValue(obj, pi.Name);
                resp += pi.Name + ": " + oldpp + Environment.NewLine;
            }
            return resp;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cnt"></param>
        /// <returns></returns>
        public static string RandomChars(int cnt)
        {
            string[] key1 = { "!", "@", "#", "$", "%", "&", "*", "?", "+", "." };
            Random rand1 = new Random();
            string txt = "";
            for (int j = 0; j < cnt; j++)
            {
                txt += key1[rand1.Next(0, key1.Length - 1)];
                Thread.Sleep(2);
            }
            return txt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cnt"></param>
        /// <returns></returns>
        public static string RandomLower(int cnt = 10)
        {
            string[] key1 = { "b", "c", "d", "f", "g", "h", "j", "k", "a", "m", "n", "p", "q", "e", "i", "u", "r", "t", "v", "w", "x", "y", "z" };
            Random rand1 = new Random();
            string txt = "";
            for (int j = 0; j < cnt; j++)
            {
                txt += key1[rand1.Next(0, key1.Length - 1)];
                Thread.Sleep(2);
            }
            return txt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cnt"></param>
        /// <returns></returns>
        public static string RandomNumber(int cnt)
        {
            string[] key1 = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
            Random rand1 = new Random();
            string txt = "";
            for (int j = 0; j < cnt; j++)
            {
                txt += key1[rand1.Next(0, key1.Length - 1)];
                Thread.Sleep(2);
            }
            return txt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="cnt"></param>
        /// <returns></returns>
        public static string RandomUpper(int cnt = 10)
        {
            string[] key1 = { "N", "J", "Q", "K", "U", "D", "V", "E", "T", "F", "C", "Y", "P", "R", "W", "M", "X", "Z", "G", "B", "P", "A", "H", "L", "U", "V", "Y" };
            Random rand1 = new Random();
            string txt = "";
            for (int j = 0; j < cnt; j++)
            {
                txt += key1[rand1.Next(0, key1.Length - 1)];
                Thread.Sleep(2);
            }
            return txt;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="fle"></param>
        /// <param name="delete"></param>
        /// <returns></returns>
        public static byte[] ReadBytesOfFile(string fle, bool delete = false)
        {
            byte[] bytes = new byte[1];
            try
            {
                using (FileStream fsSource = new FileStream(fle, FileMode.Open, FileAccess.Read))
                {
                    bytes = new byte[fsSource.Length];
                    int numBytesToRead = (int)fsSource.Length;
                    int numBytesRead = 0;
                    while (numBytesToRead > 0)
                    {
                        int n2 = fsSource.Read(bytes, numBytesRead, numBytesToRead);
                        if (n2 == 0)
                        {
                            fsSource.Close();
                            fsSource.Dispose();
                            break;
                        }
                        numBytesRead += n2;
                        numBytesToRead -= n2;
                    }
                }
                if (delete)
                {
                    FileInfo fi = new FileInfo(fle);
                    fi.Delete();
                }
            }
            catch
            {
            }
            return bytes;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="word"></param>
        /// <returns></returns>
        public static string ScrambleWord(string word)
        {
            char[] chars = new char[word.Length];
            Random rand = new Random(10000);
            int index = 0;
            while (word.Length > 0)
            {
                int next = rand.Next(0, word.Length - 1);
                chars[index] = word[next];
                word = word.Substring(0, next) + word.Substring(next + 1);
                ++index;
            }
            return new String(chars);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectInstance"></param>
        /// <returns></returns>
        public static string SerializeJSON(object objectInstance)
        {
            return JsonConvert.SerializeObject(objectInstance);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="objectInstance"></param>
        /// <returns></returns>
        public static string SerializeXML(object objectInstance)
        {
            string txt = "";
            var emptyNamepsaces = new XmlSerializerNamespaces(new[] { XmlQualifiedName.Empty });
            var serializer = new XmlSerializer(objectInstance.GetType());
            var settings = new XmlWriterSettings
            {
                OmitXmlDeclaration = false,
                Encoding = new UTF8Encoding(false),
                ConformanceLevel = ConformanceLevel.Document
            };
            var memoryStream = new MemoryStream();
            using (var writer = XmlWriter.Create(memoryStream, settings))
            {
                serializer.Serialize(writer, objectInstance, emptyNamepsaces);
                txt = Encoding.UTF8.GetString(memoryStream.ToArray());
            }
            return txt;
        }

        /// <summary>
        /// Shape a list
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="obj"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public static object ShapeList<TSource>(this IList<TSource> obj, string fields)
        {
            List<string> lstOfFields = new List<string>();
            if (string.IsNullOrEmpty(fields))
            {
                return obj;
            }
            lstOfFields = fields.Split(',').ToList();
            List<string> lstOfFieldsToWorkWith = new List<string>(lstOfFields);

            List<System.Dynamic.ExpandoObject> lsobjectToReturn = new List<System.Dynamic.ExpandoObject>();
            if (!lstOfFieldsToWorkWith.Any())
            {
                return obj;
            }
            else
            {
                foreach (var kj in obj)
                {
                    System.Dynamic.ExpandoObject objectToReturn = new System.Dynamic.ExpandoObject();

                    foreach (var field in lstOfFieldsToWorkWith)
                    {
                        try
                        {
                            var fieldValue = kj.GetType()
                            .GetProperty(field.Trim(), BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
                            .GetValue(kj, null);
                            ((IDictionary<String, Object>)objectToReturn).Add(field.Trim(), fieldValue);
                        }
                        catch
                        {
                        }
                    }

                    lsobjectToReturn.Add(objectToReturn);
                }
            }
            return lsobjectToReturn;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static string TimeStampCode(string prefix = "")
        {
            Thread.Sleep(1);
            string stamp = DateTime.Now.ToString("yyMMddHHmmssffffff");
            long num = long.Parse(stamp);

            var g = Guid.NewGuid().ToString().Substring(0, 3).ToUpper();
            return prefix + DecimalToArbitrarySystem(num, 36) + g;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dtm"></param>
        /// <param name="prefix"></param>
        /// <returns></returns>
        public static string TimeStampCode(DateTime dtm, string prefix = "")
        {
            Thread.Sleep(1);
            string stamp = dtm.ToString("yyMMddHHmmssffffff");
            long num = long.Parse(stamp);
            return prefix + DecimalToArbitrarySystem(num, 36);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="datetime"></param>
        /// <returns></returns>
        public static DateTime ToDateTime(string datetime)
        {
            var dtm = DateTime.Now;
            if (!DateTime.TryParse(datetime, out dtm))
            {
                dtm = DefaultDate();
            }
            return dtm;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static double ToDouble(object v)
        {
            double resp = 0;
            try
            {
                resp = Convert.ToDouble(v);
            }
            catch { }
            return resp;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="v"></param>
        /// <returns></returns>
        public static decimal ToDecimal(object v)
        {
            decimal resp = 0;
            try
            {
                resp = Convert.ToDecimal(v);
            }
            catch { }
            return resp;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="txt"></param>
        /// <returns></returns>
        public static Guid ToGuid(string txt)
        {
            try
            {
                return new Guid(txt);
            }
            catch
            {
                return new Guid();
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static int ToInt32(object p)
        {
            int resp = 0;
            try
            {
                resp = Convert.ToInt32(p);
            }
            catch { }
            return resp;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="p"></param>
        /// <returns></returns>
        public static long ToInt64(object p)
        {
            long resp = 0;
            try
            {
                resp = Convert.ToInt64(p);
            }
            catch { }
            return resp;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="s"></param>
        /// <returns></returns>
        public static string UppercaseFirst(string s)
        {
            if (string.IsNullOrEmpty(s))
            {
                return string.Empty;
            }
            return char.ToUpper(s[0]) + s.Substring(1).ToLower();
        }
    }
}
