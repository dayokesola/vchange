﻿using AutoMapper;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Form.Content;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Domain.Model.Content;
using Bootes.Core.Domain.Model.MyChange;

namespace Bootes.Core.Common
{
    /// <summary>
    /// 
    /// </summary>
    public static class AutoMapperConfig
    {
        /// <summary>
        /// Registers the mappings.
        /// </summary>
        public static void RegisterMappings()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.CreateMap<SiteUser, SiteUserModel>().ReverseMap();
                cfg.CreateMap<SiteUser, SiteUserForm>().ReverseMap();
                cfg.CreateMap<SiteUserModel, SiteUserForm>().ReverseMap();

                cfg.CreateMap<App, AppModel>().ReverseMap();
                cfg.CreateMap<App, AppForm>().ReverseMap();
                cfg.CreateMap<AppModel, AppForm>().ReverseMap();

                cfg.CreateMap<EntitySetting, EntitySettingModel>().ReverseMap();
                cfg.CreateMap<EntitySetting, EntitySettingForm>().ReverseMap();
                cfg.CreateMap<EntitySettingModel, EntitySettingForm>().ReverseMap();

                cfg.CreateMap<Corporate, CorporateModel>().ReverseMap();
                cfg.CreateMap<Corporate, CorporateForm>().ReverseMap();
                cfg.CreateMap<CorporateModel, CorporateForm>().ReverseMap();


                cfg.CreateMap<SiteRole, SiteRoleModel>().ReverseMap();
                cfg.CreateMap<SiteRole, SiteRoleForm>().ReverseMap();
                cfg.CreateMap<SiteRoleModel, SiteRoleForm>().ReverseMap();


                cfg.CreateMap<Article, ArticleModel>().ReverseMap();
                cfg.CreateMap<Article, ArticleForm>().ReverseMap();
                cfg.CreateMap<ArticleModel, ArticleForm>().ReverseMap();

                cfg.CreateMap<Location, LocationModel>().ReverseMap();
                cfg.CreateMap<Location, LocationForm>().ReverseMap();
                cfg.CreateMap<LocationModel, LocationForm>().ReverseMap();

                cfg.CreateMap<OutBox, OutBoxModel>().ReverseMap();
                cfg.CreateMap<OutBox, OutBoxForm>().ReverseMap();
                cfg.CreateMap<OutBoxModel, OutBoxForm>().ReverseMap();


                cfg.CreateMap<CorporateRole, CorporateRoleModel>().ReverseMap();
                cfg.CreateMap<CorporateRole, CorporateRoleForm>().ReverseMap();
                cfg.CreateMap<CorporateRoleModel, CorporateRoleForm>().ReverseMap();

                cfg.CreateMap<CorporateUser, CorporateUserModel>().ReverseMap();
                cfg.CreateMap<CorporateUser, CorporateUserForm>().ReverseMap();
                cfg.CreateMap<CorporateUserModel, CorporateUserForm>().ReverseMap();


                cfg.CreateMap<Client, ClientModel>().ReverseMap();
                cfg.CreateMap<Client, ClientForm>().ReverseMap();
                cfg.CreateMap<ClientModel, ClientForm>().ReverseMap();



                cfg.CreateMap<Permission, PermissionModel>().ReverseMap();
                cfg.CreateMap<Permission, PermissionForm>().ReverseMap();
                cfg.CreateMap<PermissionModel, PermissionForm>().ReverseMap();


                cfg.CreateMap<Tag, TagModel>().ReverseMap();
                cfg.CreateMap<Tag, TagForm>().ReverseMap();
                cfg.CreateMap<TagModel, TagForm>().ReverseMap();


                cfg.CreateMap<Wallet, WalletModel>().ReverseMap();
                cfg.CreateMap<Wallet, WalletForm>().ReverseMap();
                cfg.CreateMap<WalletModel, WalletForm>().ReverseMap();


                cfg.CreateMap<WalletTransact, WalletTransactModel>().ReverseMap();
                cfg.CreateMap<WalletTransact, WalletTransactForm>().ReverseMap();
                cfg.CreateMap<WalletTransactModel, WalletTransactForm>().ReverseMap();

                cfg.CreateMap<WalletStatement, WalletStatementModel>().ReverseMap();
                cfg.CreateMap<WalletStatement, WalletStatementForm>().ReverseMap();
                cfg.CreateMap<WalletStatementModel, WalletStatementForm>().ReverseMap();


            });
        }
    }

}
