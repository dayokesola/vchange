using System;
using System.Linq;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Web;
using System.Data;
using System.Text;
using ExcelLibrary.SpreadSheet;

namespace Bootes.Core.Common
{

    public class DataExport
    {
        public static string GenerateRndNumber(int cnt)
        {
            string[] key2 = new string[]
            {
                "0",
                "1",
                "2",
                "3",
                "4",
                "5",
                "6",
                "7",
                "8",
                "9"
            };
            Random rand = new Random();
            string txt = "";
            for (int i = 0; i < cnt; i++)
            {
                txt += key2[rand.Next(0, 9)];
            }
            return txt;
        }

        public static FileInfo DataSetToExcel(DataSet ds, string fle)
        {
            Workbook wb = new Workbook();
            for (int t = 0; t < ds.Tables.Count; t++)
            {
                DataTable dt = ds.Tables[t];
                Worksheet ws = new Worksheet(dt.TableName);
                for (int i = 0; i < dt.Columns.Count; i++)
                {
                    ws.Cells[0, i] = new Cell(dt.Columns[i].ColumnName);
                }
                int cols = dt.Columns.Count;
                int rows = dt.Rows.Count;
                int j = 0;
                for (int k = 0; k < rows; k++)
                {
                    j++;
                    for (int l = 0; l < cols; l++)
                    {
                        string kb = dt.Columns[l].DataType.ToString();
                        try
                        {
                            switch (kb)
                            {
                                case "System.Decimal":
                                    ws.Cells[j, l] = new Cell(Convert.ToDouble(dt.Rows[k][l].ToString()));
                                    break;
                                case "System.Double":
                                    ws.Cells[j, l] = new Cell(Convert.ToDouble(dt.Rows[k][l].ToString()));
                                    break;
                                case "System.Int16":
                                    ws.Cells[j, l] = new Cell(dt.Rows[k][l].ToString(), "#,##0");
                                    break;
                                case "System.Int32":
                                    ws.Cells[j, l] = new Cell(dt.Rows[k][l].ToString(), "#,##0");
                                    break;
                                case "System.Int64":
                                    ws.Cells[j, l] = new Cell(dt.Rows[k][l].ToString(), "#,##0");
                                    break;
                                case "System.DateTime":
                                    ws.Cells[j, l] = new Cell(dt.Rows[k][l].ToString(), "DD-MMM-YYYY");
                                    break;
                                default:
                                    ws.Cells[j, l] = new Cell(dt.Rows[k][l].ToString());
                                    break;
                            }
                        }
                        catch
                        {
                            ws.Cells[j, l] = new Cell("");
                        }
                    }
                }

                wb.Worksheets.Add(ws);
            }


            string pth = Constants.FilesRoot + "";
            if (string.IsNullOrEmpty(fle))
            {
                fle = Util.TimeStampCode("XLS") + ".xls";
            }

            string csvCompletePath = pth + fle;
            fle = DateTime.Now.ToString("yyyyMMddHHmmss") + GenerateRndNumber(4) + ".xls";

            wb.Save(csvCompletePath);
            wb = null;
            var fi = new FileInfo(csvCompletePath);
            return fi;
        }

        public static string generateFileName()
        {
            string[] key = new string[]
            {
                "b",
                "c",
                "d",
                "f",
                "g",
                "h",
                "j",
                "k",
                "m",
                "n",
                "p",
                "q",
                "r",
                "t",
                "v",
                "w",
                "x",
                "y",
                "z"
            };
            Random rand = new Random();
            string nme = "";
            nme += DateTime.Now.ToString("yyMMddHHmmss");
            nme += key[rand.Next(0, 18)];
            nme += key[rand.Next(0, 18)];
            return nme + key[rand.Next(0, 18)];
        }


        public static FileInfo ListToCSV<T>(List<T> list, string fle = "")
        {
            string pth = Constants.FilesRoot + "";
            if (string.IsNullOrEmpty(fle))
            {
                fle = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            }

            string csvCompletePath = pth + fle;
            if (list == null || list.Count == 0)
            {
                throw new Exception("List is Empty");
            }

            //get type from 0th member
            Type t = list[0].GetType();
            string newLine = Environment.NewLine;
            if (!Directory.Exists(Path.GetDirectoryName(csvCompletePath))) Directory.CreateDirectory(Path.GetDirectoryName(csvCompletePath));
            if (!File.Exists(csvCompletePath))
            {
                var mf = File.Create(csvCompletePath);
                mf.Close();
            }
            using (var sw = new StreamWriter(csvCompletePath))
            {
                object o = Activator.CreateInstance(t);
                PropertyInfo[] props = o.GetType().GetProperties(BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance);
                sw.Write(string.Join(",", props.Select(d => d.Name).ToArray()) + newLine);
                foreach (T item in list)
                {
                    var row = string.Join(",", props.Select(d => Util.GetString(item.GetType()
                                                                    .GetProperty(d.Name, BindingFlags.IgnoreCase | BindingFlags.Public | BindingFlags.Instance)
                                                                    .GetValue(item, null)))
                                                            .ToArray());

                    row = row.Replace(Environment.NewLine, "~");
                    sw.Write(row + newLine);
                }
            }



            var fi = new FileInfo(csvCompletePath);
            return fi;

        }


        public static FileInfo DataTableToCSV(DataTable dt, string fle = "")
        {
            string pth = Constants.FilesRoot + "downloads/";
            if (string.IsNullOrEmpty(fle))
            {
                fle = DateTime.Now.ToString("yyyyMMddHHmmss") + ".csv";
            }

            string csvCompletePath = pth + fle;

            File.Delete(csvCompletePath);

            StringBuilder sb = new StringBuilder();

            int cnt = dt.Columns.Count;


            string newLine = Environment.NewLine;
            if (!Directory.Exists(Path.GetDirectoryName(csvCompletePath))) Directory.CreateDirectory(Path.GetDirectoryName(csvCompletePath));
            if (!File.Exists(csvCompletePath))
            {
                var mf = File.Create(csvCompletePath);
                mf.Close();
            }


            using (var sw = new StreamWriter(csvCompletePath))
            {
                sb = new StringBuilder();
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    if (k == cnt - 1)
                        sb.Append(dt.Columns[k].ColumnName);
                    else
                        sb.Append(dt.Columns[k].ColumnName + ',');
                }

                sw.Write(string.Join(",", sb.ToString() + newLine));

                var rcdata = "";

                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    try
                    {
                        sb = new StringBuilder();
                        for (int k = 0; k < dt.Columns.Count; k++)
                        {
                            try
                            {
                                rcdata = dt.Rows[i][k].ToString();
                            }
                            catch { rcdata = ""; }

                            if (k == cnt - 1)
                                sb.Append(rcdata.Replace(",", ";"));
                            else
                                sb.Append(rcdata.Replace(",", ";") + ',');
                        }
                        sb.Replace("\r\n", "\n").Replace("\r", "\n").Replace("\n", "~");
                        sw.Write(sb.ToString() + newLine);
                    }
                    catch { }
                }

            }


            var fi = new FileInfo(csvCompletePath);
            return fi;



        }
    }

}