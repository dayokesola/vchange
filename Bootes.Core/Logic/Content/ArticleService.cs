﻿using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Form.Content;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Content;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Logic.Content
{ 
    /// <summary>
    /// Article Service
    /// </summary>
    public class ArticleService : BaseService<Article, ArticleModel, ArticleForm, int>
    {
        /// <summary>
        /// IQueryable Article Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="title"></param>
        /// <param name="tags"></param>
        /// <param name="content"></param>
        /// <param name="editorType"></param>
        /// <param name="altContents"></param>
        /// <returns></returns>
        public IQueryable<Article> Search(string name = "", string title = "", string tags = "", string content = "", int editorType = 0, string altContents = "")
        {
            return DataModule.Articles.Search(name, title, tags, content, editorType, altContents);
        }


        /// <summary>
        /// IEnumerable Article Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="title"></param>
        /// <param name="tags"></param>
        /// <param name="content"></param>
        /// <param name="editorType"></param>
        /// <param name="altContents"></param>
        /// <returns></returns>
        public IEnumerable<ArticleModel> SearchModel(string name = "", string title = "", string tags = "", string content = "", int editorType = 0, string altContents = "")
        {
            return DataModule.Articles.Search(name, title, tags, content, editorType, altContents)
                .Select(FactoryModule.Articles.CreateModel);
        }


        /// <summary>
        /// Paged Article Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="title"></param>
        /// <param name="tags"></param>
        /// <param name="content"></param>
        /// <param name="editorType"></param>
        /// <param name="altContents"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<ArticleModel> SearchView(string name = "", string title = "", string tags = "", string content = "", int editorType = 0, string altContents = "",
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.Articles.SearchView(name, title, tags, content, editorType, altContents, page, pageSize, sort);
        }

        /// <summary>
        /// Create Article Model from Article Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ArticleModel Create(Article entity)
        {
            return FactoryModule.Articles.CreateModel(entity);
        }

        /// <summary>
        /// Create Article Model from Article Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public ArticleModel Create(ArticleForm form)
        {
            return FactoryModule.Articles.CreateModel(form);
        }

        /// <summary>
        /// Create Article Entity from Article Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Article Create(ArticleModel model)
        {
            return FactoryModule.Articles.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of Article before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(ArticleModel model)
        {
            return DataModule.Articles.ItemExists(model);
        }

        /// <summary>
        /// Delete Article
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(Article entity)
        {
            return DataModule.Articles.Delete(entity);
        }

        /// <summary>
        /// Get Article Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Article Get(int id)
        {
            return DataModule.Articles.Get(id);
        }



        /// <summary>
        /// Get Article Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ArticleModel GetModel(int id)
        {
            return DataModule.Articles.GetModel(id);
        }

        /// <summary>
        /// Insert new Article to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public ArticleModel Insert(ArticleModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.Articles.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("Article Name already exists");
                }
            }
            var entity = FactoryModule.Articles.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.Articles.Insert(entity);
            return FactoryModule.Articles.CreateModel(entity);
        }

        /// <summary>
        /// Update a Article Entity with a Article Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public Article Patch(Article entity, ArticleModel model, string fields)
        {
            return FactoryModule.Articles.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update Article, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public ArticleModel Update(Article entity, ArticleModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.Articles.CreateModel(DataModule.Articles.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of Article before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(ArticleModel model)
        {
            return DataModule.Articles.ItemExists(model, model.Id);
        }

    }


}
