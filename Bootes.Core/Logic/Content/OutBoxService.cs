using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Form.Content;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Domain.Model.Content;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Logic.Content
{

    /// <summary>
    /// OutBox Service
    /// </summary>
    public partial class OutBoxService : BaseService<OutBox, OutBoxModel, OutBoxForm, long>
    {
        public void SendMessage(OutBoxModel outbox)
        {
            outbox.RecordStatus = Domain.Enum.RecordStatus.Active;
            if (outbox.SentRemarks != "MOCKED")
            {
                outbox.IsSent = MailUtil.SendMail(outbox.ToAddress, outbox.Subject, outbox.Contents);
            }
            else
            {
                outbox.IsSent = true;
            }
            outbox.SentAt = Util.CurrentDateTime();
            if (outbox.IsSent)
            {
                outbox.SentRemarks = "OK";
            }
            else
            {
                outbox.SentRemarks = "FAILED";
            }
            Insert(outbox);
        }

        public void SendSystemMessage(string title, string message, string user)
        {
            var outbox = new OutBoxModel()
            {
                Contents = message,
                FromAddress = user,
                MessageCode = "SYSTEM_ALERT",
                MessageTypeId = 0,
                ToAddress = "support@" + Constants.APP_DOMAIN,
                Subject = title
            };
            outbox.RecordStatus = Domain.Enum.RecordStatus.Active;
            outbox.SentAt = Util.CurrentDateTime();
            outbox.IsSent = true;
            outbox.SentRemarks = "MOCKED";
            SendMessage(outbox);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="txId"></param>
        /// <param name="receiveFlag">Can be CREDIT, DEBIT, BOTH, NONE, SYSTEM</param>
        public void NotifyTransaction(long txId, string receiveFlag)
        {
            //thread this
        }

        public void MailChangedPassword(SiteUserModel user)
        {

        }

        public void MailLogin(SiteUserModel user)
        { 
        }

        public void MailSignUp(SiteUserModel user)
        {

        }

        public bool MailResetPassword(SiteUserModel user, string token)
        {
            var article = Logic.ArticleService.SearchModel("RESETPWD_MAIL").FirstOrDefault();
            if (article == null) throw new Exception("Reset Password template not found");
            var outbox = FactoryModule.OutBoxes.CreateMailResetPassword(user, token, article);
            Logic.OutBoxService.SendMessage(outbox);
            return true;
        }
    }

    /// <summary>
    /// OutBox Service
    /// </summary>
    public partial class OutBoxService : BaseService<OutBox, OutBoxModel, OutBoxForm, long>
    {
        /// <summary>
        /// IQueryable OutBox Entity Search
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="messageCode"></param>
        /// <param name="messageTypeId"></param>
        /// <param name="scheduledAt"></param>
        /// <param name="contents"></param>
        /// <param name="attachments"></param>
        /// <param name="isSent"></param>
        /// <param name="sentAt"></param>
        /// <param name="sentRemarks"></param>
        /// <returns></returns>
        public IQueryable<OutBox> Search(string toAddress = "", string fromAddress = "", string subject = "", string messageCode = "", int messageTypeId = 0, DateTime? scheduledAt = null, string contents = "", string attachments = "", bool? isSent = null, DateTime? sentAt = null, string sentRemarks = "")
        {
            return DataModule.OutBoxes.Search(toAddress, fromAddress, subject, messageCode, messageTypeId, scheduledAt, contents, attachments, isSent, sentAt, sentRemarks);
        }


        /// <summary>
        /// IEnumerable OutBox Model Search
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="messageCode"></param>
        /// <param name="messageTypeId"></param>
        /// <param name="scheduledAt"></param>
        /// <param name="contents"></param>
        /// <param name="attachments"></param>
        /// <param name="isSent"></param>
        /// <param name="sentAt"></param>
        /// <param name="sentRemarks"></param>
        /// <returns></returns>
        public IEnumerable<OutBoxModel> SearchModel(string toAddress = "", string fromAddress = "", string subject = "", string messageCode = "", int messageTypeId = 0, DateTime? scheduledAt = null, string contents = "", string attachments = "", bool? isSent = null, DateTime? sentAt = null, string sentRemarks = "")
        {
            return DataModule.OutBoxes.Search(toAddress, fromAddress, subject, messageCode, messageTypeId, scheduledAt, contents, attachments, isSent, sentAt, sentRemarks)
                .Select(FactoryModule.OutBoxes.CreateModel);
        }


        /// <summary>
        /// Paged OutBox Model Search
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="messageCode"></param>
        /// <param name="messageTypeId"></param>
        /// <param name="scheduledAt"></param>
        /// <param name="contents"></param>
        /// <param name="attachments"></param>
        /// <param name="isSent"></param>
        /// <param name="sentAt"></param>
        /// <param name="sentRemarks"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<OutBoxModel> SearchView(string toAddress = "", string fromAddress = "", string subject = "", string messageCode = "", int messageTypeId = 0, DateTime? scheduledAt = null, string contents = "", string attachments = "", bool? isSent = null, DateTime? sentAt = null, string sentRemarks = "",
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.OutBoxes.SearchView(toAddress, fromAddress, subject, messageCode, messageTypeId, scheduledAt, contents, attachments, isSent, sentAt, sentRemarks, page, pageSize, sort);
        }

        /// <summary>
        /// Create OutBox Model from OutBox Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public OutBoxModel Create(OutBox entity)
        {
            return FactoryModule.OutBoxes.CreateModel(entity);
        }

        /// <summary>
        /// Create OutBox Model from OutBox Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public OutBoxModel Create(OutBoxForm form)
        {
            return FactoryModule.OutBoxes.CreateModel(form);
        }

        /// <summary>
        /// Create OutBox Entity from OutBox Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public OutBox Create(OutBoxModel model)
        {
            return FactoryModule.OutBoxes.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of OutBox before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(OutBoxModel model)
        {
            return DataModule.OutBoxes.ItemExists(model);
        }

        /// <summary>
        /// Delete OutBox
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(OutBox entity)
        {
            return DataModule.OutBoxes.Delete(entity);
        }

        /// <summary>
        /// Get OutBox Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OutBox Get(long id)
        {
            return DataModule.OutBoxes.Get(id);
        }



        /// <summary>
        /// Get OutBox Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public OutBoxModel GetModel(long id)
        {
            return DataModule.OutBoxes.GetModel(id);
        }

        /// <summary>
        /// Insert new OutBox to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public OutBoxModel Insert(OutBoxModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.OutBoxes.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("OutBox Name already exists");
                }
            }
            var entity = FactoryModule.OutBoxes.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.OutBoxes.Insert(entity);
            return FactoryModule.OutBoxes.CreateModel(entity);
        }

        /// <summary>
        /// Update a OutBox Entity with a OutBox Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public OutBox Patch(OutBox entity, OutBoxModel model, string fields)
        {
            return FactoryModule.OutBoxes.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update OutBox, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public OutBoxModel Update(OutBox entity, OutBoxModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.OutBoxes.CreateModel(DataModule.OutBoxes.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of OutBox before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(OutBoxModel model)
        {
            return DataModule.OutBoxes.ItemExists(model, model.Id);
        }

    }



}