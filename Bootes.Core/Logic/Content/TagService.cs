using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Form.Content;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Domain.Model.Content;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Logic.Content
{


    /// <summary>
    /// Tag Service
    /// </summary>
    public partial class TagService : BaseService<Tag, TagModel, TagForm, long>
    {

        public void AddTags(string tags, int id, string objecttype)
        {
            if (string.IsNullOrEmpty(tags)) return;
            //var _tags = Search(objectType: objecttype, objectId: id);
            //foreach (var _tag in _tags)
            //{
            //    Delete(_tag);
            //}


            DataModule.Tags.DeleteByObject(id, objecttype);

            char[] sep = { ',', ';' };
            var bits = tags.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            foreach (var bit in bits)
            {
                var tag = new TagModel()
                {
                    ObjectType = objecttype,
                    ObjectId = id,
                    TagName = bit.ToLower().Trim()
                };
                Insert(tag);
            }
        }

        public List<TagFrequency> TagFrequencies(string objectType)
        {
            return DataModule.Tags.TagFrequenies(objectType);
        }

        public string CleanTags(string tags)
        {
            var txt = new List<string>();
            tags = tags.Replace('"', ' ');
            tags = tags.Replace("'", " ");
            char[] sep = { ',', ';', '-' };
            var bits = tags.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            foreach (var bit in bits)
            {
                txt.Add(bit.ToLower().Trim());
            }
            return string.Join(";", txt.ToArray());
        }

        public void MerchantUpdateOrAdd(CorporateModel dto, string tags)
        {
            var txt = Tokenize(dto.Name);
            txt += Tokenize(tags);
            txt += Tokenize(dto.PublicName); 
            txt += Tokenize(dto.CountryCode); 
            var ls = ListOut(txt); 
            TagData("merchant", dto.Id, ls);
        }

        public void TagData(string objectType, long objectId, List<string> list)
        {
            var tags_home = Logic.TagService.SearchView(objectType: objectType, objectId: objectId, page: 0).Items;

            foreach (var l in list)
            {
                var tag = tags_home.FirstOrDefault(x => x.TagName == l);
                if (tag == null)
                {
                    var k = Insert(new TagModel()
                    {
                        ObjectId = objectId,
                        ObjectType = objectType,
                        TagName = l.ToLower(),
                        RecordStatus = Domain.Enum.RecordStatus.Active
                    }, false);

                    tags_home.Add(k);
                }
            }
             

            foreach (var t in tags_home)
            {
                var item = list.FirstOrDefault(x => x == t.TagName);
                if (string.IsNullOrEmpty(item))
                {
                    Logic.TagService.DeleteById(t.Id);
                }
            }
        }

        private List<string> ListOut(string txt)
        { 
            char[] sep = { ',', ' ', ';' };
            var bits = txt.Split(sep, StringSplitOptions.RemoveEmptyEntries);
            var items = new List<string>();
            foreach(var bit in bits)
            {
                if (!items.Contains(bit))
                {
                    items.Add(bit);
                }
            }
            return items;
        }

        private string Tokenize(string code)
        {
            var txt = "";
            if (!string.IsNullOrEmpty(code))
            {
                char[] sep = { ',', ' ', ';' };
                var bits = code.Split(sep, StringSplitOptions.RemoveEmptyEntries);
                if (bits != null && bits.Any())
                {
                    txt = string.Join(",", bits);
                }
                return txt.ToLower() + ",";
            }
            else
            {

            }
            return txt;
        }
    }

    /// <summary>
    /// Tag Service
    /// </summary>
    public partial class TagService : BaseService<Tag, TagModel, TagForm, long>
    {
        /// <summary>
        /// IQueryable Tag Entity Search
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="objectId"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public IQueryable<Tag> Search(string objectType = "", long objectId = 0, string tagName = "")
        {
            return DataModule.Tags.Search(objectType, objectId, tagName);
        }


        /// <summary>
        /// IEnumerable Tag Model Search
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="objectId"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public IEnumerable<TagModel> SearchModel(string objectType = "", long objectId = 0, string tagName = "")
        {
            return DataModule.Tags.Search(objectType, objectId, tagName)
                .Select(FactoryModule.Tags.CreateModel);
        }


        /// <summary>
        /// Paged Tag Model Search
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="objectId"></param>
        /// <param name="tagName"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<TagModel> SearchView(string objectType = "", long objectId = 0, string tagName = "",
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.Tags.SearchView(objectType, objectId, tagName, page, pageSize, sort);
        }

        /// <summary>
        /// Create Tag Model from Tag Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public TagModel Create(Tag entity)
        {
            return FactoryModule.Tags.CreateModel(entity);
        }

        /// <summary>
        /// Create Tag Model from Tag Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public TagModel Create(TagForm form)
        {
            return FactoryModule.Tags.CreateModel(form);
        }

        /// <summary>
        /// Create Tag Entity from Tag Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Tag Create(TagModel model)
        {
            return FactoryModule.Tags.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of Tag before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(TagModel model)
        {
            return DataModule.Tags.ItemExists(model);
        }

        /// <summary>
        /// Delete Tag
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(Tag entity)
        {
            return DataModule.Tags.DeleteFinally(entity);
        }
        public int DeleteById(long id)
        {
            return DataModule.Tags.DeleteById(id);
        }
        /// <summary>
        /// Get Tag Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Tag Get(long id)
        {
            return DataModule.Tags.Get(id);
        }



        /// <summary>
        /// Get Tag Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public TagModel GetModel(long id)
        {
            return DataModule.Tags.GetModel(id);
        }

        /// <summary>
        /// Insert new Tag to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public TagModel Insert(TagModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.Tags.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("Tag Name already exists");
                }
            }
            var entity = FactoryModule.Tags.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.Tags.Insert(entity);
            return FactoryModule.Tags.CreateModel(entity);
        }

        /// <summary>
        /// Update a Tag Entity with a Tag Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public Tag Patch(Tag entity, TagModel model, string fields)
        {
            return FactoryModule.Tags.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update Tag, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public TagModel Update(Tag entity, TagModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.Tags.CreateModel(DataModule.Tags.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of Tag before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(TagModel model)
        {
            return DataModule.Tags.ItemExists(model, model.Id);
        }

    }
}