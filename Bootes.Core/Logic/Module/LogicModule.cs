﻿using Bootes.Core.Domain.Module;
using Bootes.Core.Logic.Account;
using Bootes.Core.Logic.Cache;
using Bootes.Core.Logic.Content;
using Bootes.Core.Logic.MyChange;

namespace Bootes.Core.Logic.Module
{
    public class LogicModule
    {
        private AppService _app;
        private AppCacheService _appcache;
        private ArticleService _article;
        private ClientService _client;
        private DashboardService _dash;
        private CorporateService _corporate;
        private CorporateRoleService _corporaterole;
        private CorporateUserService _corporateuser;
        private EntitySettingService _entitysetting;
        private FactoryModule _factory;
        private LocationService _location;
        private OutBoxService _outbox;
        private PermissionService _permission;
        private SiteRoleService _siterole;
        private SiteUserService _siteuser;
        private TagService _tag;
        private WalletService _wallet;

        private WalletStatementService _walletstatement;
        private WalletTransactService _wallettransact;
        /// <summary>
        /// AppCacheService Module
        /// </summary>
        public AppCacheService AppCacheService { get { if (_appcache == null) { _appcache = new AppCacheService(); } return _appcache; } }
        public DashboardService DashboardService { get { if (_dash == null) { _dash = new DashboardService(); } return _dash; } }

        /// <summary>
        /// App Service Module
        /// </summary>
        public AppService AppService { get { if (_app == null) { _app = new AppService(); } return _app; } }

        /// <summary>
        /// Article Service Module
        /// </summary>
        public ArticleService ArticleService { get { if (_article == null) { _article = new ArticleService(); } return _article; } }

        /// <summary>
        /// Client Service Module
        /// </summary>
        public ClientService ClientService { get { if (_client == null) { _client = new ClientService(); } return _client; } }

        /// <summary>
        /// CorporateRole Service Module
        /// </summary>
        public CorporateRoleService CorporateRoleService { get { if (_corporaterole == null) { _corporaterole = new CorporateRoleService(); } return _corporaterole; } }

        /// <summary>
        /// Corporate Service Module
        /// </summary>
        public CorporateService CorporateService { get { if (_corporate == null) { _corporate = new CorporateService(); } return _corporate; } }

        /// <summary>
        /// CorporateUser Service Module
        /// </summary>
        public CorporateUserService CorporateUserService { get { if (_corporateuser == null) { _corporateuser = new CorporateUserService(); } return _corporateuser; } }

        /// <summary>
        /// EntitySetting Service Module
        /// </summary>
        public EntitySettingService EntitySettingService { get { if (_entitysetting == null) { _entitysetting = new EntitySettingService(); } return _entitysetting; } }

        /// <summary>
        /// 
        /// </summary>
        public FactoryModule FactoryModule { get { if (_factory == null) { _factory = new FactoryModule(); } return _factory; } }

        /// <summary>
        /// Location Service Module
        /// </summary>
        public LocationService LocationService { get { if (_location == null) { _location = new LocationService(); } return _location; } }

        /// <summary>
        /// OutBox Service Module
        /// </summary>
        public OutBoxService OutBoxService { get { if (_outbox == null) { _outbox = new OutBoxService(); } return _outbox; } }

        /// <summary>
        /// Permission Service Module
        /// </summary>
        public PermissionService PermissionService { get { if (_permission == null) { _permission = new PermissionService(); } return _permission; } }

        /// <summary>
        /// SiteRole Service Module
        /// </summary>
        public SiteRoleService SiteRoleService { get { if (_siterole == null) { _siterole = new SiteRoleService(); } return _siterole; } }

        /// <summary>
        /// SiteUser Service Module
        /// </summary>
        public SiteUserService SiteUserService { get { if (_siteuser == null) { _siteuser = new SiteUserService(); } return _siteuser; } }

        /// <summary>
        /// Tag Service Module
        /// </summary>
        public TagService TagService { get { if (_tag == null) { _tag = new TagService(); } return _tag; } }

        /// <summary>
        /// Wallet Service Module
        /// </summary>
        public WalletService WalletService { get { if (_wallet == null) { _wallet = new WalletService(); } return _wallet; } }

        /// <summary>
        /// WalletStatement Service Module
        /// </summary>
        public WalletStatementService WalletStatementService { get { if (_walletstatement == null) { _walletstatement = new WalletStatementService(); } return _walletstatement; } }
        /// <summary>
        /// WalletTransact Service Module
        /// </summary>
        public WalletTransactService WalletTransactService { get { if (_wallettransact == null) { _wallettransact = new WalletTransactService(); } return _wallettransact; } }
    }
}
