using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Domain.Model.MyChange;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Logic.MyChange
{
    public class DashboardService : BaseSingleService
    {
        public DashboardHomeModel HomePage()
        {
            var model = new DashboardHomeModel();

            //count merchants
            var sql = "";

            sql = "select count(id) from bts_corporate where corporatestatus = 2";
            model.MerchantsCount = DataModule.Corporates.GetScalar<int>(sql);

            sql = "select count(id) from bts_siteuser where userstatusid = 2";
            model.SubscribersCount = DataModule.Corporates.GetScalar<int>(sql);


            return model;
        }
    }
}