using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.MyChange;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Logic.MyChange
{





    /// <summary>
    /// WalletStatement Service
    /// </summary>
    public partial class WalletStatementService : BaseService<WalletStatement, WalletStatementModel, WalletStatementForm, long>
    {

    }

    /// <summary>
    /// WalletStatement Service
    /// </summary>
    public partial class WalletStatementService : BaseService<WalletStatement, WalletStatementModel, WalletStatementForm, long>
    {
        /// <summary>
        /// IQueryable WalletStatement Entity Search
        /// </summary>
        /// <param name="walletTransactId"></param>
        /// <param name="walletId"></param>
        /// <param name="amount"></param>
        /// <param name="currentBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="drCr"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactionCodeId"></param>
        /// <returns></returns>
        public IQueryable<WalletStatement> Search(long walletTransactId = 0, long walletId = 0, decimal amount = 0, decimal currentBalance = 0, int currencyId = 0, int drCr = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "", string sourceId = "", int transactionCodeId = 0)
        {
            return DataModule.WalletStatements.Search(walletTransactId, walletId, amount, currentBalance, currencyId, drCr, valueDate, paymentReference, narration, sourceId, transactionCodeId);
        }


        /// <summary>
        /// IEnumerable WalletStatement Model Search
        /// </summary>
        /// <param name="walletTransactId"></param>
        /// <param name="walletId"></param>
        /// <param name="amount"></param>
        /// <param name="currentBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="drCr"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactionCodeId"></param>
        /// <returns></returns>
        public IEnumerable<WalletStatementModel> SearchModel(long walletTransactId = 0, long walletId = 0, decimal amount = 0, decimal currentBalance = 0, int currencyId = 0, int drCr = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "", string sourceId = "", int transactionCodeId = 0)
        {
            return DataModule.WalletStatements.Search(walletTransactId, walletId, amount, currentBalance, currencyId, drCr, valueDate, paymentReference, narration, sourceId, transactionCodeId)
                .Select(FactoryModule.WalletStatements.CreateModel);
        }


        /// <summary>
        /// Paged WalletStatement Model Search
        /// </summary>
        /// <param name="walletTransactId"></param>
        /// <param name="walletId"></param>
        /// <param name="amount"></param>
        /// <param name="currentBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="drCr"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactionCodeId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<WalletStatementModel> SearchView(long walletTransactId = 0, long walletId = 0, decimal amount = 0, 
            decimal currentBalance = 0, int currencyId = 0, int drCr = 0, DateTime? valueDate = null, 
            string paymentReference = "", string narration = "", string sourceId = "", int transactionCodeId = 0, 
            long ownerId = 0, int walletTypeId = 0,
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.WalletStatements.SearchView(walletTransactId, walletId, amount, currentBalance, currencyId, drCr, 
                valueDate, paymentReference, narration, sourceId, transactionCodeId, ownerId, walletTypeId, page, pageSize, sort);
        }

        /// <summary>
        /// Create WalletStatement Model from WalletStatement Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WalletStatementModel Create(WalletStatement entity)
        {
            return FactoryModule.WalletStatements.CreateModel(entity);
        }

        /// <summary>
        /// Create WalletStatement Model from WalletStatement Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public WalletStatementModel Create(WalletStatementForm form)
        {
            return FactoryModule.WalletStatements.CreateModel(form);
        }

        /// <summary>
        /// Create WalletStatement Entity from WalletStatement Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public WalletStatement Create(WalletStatementModel model)
        {
            return FactoryModule.WalletStatements.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of WalletStatement before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(WalletStatementModel model)
        {
            return DataModule.WalletStatements.ItemExists(model);
        }

        /// <summary>
        /// Delete WalletStatement
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(WalletStatement entity)
        {
            return DataModule.WalletStatements.Delete(entity);
        }

        /// <summary>
        /// Get WalletStatement Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WalletStatement Get(long id)
        {
            return DataModule.WalletStatements.Get(id);
        }



        /// <summary>
        /// Get WalletStatement Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WalletStatementModel GetModel(long id)
        {
            return DataModule.WalletStatements.GetModel(id);
        }

        /// <summary>
        /// Insert new WalletStatement to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public WalletStatementModel Insert(WalletStatementModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.WalletStatements.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("WalletStatement Name already exists");
                }
            }
            var entity = FactoryModule.WalletStatements.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.WalletStatements.Insert(entity);
            return FactoryModule.WalletStatements.CreateModel(entity);
        }

        /// <summary>
        /// Update a WalletStatement Entity with a WalletStatement Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public WalletStatement Patch(WalletStatement entity, WalletStatementModel model, string fields)
        {
            return FactoryModule.WalletStatements.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update WalletStatement, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public WalletStatementModel Update(WalletStatement entity, WalletStatementModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.WalletStatements.CreateModel(DataModule.WalletStatements.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of WalletStatement before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(WalletStatementModel model)
        {
            return DataModule.WalletStatements.ItemExists(model, model.Id);
        }

    }
}