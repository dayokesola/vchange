﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Domain.Model.MyChange;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Logic.MyChange
{
    /// <summary>
    /// Wallet Service
    /// </summary>
    public partial class WalletService : BaseService<Wallet, WalletModel, WalletForm, long>
    {
        public WalletModel CreateMerchantWallet(CorporateModel entity, int currency = 1)
        {
            var wallet = FactoryModule.Wallets.DefaultModel();
            wallet.Code = Util.TimeStampCode("WALT");
            wallet.CurrencyId = currency;
            wallet.OwnerId = entity.Id;
            wallet.WalletTypeId = 2;
            wallet.Name = entity.PublicName + " Wallet";
            return Insert(wallet);
        }

        public WalletModel CreateCustomerWallet(SiteUserModel entity, int currency = 1)
        {
            var wallet = FactoryModule.Wallets.DefaultModel();
            wallet.Code = Util.TimeStampCode("WALT");
            wallet.CurrencyId = currency;
            wallet.OwnerId = entity.Id;
            wallet.WalletTypeId = 3;
            wallet.Name = entity.UserName + " Wallet";
            return Insert(wallet);
        }

        public WalletModel CreateSystemWallet(string name, int currency = 1)
        {
            var wallet = FactoryModule.Wallets.DefaultModel();
            wallet.Code = Util.TimeStampCode("WALT");
            wallet.CurrencyId = currency;
            wallet.OwnerId = 0;
            wallet.WalletTypeId = 1;
            wallet.Name = name + " Wallet";
            return Insert(wallet);
        }

        public WalletModel GetSystemWallet(int currencyId, int transactCodeId)
        {
            var syswallet = SearchModel(currencyId: currencyId, walletTypeId: 1).FirstOrDefault();
            if (syswallet == null)
            {
                var cur = Logic.EntitySettingService.Search(entityType: "CURRENCY", entityRef: currencyId, sKey: "CODE").FirstOrDefault();
                if (cur == null)
                {
                    throw new Exception("Currency not set for code: " + currencyId);
                }
                syswallet = CreateSystemWallet("VChange Suspense " + cur.SVal, currencyId);
            }
            return syswallet;
        }
    }


    /// <summary>
    /// Wallet Service
    /// </summary>
    public partial class WalletService : BaseService<Wallet, WalletModel, WalletForm, long>
    {
        /// <summary>
        /// IQueryable Wallet Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="walletTypeId"></param>
        /// <param name="currentBalance"></param>
        /// <param name="openingBalance"></param>
        /// <param name="balanceLimit"></param>
        /// <param name="heldBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        public IQueryable<Wallet> Search(string name = "", string code = "", int walletTypeId = 0, decimal currentBalance = 0, decimal openingBalance = 0, decimal balanceLimit = 0, decimal heldBalance = 0, int currencyId = 0, int ownerId = 0)
        {
            return DataModule.Wallets.Search(name, code, walletTypeId, currentBalance, openingBalance, balanceLimit, heldBalance, currencyId, ownerId);
        }


        /// <summary>
        /// IEnumerable Wallet Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="walletTypeId"></param>
        /// <param name="currentBalance"></param>
        /// <param name="openingBalance"></param>
        /// <param name="balanceLimit"></param>
        /// <param name="heldBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        public IEnumerable<WalletModel> SearchModel(string name = "", string code = "", int walletTypeId = 0, decimal currentBalance = 0, decimal openingBalance = 0, decimal balanceLimit = 0, decimal heldBalance = 0, int currencyId = 0, int ownerId = 0)
        {
            return DataModule.Wallets.Search(name, code, walletTypeId, currentBalance, openingBalance, balanceLimit, heldBalance, currencyId, ownerId)
                .Select(FactoryModule.Wallets.CreateModel);
        }


        /// <summary>
        /// Paged Wallet Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="walletTypeId"></param>
        /// <param name="currentBalance"></param>
        /// <param name="openingBalance"></param>
        /// <param name="balanceLimit"></param>
        /// <param name="heldBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="ownerId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<WalletModel> SearchView(string name = "", string code = "", int walletTypeId = 0,
            decimal currentBalance = 0, decimal openingBalance = 0, decimal balanceLimit = 0, decimal heldBalance = 0,
            int currencyId = 0, long ownerId = 0,
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.Wallets.SearchView(name, code, walletTypeId, currentBalance, openingBalance, balanceLimit, heldBalance, currencyId, ownerId, page, pageSize, sort);
        }

        /// <summary>
        /// Create Wallet Model from Wallet Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WalletModel Create(Wallet entity)
        {
            return FactoryModule.Wallets.CreateModel(entity);
        }

        /// <summary>
        /// Create Wallet Model from Wallet Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public WalletModel Create(WalletForm form)
        {
            return FactoryModule.Wallets.CreateModel(form);
        }

        /// <summary>
        /// Create Wallet Entity from Wallet Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Wallet Create(WalletModel model)
        {
            return FactoryModule.Wallets.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of Wallet before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(WalletModel model)
        {
            return DataModule.Wallets.ItemExists(model);
        }

        /// <summary>
        /// Delete Wallet
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(Wallet entity)
        {
            return DataModule.Wallets.Delete(entity);
        }

        /// <summary>
        /// Get Wallet Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Wallet Get(long id)
        {
            return DataModule.Wallets.Get(id);
        }



        /// <summary>
        /// Get Wallet Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WalletModel GetModel(long id)
        {
            return DataModule.Wallets.GetModel(id);
        }

        /// <summary>
        /// Insert new Wallet to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public WalletModel Insert(WalletModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.Wallets.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("Wallet Name already exists");
                }
            }
            var entity = FactoryModule.Wallets.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.Wallets.Insert(entity);
            return FactoryModule.Wallets.CreateModel(entity);
        }

        /// <summary>
        /// Update a Wallet Entity with a Wallet Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public Wallet Patch(Wallet entity, WalletModel model, string fields)
        {
            return FactoryModule.Wallets.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update Wallet, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public WalletModel Update(Wallet entity, WalletModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.Wallets.CreateModel(DataModule.Wallets.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of Wallet before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(WalletModel model)
        {
            return DataModule.Wallets.ItemExists(model, model.Id);
        }

    }


}
