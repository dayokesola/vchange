﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Form.Accounts;
using Bootes.Core.Domain.Form.MyChange;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.MyChange;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Logic.MyChange
{



    /// <summary>
    /// WalletTransact Service
    /// </summary>
    public partial class WalletTransactService : BaseService<WalletTransact, WalletTransactModel, WalletTransactForm, long>
    {
        public int Post(long transactId, bool notifyDr = false, bool notifyCr = false)
        {
            try
            {
                int k =  DataModule.WalletTransacts.Post(transactId);

                if(notifyCr)
                {
                    Logic.OutBoxService.NotifyTransaction(transactId, "CREDIT");
                }

                if (notifyDr)
                {
                    Logic.OutBoxService.NotifyTransaction(transactId, "DEBIT");
                }

                return k;
            }
            catch
            {
                return -2;
            }
        }

        public bool IsValidatable(WalletTransactModel model)
        {
            var validateStatuses = new List<int>
            {
                1,
                2,
                3
            };
            return validateStatuses.Contains(model.AuthStatusId);
        }

        public int Transfer(WalletTransactModel form)
        {
            int v = -1;
            try
            { 
                var k = Insert(form);
                if(k.Id > 0)
                {
                    v = Post(k.Id);
                }

            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
            return v;
        }

        public bool IsCancellable(WalletTransactModel model)
        {
            var cancellableStatuses = new List<int>
            {
                1,
                2
            };
            return cancellableStatuses.Contains(model.AuthStatusId);
        }


        public WalletTransactModel GiveChange(GiveChangeForm form)
        {
            //
            if (form.Amount <= 0)
            {
                throw new Exception("Invalid Amount");
            }
            //validate merchant
            var merchant = Logic.CorporateService.GetModel(form.MerchantId);
            if (merchant == null)
            {
                throw new Exception("No merchant found");
            }
            //get merchants wallet
            var merchantWallet = Logic.WalletService.SearchView(walletTypeId: 2, ownerId: form.MerchantId, page: 0).Items.FirstOrDefault();
            if (merchantWallet == null)
            {
                throw new Exception("No DR wallet found");
            }
            //validate mobile
            if (!Util.IsMobile(form.Mobile, merchant.CountryCode))
            {
                throw new Exception("Invalid Mobile for " + merchant.CountryName);
            }
            //get user  by mobile
            var user = Logic.SiteUserService.SearchView(mobile: form.Mobile, page: 0).Items.FirstOrDefault();
            var userWallet = Logic.FactoryModule.Wallets.DefaultModel();
            if (user == null)
            {
                var pin = Util.RandomNumber(4);
                var pwd = Util.RandomUpper(3) + Util.RandomNumber(3) + Util.RandomLower(3) + Util.RandomChars(3);
                var signupform = new SignUpForm()
                {
                    CountryId = merchant.CountryId,
                    Email = form.Mobile + "@" + Constants.APP_DOMAIN,
                    FirstName = "User",
                    LastName = form.Mobile,
                    Mobile = form.Mobile,
                    Password = pwd,
                    Pin = pin,
                    Pin2 = pin,
                    Password2 = pwd,
                    PublicName = form.Mobile
                };  
                var verdict = Logic.SiteUserService.SignUp(signupform, out user); 
                if(verdict == Domain.Enum.SignUpStatus.Ok || verdict == Domain.Enum.SignUpStatus.OkDoReset)
                { }
                else
                {
                    Log.Error(new Exception("Could not profile new mobile: " + form.Mobile + "; " + verdict.ToString()));
                    throw new Exception("Could not profile new mobile");
                } 
            }

            if(userWallet.Id <= 0)
            {
                //get user wallet 
                userWallet = Logic.WalletService.SearchView(walletTypeId: 3, currencyId: merchantWallet.CurrencyId, ownerId: user.Id, page: 0)
                    .Items.FirstOrDefault(); 
            }

            if(userWallet == null || userWallet.Id <= 0)
            {
                throw new Exception("Could not get beneficiary wallet");
            }
             
            //Now I have user, merchant, userwallet, merchantwallet 
            var model = FactoryModule.WalletTransacts.CreateModel(form, merchantWallet, userWallet);
            model.AuthStatusId = 5;
            model.ValueDate = Util.CurrentDateTime().Date;
            model = Insert(model, false);
            int k = Post(model.Id, false, form.SendAlert); 
            var tx = Get(model.Id);
            tx.AuthDate = Util.CurrentDateTime();
            tx.GateWayReference = "W2W" + tx.TxId; 
            if(k != 1)
            { 
                tx.AuthStatusId = 4; 
                tx.GateWayRemark = "ERROR: " + k.ToString(); 
            }
            else
            {
                tx.AuthStatusId = 6;
                tx.GateWayRemark = "SUCCESS: " + k.ToString();
            }
            Update(tx); 
            return GetModel(model.Id);
        }

        public WalletTransactModel RequestChange(RequestChangeForm form)
        {
            if(form.Amount <= 0)
            { 
                throw new Exception("Invalid Amount");
            }

            var merchant = Logic.CorporateService.Get(form.MerchantId);
            if (merchant == null)
            {
                throw new Exception("Invalid merchant");
            }
            var merchantWallet = Logic.WalletService.SearchView(walletTypeId: 2, ownerId: form.MerchantId, page: 0).Items.FirstOrDefault();
            if (merchantWallet == null)
            {
                throw new Exception("No DR wallet found");
            }

            var user = Logic.SiteUserService.GetByMobile(form.Mobile);
            if(user == null)
            {
                throw new Exception("Invalid mobile user");
            }
            var userWallet = Logic.WalletService.SearchView(walletTypeId: 3, ownerId: user.Id, page: 0).Items.FirstOrDefault();
            if (userWallet == null)
            {
                throw new Exception("No CR wallet found");
            }
            var model = FactoryModule.WalletTransacts.CreateModel(form, merchantWallet, userWallet);
            return model;
        }
    }


    /// <summary>
    /// WalletTransact Service
    /// </summary>
    public partial class WalletTransactService : BaseService<WalletTransact, WalletTransactModel, WalletTransactForm, long>
    {
        /// <summary>
        /// IQueryable WalletTransact Entity Search
        /// </summary>
        /// <param name="drWalletId"></param>
        /// <param name="crWalletId"></param>
        /// <param name="amount"></param>
        /// <param name="currencyId"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="initId"></param>
        /// <param name="initDate"></param>
        /// <param name="authId"></param>
        /// <param name="authDate"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactCodeId"></param>
        /// <returns></returns>
        public IQueryable<WalletTransact> Search(long drWalletId = 0, long crWalletId = 0, decimal amount = 0, int currencyId = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "", long initId = 0, DateTime? initDate = null, long authId = 0, DateTime? authDate = null, string sourceId = "", int transactCodeId = 0)
        {
            return DataModule.WalletTransacts.Search(drWalletId, crWalletId, amount, currencyId, valueDate, paymentReference, narration, initId, initDate, authId, authDate, sourceId, transactCodeId);
        }


        /// <summary>
        /// IEnumerable WalletTransact Model Search
        /// </summary>
        /// <param name="drWalletId"></param>
        /// <param name="crWalletId"></param>
        /// <param name="amount"></param>
        /// <param name="currencyId"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="initId"></param>
        /// <param name="initDate"></param>
        /// <param name="authId"></param>
        /// <param name="authDate"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactCodeId"></param>
        /// <returns></returns>
        public IEnumerable<WalletTransactModel> SearchModel(long drWalletId = 0, long crWalletId = 0, decimal amount = 0, int currencyId = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "", long initId = 0, DateTime? initDate = null, long authId = 0, DateTime? authDate = null, string sourceId = "", int transactCodeId = 0)
        {
            return DataModule.WalletTransacts.Search(drWalletId, crWalletId, amount, currencyId, valueDate, paymentReference, narration, initId, initDate, authId, authDate, sourceId, transactCodeId)
                .Select(FactoryModule.WalletTransacts.CreateModel);
        }


        /// <summary>
        /// Paged WalletTransact Model Search
        /// </summary>
        /// <param name="drWalletId"></param>
        /// <param name="crWalletId"></param>
        /// <param name="amount"></param>
        /// <param name="currencyId"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="initId"></param>
        /// <param name="initDate"></param>
        /// <param name="authId"></param>
        /// <param name="authDate"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactCodeId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<WalletTransactModel> SearchView(long drWalletId = 0, long crWalletId = 0, decimal amount = 0,
            int currencyId = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "",
            long initId = 0, DateTime? initDate = null, long authId = 0, DateTime? authDate = null,
            string sourceId = "", int transactCodeId = 0, string txId = "", string gatewayRef = "", int authStatusId = 0,
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.WalletTransacts.SearchView(drWalletId, crWalletId, amount, currencyId, valueDate,
                paymentReference, narration, initId, initDate, authId, authDate, sourceId,
                transactCodeId, txId, gatewayRef, authStatusId,
                page, pageSize, sort);
        }

        /// <summary>
        /// Create WalletTransact Model from WalletTransact Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public WalletTransactModel Create(WalletTransact entity)
        {
            return FactoryModule.WalletTransacts.CreateModel(entity);
        }

        /// <summary>
        /// Create WalletTransact Model from WalletTransact Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public WalletTransactModel Create(WalletTransactForm form)
        {
            return FactoryModule.WalletTransacts.CreateModel(form);
        }

        /// <summary>
        /// Create WalletTransact Entity from WalletTransact Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public WalletTransact Create(WalletTransactModel model)
        {
            return FactoryModule.WalletTransacts.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of WalletTransact before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(WalletTransactModel model)
        {
            return DataModule.WalletTransacts.ItemExists(model);
        }

        /// <summary>
        /// Delete WalletTransact
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(WalletTransact entity)
        {
            return DataModule.WalletTransacts.Delete(entity);
        }

        /// <summary>
        /// Get WalletTransact Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WalletTransact Get(long id)
        {
            return DataModule.WalletTransacts.Get(id);
        }



        /// <summary>
        /// Get WalletTransact Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public WalletTransactModel GetModel(long id)
        {
            return DataModule.WalletTransacts.GetModel(id);
        }

        /// <summary>
        /// Insert new WalletTransact to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public WalletTransactModel Insert(WalletTransactModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.WalletTransacts.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("WalletTransact Name already exists");
                }
            }
            var entity = FactoryModule.WalletTransacts.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.WalletTransacts.Insert(entity);
            return FactoryModule.WalletTransacts.CreateModel(entity);
        }

        /// <summary>
        /// Update a WalletTransact Entity with a WalletTransact Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public WalletTransact Patch(WalletTransact entity, WalletTransactModel model, string fields)
        {
            return FactoryModule.WalletTransacts.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update WalletTransact, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public WalletTransactModel Update(WalletTransact entity, WalletTransactModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.WalletTransacts.CreateModel(DataModule.WalletTransacts.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of WalletTransact before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(WalletTransactModel model)
        {
            return DataModule.WalletTransacts.ItemExists(model, model.Id);
        }

    }

}
