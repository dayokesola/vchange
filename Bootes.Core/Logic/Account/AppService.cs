﻿using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Logic.Account
{

    /// <summary>
    /// App Service
    /// </summary>
    public class AppService : BaseService<App, AppModel, AppForm, int>
    {
        /// <summary>
        /// IQueryable App Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="runtimeStatusId"></param>
        /// <param name="availableAt"></param>
        /// <returns></returns>
        public IQueryable<App> Search(string name = "", string code = "", int runtimeStatusId = 0, DateTime? availableAt = null)
        {
            return DataModule.Apps.Search(name, code, runtimeStatusId, availableAt);
        }


        /// <summary>
        /// IEnumerable App Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="runtimeStatusId"></param>
        /// <param name="availableAt"></param>
        /// <returns></returns>
        public IEnumerable<AppModel> SearchModel(string name = "", string code = "", int runtimeStatusId = 0, DateTime? availableAt = null)
        {
            return DataModule.Apps.Search(name, code, runtimeStatusId, availableAt)
                .Select(FactoryModule.Apps.CreateModel);
        }


        /// <summary>
        /// Paged App Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="runtimeStatusId"></param>
        /// <param name="availableAt"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<AppModel> SearchView(string name = "", string code = "", int runtimeStatusId = 0, DateTime? availableAt = null,
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.Apps.SearchView(name, code, runtimeStatusId, availableAt, page, pageSize, sort);
        }

        /// <summary>
        /// Create App Model from App Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public AppModel Create(App entity)
        {
            return FactoryModule.Apps.CreateModel(entity);
        }

        /// <summary>
        /// Create App Model from App Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public AppModel Create(AppForm form)
        {
            return FactoryModule.Apps.CreateModel(form);
        }

        /// <summary>
        /// Create App Entity from App Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public App Create(AppModel model)
        {
            return FactoryModule.Apps.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of App before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(AppModel model)
        {
            return DataModule.Apps.ItemExists(model);
        }

        /// <summary>
        /// Delete App
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(App entity)
        {
            return DataModule.Apps.Delete(entity);
        }

        /// <summary>
        /// Get App Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public App Get(int id)
        {
            return DataModule.Apps.Get(id);
        }



        /// <summary>
        /// Get App Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public AppModel GetModel(int id)
        {
            return DataModule.Apps.GetModel(id);
        }

        /// <summary>
        /// Insert new App to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public AppModel Insert(AppModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.Apps.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("App Name already exists");
                }
            }
            var entity = FactoryModule.Apps.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.Apps.Insert(entity);
            return FactoryModule.Apps.CreateModel(entity);
        }

        /// <summary>
        /// Update a App Entity with a App Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public App Patch(App entity, AppModel model, string fields)
        {
            return FactoryModule.Apps.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update App, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public AppModel Update(App entity, AppModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.Apps.CreateModel(DataModule.Apps.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of App before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(AppModel model)
        {
            return DataModule.Apps.ItemExists(model, model.Id);
        }

    }








}
