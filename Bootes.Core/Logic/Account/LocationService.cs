using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Logic.Account
{

    /// <summary>
    /// Location Service
    /// </summary>
    public class LocationService : BaseService<Location, LocationModel, LocationForm, int>
    {
        /// <summary>
        /// IQueryable Location Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <param name="parentId"></param>
        /// <param name="locationTypeId"></param>
        /// <returns></returns>
        public IQueryable<Location> Search(string name = "", string code = "", string info = "", int parentId = 0, int locationTypeId = 0)
        {
            return DataModule.Locations.Search(name, code, info, parentId, locationTypeId);
        }


        /// <summary>
        /// IEnumerable Location Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <param name="parentId"></param>
        /// <param name="locationTypeId"></param>
        /// <returns></returns>
        public IEnumerable<LocationModel> SearchModel(string name = "", string code = "", string info = "", int parentId = 0, int locationTypeId = 0)
        {
            return DataModule.Locations.Search(name, code, info, parentId, locationTypeId)
                .Select(FactoryModule.Locations.CreateModel);
        }


        /// <summary>
        /// Paged Location Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <param name="parentId"></param>
        /// <param name="locationTypeId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<LocationModel> SearchView(string name = "", string code = "", string info = "", int parentId = 0, int locationTypeId = 0,
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.Locations.SearchView(name, code, info, parentId, locationTypeId, page, pageSize, sort);
        }

        /// <summary>
        /// Create Location Model from Location Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public LocationModel Create(Location entity)
        {
            return FactoryModule.Locations.CreateModel(entity);
        }

        /// <summary>
        /// Create Location Model from Location Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public LocationModel Create(LocationForm form)
        {
            return FactoryModule.Locations.CreateModel(form);
        }

        /// <summary>
        /// Create Location Entity from Location Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Location Create(LocationModel model)
        {
            return FactoryModule.Locations.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of Location before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(LocationModel model)
        {
            return DataModule.Locations.ItemExists(model);
        }

        /// <summary>
        /// Delete Location
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(Location entity)
        {
            return DataModule.Locations.Delete(entity);
        }

        /// <summary>
        /// Get Location Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Location Get(int id)
        {
            return DataModule.Locations.Get(id);
        }



        /// <summary>
        /// Get Location Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public LocationModel GetModel(int id)
        {
            return DataModule.Locations.GetModel(id);
        }

        /// <summary>
        /// Insert new Location to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public LocationModel Insert(LocationModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.Locations.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("Location Name already exists");
                }
            }
            var entity = FactoryModule.Locations.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.Locations.Insert(entity);
            return FactoryModule.Locations.CreateModel(entity);
        }

        /// <summary>
        /// Update a Location Entity with a Location Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public Location Patch(Location entity, LocationModel model, string fields)
        {
            return FactoryModule.Locations.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update Location, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public LocationModel Update(Location entity, LocationModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.Locations.CreateModel(DataModule.Locations.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of Location before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(LocationModel model)
        {
            return DataModule.Locations.ItemExists(model, model.Id);
        }

    }

}