﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Form.Accounts;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;

namespace Bootes.Core.Logic.Account
{
    /// <summary>
    /// SiteUser Service
    /// </summary>
    public partial class SiteUserService : BaseService<SiteUser, SiteUserModel, SiteUserForm, long>
    {
        public ChangePwdStatus ChangePwd(ChangePwdForm form, out SiteUserModel user)
        {
            user = GetByEmail(form.Email);
            if (form.Password == null || form.Password2 == null)
            {
                return ChangePwdStatus.InvalidPassword;
            }
            else
            {
                if (form.Password != form.Password2)
                {
                    return ChangePwdStatus.InvalidPassword;
                }
            }
            if (user == null) return ChangePwdStatus.EmailDoesNotExists;
            if (user.IsLockedOut) return ChangePwdStatus.UserLockedOut;
            var token = CryptUtil.enkrypt(form.Token);
            if (user.Token != token) return ChangePwdStatus.InvalidToken;
            var usr = Get(user.Id);
            usr.Password = FactoryModule.SiteUsers.MakePassword(form.Password, user.UserStamp);
            Update(usr);
            Logic.OutBoxService.MailChangedPassword(user);
            return ChangePwdStatus.Ok;
        }

        public string CreateToken(ClaimsIdentity newIdentity)
        {
            var tk = "";
            try
            {
                string sec = Constants.APP_JWT;
                DateTime issuedAt = DateTime.UtcNow;
                DateTime expires = DateTime.UtcNow.AddDays(7);
                var tokenHandler = new JwtSecurityTokenHandler();
                var now = DateTime.UtcNow;
                var securityKey = new SymmetricSecurityKey(Encoding.Default.GetBytes(sec));
                var signingCredentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256Signature);


                //create the jwt
                var token =
                    (JwtSecurityToken)
                        tokenHandler.CreateJwtSecurityToken(issuer: Constants.SITE_URL, audience: Constants.API_URL,
                            subject: newIdentity, notBefore: issuedAt, expires: expires, signingCredentials: signingCredentials);
                tk = tokenHandler.WriteToken(token);


            }
            catch { }
            return tk;
        }

        public int DeleteFinally(SiteUser entity)
        {
            return DataModule.SiteUsers.DeleteFinally(entity);
        }
        public SiteUserModel GetByEmail(string email)
        {
            return SearchModel(email: email).FirstOrDefault();
        }

        public SiteUserModel GetByMobile(string mobile)
        {
            return SearchModel(mobile: mobile).FirstOrDefault();
        }
         
        public void MakeAdmin()
        {
            try
            {
                var _user = Logic.SiteUserService.Search().FirstOrDefault();
                if (_user == null)
                {
                    Log.Info("could not find user");
                    return;
                }
                _user.UserStatusId = UserStatus.Active;
                Logic.SiteUserService.Update(_user);

                var user = Logic.SiteUserService.SearchModel().FirstOrDefault();

                var setting = Logic.EntitySettingService.Search(entityType: "SYSTEM", sKey: "ADMIN").FirstOrDefault();
                if (setting != null)
                {
                    Log.Info("admin already set");
                    return;
                }


                var corp = Logic.CorporateService.Search().FirstOrDefault();
                var role = Logic.SiteRoleService.Search().FirstOrDefault();

                //create corporate role
                var corprole = FactoryModule.CorporateRoles.CreateModel(corp, role, true);
                corprole = Logic.CorporateRoleService.Insert(corprole);
                if (corprole.Id <= 0)
                {
                    Log.Info("could not add corporate role");
                    return;
                }

                //create user in role
                var corpuser = FactoryModule.CorporateUsers.CreateModel(user, corprole);
                corpuser = Logic.CorporateUserService.Insert(corpuser);
                if (corpuser.Id < 0)
                {
                    Log.Info("could not add corporate user");
                    return;
                }

                //lock onfig
                var _setting = new EntitySettingModel()
                {
                    EntityRef = Constants.SYSTEM_VERSION,
                    EntityType = "SYSTEM",
                    SKey = "ADMIN",
                    SVal = user.UserStamp.ToString(),
                    RecordStatus = RecordStatus.Active,
                    ParamType = "guid"
                };
                Logic.EntitySettingService.Insert(_setting);
                return;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
            }
        }


        public SignUpStatus SignUpCorporate(SignUpForm form, out SiteUserModel user)
        {
            user = Logic.FactoryModule.SiteUsers.DefaultModel();
            //check mobile
            user = GetByMobile(form.Mobile);
            if (user == null)
            {
                user = GetByEmail(form.Email);
            }

            if (user == null)
            {
                //create user
                user = FactoryModule.SiteUsers.CreateModel(form);
                var token = Util.RandomNumber(6);
                user.Token = CryptUtil.enkrypt(token);
                user = Insert(user);
            }
            //is user created
            if (user.Id <= 0) return SignUpStatus.CreationFailed;


            var corprole = Logic.CorporateRoleService.GetModel(form.CorporateRoleId);
            if (corprole == null)
            {
                throw new Exception("Corporate Role not found");
            }

            var corpuser = FactoryModule.CorporateUsers.CreateModel(user, corprole);
            corpuser.UserStatusId = 1;
            corpuser = Logic.CorporateUserService.Insert(corpuser);
            if (corpuser.Id < 0)
            {
                Log.Info("could not add corporate user");
                throw new Exception("could not add public user");
            }
            //send confirmation email only  
            Logic.OutBoxService.MailSignUp(user);
            return SignUpStatus.Ok;
        }

        public SignInStatus SignIn(SignInForm form, string mode, out SiteUserModel user)
        {
            user = Logic.FactoryModule.SiteUsers.DefaultModel();
            var secret = "";
            if (mode == "mobile")
            {
                user = GetByMobile(form.Mobile);
                if (user == null) return SignInStatus.InvalidMobileUser;
                secret = FactoryModule.SiteUsers.MakePassword(form.Pin, user.UserStamp);
                if (user.PIN != secret) return SignInStatus.InvalidMobileAuth;
            }
            if (mode == "email")
            {
                user = GetByEmail(form.Email);
                if (user == null) return SignInStatus.InvalidEmailUser;
                secret = FactoryModule.SiteUsers.MakePassword(form.Password, user.UserStamp);
                if (user.Password != secret) return SignInStatus.InvalidEmailAuth;
            }
            if (user.IsLockedOut) return SignInStatus.UserLockedOut;

            //at this point user is authenticated!

            Logic.OutBoxService.MailLogin(user);
            return SignInStatus.Ok;
        }

        public SignUpStatus SignUp(SignUpForm form, out SiteUserModel user)
        {
            user = Logic.FactoryModule.SiteUsers.DefaultModel();
            //check mobile
            user = GetByMobile(form.Mobile);
            if (user != null) return SignUpStatus.MobileExists;
            //check email
            user = GetByEmail(form.Email);
            if (user != null) return SignUpStatus.EmailExists;
            //create user
            user = FactoryModule.SiteUsers.CreateModel(form);
            var token = Util.RandomNumber(6);
            user.Token = CryptUtil.enkrypt(token);
            user = Insert(user);
            //is user created
            if (user.Id <= 0) return SignUpStatus.CreationFailed;
            //create user in default role

            //send confirmation email only 
            Logic.WalletService.CreateCustomerWallet(user);

            Logic.OutBoxService.MailSignUp(user);
            return SignUpStatus.Ok;
        }

        public ResetPwdStatus ResetPwd(ResetPwdForm form)
        {
            if (string.IsNullOrEmpty(form.Email))
            {
                return ResetPwdStatus.EmailDoesNotExists;
            }
            if (!Util.IsEmail(form.Email))
            {
                return ResetPwdStatus.EmailDoesNotExists;
            }
            var user = Logic.FactoryModule.SiteUsers.DefaultModel();
            user = GetByEmail(form.Email);
            if (user == null) return ResetPwdStatus.EmailDoesNotExists;
            if (user.IsLockedOut) return ResetPwdStatus.UserLockedOut;
            var usr = Get(user.Id);
            var token = Util.RandomNumber(12);
            usr.Token = CryptUtil.enkrypt(token);
            Update(usr);
            Logic.OutBoxService.MailResetPassword(user, token);
            return ResetPwdStatus.Ok;
        }


        public bool SMSActivate(SiteUserModel user, string token)
        {
            var article = Logic.ArticleService.SearchModel("ACTIVATION_SMS").FirstOrDefault();
            if (article == null) throw new Exception("SMS activation article not found");
            var outbox = FactoryModule.OutBoxes.CreateSMSActivationModel(user, token, article);
            Logic.OutBoxService.SendMessage(outbox);
            return true;
        }

        public ChangePwdStatus ValidateResetPwd(ChangePwdForm form)
        {
            var user = Logic.FactoryModule.SiteUsers.DefaultModel();
            user = GetByEmail(form.Email); 
            if (user == null) return ChangePwdStatus.EmailDoesNotExists;
            if (user.IsLockedOut) return ChangePwdStatus.UserLockedOut;
            var token = CryptUtil.enkrypt(form.Token);
            if (user.Token != token) return ChangePwdStatus.InvalidToken;
            return ChangePwdStatus.Ok;
        }

        public string ValidateSignIn(SignInForm form)
        {
            var mode = "mobile";
            if (string.IsNullOrEmpty(form.Mobile) && string.IsNullOrEmpty(form.Pin))
            {
                mode = "email";
                if (string.IsNullOrEmpty(form.Email) && string.IsNullOrEmpty(form.Password))
                {
                    mode = "undef";
                }
            }
            return mode;
        }

        public string ValidateSignUp(SignUpForm form)
        {
            var err = new List<string>();
            if (form.Password == null)
            {
                err.Add("Password is required");
            }
            if (form.Password2 == null)
            {
                err.Add("Confirm Password is required");
            }

            if (form.Password != form.Password2)
            {
                err.Add("Passwords do not match");
            }
            if (form.Pin == null)
            {
                err.Add("PIN is required");
            }
            if (form.Pin2 == null)
            {
                err.Add("Confirm PIN is required");
            }
            if (form.Pin != form.Pin2)
            {
                err.Add("PINs do not match");
            }

            if (form.Pin != null)
            {
                if (!Util.IsPIN(form.Pin))
                {
                    err.Add("PIN has to be four digits and not sequential characters");
                }
            }
            if (form.Email == null)
            {
                err.Add("Email is required");
            }
            else
            {
                if (!Util.IsEmail(form.Email))
                {
                    err.Add("Email is not valid");
                }
            }



            var country = Logic.LocationService.GetModel(form.CountryId);
            if (country == null)
            {
                err.Add("Invalid Country");
            }
            else
            {
                if (country.LocationTypeId != 2)
                {
                    err.Add("Invalid Country");
                }
            }

            if (!Util.IsMobile(form.Mobile, country.CodeLower))
            {
                err.Add("Mobile is not valid e.g. 2348039590434");
            }

            if (err.Count > 0) return string.Join("<br />", err.ToArray());
            else return string.Empty;
        }
    }

    /// <summary>
    /// SiteUser Service
    /// </summary>
    public partial class SiteUserService : BaseService<SiteUser, SiteUserModel, SiteUserForm, long>
    {
        /// <summary>
        /// Create SiteUser Model from SiteUser Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public SiteUserModel Create(SiteUser entity)
        {
            return FactoryModule.SiteUsers.CreateModel(entity);
        }

        /// <summary>
        /// Create SiteUser Model from SiteUser Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public SiteUserModel Create(SiteUserForm form)
        {
            return FactoryModule.SiteUsers.CreateModel(form);
        }

        /// <summary>
        /// Create SiteUser Entity from SiteUser Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public SiteUser Create(SiteUserModel model)
        {
            return FactoryModule.SiteUsers.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of SiteUser before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(SiteUserModel model)
        {
            return DataModule.SiteUsers.ItemExists(model);
        }

        /// <summary>
        /// Delete SiteUser
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(SiteUser entity)
        {
            return DataModule.SiteUsers.Delete(entity);
        }

        /// <summary>
        /// Get SiteUser Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SiteUser Get(long id)
        {
            return DataModule.SiteUsers.Get(id);
        }

        /// <summary>
        /// Get SiteUser Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SiteUserModel GetModel(long id)
        {
            return DataModule.SiteUsers.GetModel(id);
        }

        /// <summary>
        /// Insert new SiteUser to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public SiteUserModel Insert(SiteUserModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.SiteUsers.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("SiteUser Name already exists");
                }
            }
            var entity = FactoryModule.SiteUsers.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.SiteUsers.Insert(entity);
            return FactoryModule.SiteUsers.CreateModel(entity);
        }

        /// <summary>
        /// Update a SiteUser Entity with a SiteUser Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public SiteUser Patch(SiteUser entity, SiteUserModel model, string fields)
        {
            return FactoryModule.SiteUsers.Patch(entity, model, fields);
        }

        /// <summary>
        /// IQueryable SiteUser Entity Search
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="password"></param>
        /// <param name="pIN"></param>
        /// <param name="token"></param>
        /// <param name="appId"></param>
        /// <param name="passwordChangedAt"></param>
        /// <param name="userStatusId"></param>
        /// <param name="mobileValidated"></param>
        /// <param name="emailValidated"></param>
        /// <param name="isLookedOut"></param>
        /// <param name="userStamp"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public IQueryable<SiteUser> Search(string userName = "", string firstName = "", string lastName = "",
            string email = "", string mobile = "", string token = "",
            int appId = 0, UserStatus? userStatusId = null,
            bool? mobileValidated = null, bool? emailValidated = null, bool? isLookedOut = null,
            string userStamp = "", int countryId = 0)
        {
            return DataModule.SiteUsers.Search(userName, firstName, lastName, email,
                mobile, token, appId, userStatusId, mobileValidated, emailValidated, isLookedOut, userStamp, countryId);
        }


        /// <summary>
        /// IEnumerable SiteUser Model Search
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="password"></param>
        /// <param name="pIN"></param>
        /// <param name="token"></param>
        /// <param name="appId"></param>
        /// <param name="passwordChangedAt"></param>
        /// <param name="userStatusId"></param>
        /// <param name="mobileValidated"></param>
        /// <param name="emailValidated"></param>
        /// <param name="isLookedOut"></param>
        /// <param name="userStamp"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public IEnumerable<SiteUserModel> SearchModel(string userName = "", string firstName = "", string lastName = "",
            string email = "", string mobile = "", string token = "", int appId = 0,
            UserStatus? userStatusId = null, bool? mobileValidated = null, bool? emailValidated = null,
            bool? isLookedOut = null, string userStamp = "", int countryId = 0)
        {
            return DataModule.SiteUsers.Search(userName, firstName, lastName, email, mobile, token, appId,
                userStatusId, mobileValidated, emailValidated, isLookedOut, userStamp, countryId)
                .Select(FactoryModule.SiteUsers.CreateModel);
        }


        /// <summary>
        /// Paged SiteUser Model Search
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="password"></param>
        /// <param name="pIN"></param>
        /// <param name="token"></param>
        /// <param name="appId"></param>
        /// <param name="passwordChangedAt"></param>
        /// <param name="userStatusId"></param>
        /// <param name="mobileValidated"></param>
        /// <param name="emailValidated"></param>
        /// <param name="isLookedOut"></param>
        /// <param name="userStamp"></param>
        /// <param name="countryId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<SiteUserModel> SearchView(string userName = "", string firstName = "", string lastName = "",
            string email = "", string mobile = "",
            string token = "", int appId = 0, UserStatus? userStatusId = null, bool? mobileValidated = null,
            bool? emailValidated = null, bool? isLookedOut = null, string userStamp = "", int countryId = 0,
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.SiteUsers.SearchView(userName, firstName, lastName, email, mobile, token, appId, userStatusId, mobileValidated, emailValidated, isLookedOut, userStamp, countryId, page, pageSize, sort);
        }
        /// <summary>
        /// Update SiteUser, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public SiteUserModel Update(SiteUser entity, SiteUserModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.SiteUsers.CreateModel(DataModule.SiteUsers.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of SiteUser before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(SiteUserModel model)
        {
            return DataModule.SiteUsers.ItemExists(model, model.Id);
        }

    }

}
