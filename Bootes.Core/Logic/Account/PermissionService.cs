using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Logic.Account
{

    /// <summary>
    /// Permission Service
    /// </summary>
    public partial class PermissionService : BaseService<Permission, PermissionModel, PermissionForm, int>
    {

    }

    /// <summary>
    /// Permission Service
    /// </summary>
    public partial class PermissionService : BaseService<Permission, PermissionModel, PermissionForm, int>
    {
        /// <summary>
        /// IQueryable Permission Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public IQueryable<Permission> Search(string name = "", string code = "", string info = "")
        {
            return DataModule.Permissions.Search(name, code, info);
        }


        /// <summary>
        /// IEnumerable Permission Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public IEnumerable<PermissionModel> SearchModel(string name = "", string code = "", string info = "")
        {
            return DataModule.Permissions.Search(name, code, info)
                .Select(FactoryModule.Permissions.CreateModel);
        }


        /// <summary>
        /// Paged Permission Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<PermissionModel> SearchView(string name = "", string code = "", string info = "",
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.Permissions.SearchView(name, code, info, page, pageSize, sort);
        }

        /// <summary>
        /// Create Permission Model from Permission Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public PermissionModel Create(Permission entity)
        {
            return FactoryModule.Permissions.CreateModel(entity);
        }

        /// <summary>
        /// Create Permission Model from Permission Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public PermissionModel Create(PermissionForm form)
        {
            return FactoryModule.Permissions.CreateModel(form);
        }

        /// <summary>
        /// Create Permission Entity from Permission Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Permission Create(PermissionModel model)
        {
            return FactoryModule.Permissions.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of Permission before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(PermissionModel model)
        {
            return DataModule.Permissions.ItemExists(model);
        }

        /// <summary>
        /// Delete Permission
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(Permission entity)
        {
            return DataModule.Permissions.Delete(entity);
        }

        /// <summary>
        /// Get Permission Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Permission Get(int id)
        {
            return DataModule.Permissions.Get(id);
        }



        /// <summary>
        /// Get Permission Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public PermissionModel GetModel(int id)
        {
            return DataModule.Permissions.GetModel(id);
        }

        /// <summary>
        /// Insert new Permission to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public PermissionModel Insert(PermissionModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.Permissions.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("Permission Name already exists");
                }
            }
            var entity = FactoryModule.Permissions.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.Permissions.Insert(entity);
            return FactoryModule.Permissions.CreateModel(entity);
        }

        /// <summary>
        /// Update a Permission Entity with a Permission Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public Permission Patch(Permission entity, PermissionModel model, string fields)
        {
            return FactoryModule.Permissions.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update Permission, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public PermissionModel Update(Permission entity, PermissionModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.Permissions.CreateModel(DataModule.Permissions.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of Permission before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(PermissionModel model)
        {
            return DataModule.Permissions.ItemExists(model, model.Id);
        }

    }


}