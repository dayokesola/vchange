using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Logic.Account
{

    /// <summary>
    /// Corporate Service
    /// </summary>
    public partial class CorporateService : BaseService<Corporate, CorporateModel, CorporateForm, int>
    {
        public string ValidateSignUp(CorporateForm form)
        {
            var err = new List<string>();
            //if (form.AdminPassword != form.AdminPassword2)
            //{
            //    err.Add("Passwords do not match");
            //}

            //if (!Util.IsEmail(form.AdminEmail))
            //{
            //    err.Add("Email is not valid");
            //}
            var country = Logic.LocationService.Get(form.CountryId);
            if (country == null)
            {
                err.Add("Invalid Country");
            }
            else
            {
                if (country.LocationTypeId != 2)
                {
                    err.Add("Invalid Country");
                }
            }
            if (err.Count > 0) return string.Join("<br />", err.ToArray());
            else return string.Empty;
        }

        public SignUpStatus SignUp(CorporateForm form, SiteUserModel creator, out string code)
        {
            var user = Logic.FactoryModule.SiteUsers.DefaultModel();
            code = Util.TimeStampCode("CORP");
            //Now lets create the corporate
            CorporateModel corp = FactoryModule.Corporates.CreateModel(form);
            corp.Code = code;
            corp.AppId = Constants.APP_ID;
            corp.CorporateStatus = 0; //pending
            corp = Insert(corp);
            if (corp.Id <= 0)
            {
                //Logic.SiteUserService.DeleteFinally(FactoryModule.SiteUsers.CreateEntity(user));
                return SignUpStatus.CorporateAccountFailed;
            }

            //Create a corporate Role
            var cRole = Logic.CorporateRoleService.AddAdminRole(corp);
            if (cRole.Id <= 0)
            {
                //Logic.SiteUserService.DeleteFinally(FactoryModule.SiteUsers.CreateEntity(user));
                return SignUpStatus.CorporateAccountFailed;
            }
            else
            {
                //add creator as admin
                Logic.CorporateUserService.AddAdmin(corp, cRole, creator, false);
            }
            //Add user to corporate role 
            //send confirmation email only  
            //var isok = Logic.SiteUserService.MailActivate(user, token);
            //if (!isok) return SignUpStatus.OkDoReset;
            return SignUpStatus.Ok;
        }

        public CorporateModel GetModel(long ownerId)
        {
            var id = Util.ToInt32(ownerId.ToString());
            return GetModel(id);
        }

        public List<CorporateModel> GetUserCorporates(long userId, int corporateStatus = -1)
        {
            return DataModule.Corporates.GetUserCorporates(userId, corporateStatus);
        }
    }


    /// <summary>
    /// Corporate Service
    /// </summary>
    public partial class CorporateService : BaseService<Corporate, CorporateModel, CorporateForm, int>
    {
        /// <summary>
        /// IQueryable Corporate Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="countryId"></param>
        /// <param name="regId"></param>
        /// <param name="publicName"></param>
        /// <param name="appId"></param>
        /// <returns></returns>
        public IQueryable<Corporate> Search(string name = "", string code = "", int countryId = 0, string regId = "", string publicName = "", int appId = 0)
        {
            return DataModule.Corporates.Search(name, code, countryId, regId, publicName, appId);
        }


        /// <summary>
        /// IEnumerable Corporate Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="countryId"></param>
        /// <param name="regId"></param>
        /// <param name="publicName"></param>
        /// <param name="appId"></param>
        /// <returns></returns>
        public IEnumerable<CorporateModel> SearchModel(string name = "", string code = "", int countryId = 0, 
            string regId = "", string publicName = "", int appId = 0)
        {
            return DataModule.Corporates.Search(name, code, countryId, regId, publicName, appId)
                .Select(FactoryModule.Corporates.CreateModel);
        }


        /// <summary>
        /// Paged Corporate Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="countryId"></param>
        /// <param name="regId"></param>
        /// <param name="publicName"></param>
        /// <param name="appId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<CorporateModel> SearchView(string name = "", string code = "", int countryId = 0, string regId = "", 
            string publicName = "", int appId = 0, int corporatestatus =-1,
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.Corporates.SearchView(name, code, countryId, regId, publicName, appId, corporatestatus,
                page, pageSize, sort);
        }

        /// <summary>
        /// Create Corporate Model from Corporate Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public CorporateModel Create(Corporate entity)
        {
            return FactoryModule.Corporates.CreateModel(entity);
        }

        /// <summary>
        /// Create Corporate Model from Corporate Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public CorporateModel Create(CorporateForm form)
        {
            return FactoryModule.Corporates.CreateModel(form);
        }

        /// <summary>
        /// Create Corporate Entity from Corporate Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Corporate Create(CorporateModel model)
        {
            return FactoryModule.Corporates.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of Corporate before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(CorporateModel model)
        {
            return DataModule.Corporates.ItemExists(model);
        }

        /// <summary>
        /// Delete Corporate
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(Corporate entity)
        {
            return DataModule.Corporates.Delete(entity);
        }

        /// <summary>
        /// Get Corporate Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Corporate Get(int id)
        {
            return DataModule.Corporates.Get(id);
        }



        /// <summary>
        /// Get Corporate Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CorporateModel GetModel(int id)
        {
            return DataModule.Corporates.GetModel(id);
        }

        /// <summary>
        /// Insert new Corporate to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public CorporateModel Insert(CorporateModel model, bool check = true, bool defaultStatus = true)
        {
            if (check)
            {
                var routeSearch = DataModule.Corporates.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("Corporate Name already exists");
                }
            }
            var entity = FactoryModule.Corporates.CreateEntity(model);
            if (defaultStatus)
                entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.Corporates.Insert(entity);
            return FactoryModule.Corporates.CreateModel(entity);
        }

        /// <summary>
        /// Update a Corporate Entity with a Corporate Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public Corporate Patch(Corporate entity, CorporateModel model, string fields)
        {
            return FactoryModule.Corporates.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update Corporate, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public CorporateModel Update(Corporate entity, CorporateModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.Corporates.CreateModel(DataModule.Corporates.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of Corporate before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(CorporateModel model)
        {
            return DataModule.Corporates.ItemExists(model, model.Id);
        }

    }






}