using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;

namespace Bootes.Core.Logic.Account
{

    /// <summary>
    /// CorporateRole Service
    /// </summary>
    public partial class CorporateRoleService : BaseService<CorporateRole, CorporateRoleModel, CorporateRoleForm, int>
    {
        public CorporateRoleModel AddAdminRole(CorporateModel corp)
        {
            var role = new CorporateRoleModel()
            {
                CorporateId = corp.Id,
                IsAdmin = true,
                Name = "Corporate Admin",
                RecordStatus = RecordStatus.Active,
                RoleTypeId = 2
            }; 
            return Insert(role);
        }
    }

  
    /// <summary>
    /// CorporateRole Service
    /// </summary>
    public partial class CorporateRoleService : BaseService<CorporateRole, CorporateRoleModel, CorporateRoleForm, int>
    {
        /// <summary>
        /// IQueryable CorporateRole Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="corporateId"></param>
        /// <param name="isAdmin"></param>
        /// <param name="roleTypeId"></param>
        /// <returns></returns>
        public IQueryable<CorporateRole> Search(string name = "", int corporateId = 0, bool? isAdmin = null, int roleTypeId = 0)
        {
            return DataModule.CorporateRoles.Search(name, corporateId, isAdmin, roleTypeId);
        }


        /// <summary>
        /// IEnumerable CorporateRole Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="corporateId"></param>
        /// <param name="isAdmin"></param>
        /// <param name="roleTypeId"></param>
        /// <returns></returns>
        public IEnumerable<CorporateRoleModel> SearchModel(string name = "", int corporateId = 0, bool? isAdmin = null, int roleTypeId = 0)
        {
            return DataModule.CorporateRoles.Search(name, corporateId, isAdmin, roleTypeId)
                .Select(FactoryModule.CorporateRoles.CreateModel);
        }


        /// <summary>
        /// Paged CorporateRole Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="corporateId"></param>
        /// <param name="isAdmin"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<CorporateRoleModel> SearchView(string name = "", int corporateId = 0, bool? isAdmin = null, int roleTypeId = 0,
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.CorporateRoles.SearchView(name, corporateId, isAdmin, roleTypeId, page, pageSize, sort);
        }

        /// <summary>
        /// Create CorporateRole Model from CorporateRole Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public CorporateRoleModel Create(CorporateRole entity)
        {
            return FactoryModule.CorporateRoles.CreateModel(entity);
        }

        /// <summary>
        /// Create CorporateRole Model from CorporateRole Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public CorporateRoleModel Create(CorporateRoleForm form)
        {
            return FactoryModule.CorporateRoles.CreateModel(form);
        }

        /// <summary>
        /// Create CorporateRole Entity from CorporateRole Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CorporateRole Create(CorporateRoleModel model)
        {
            return FactoryModule.CorporateRoles.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of CorporateRole before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(CorporateRoleModel model)
        {
            return DataModule.CorporateRoles.ItemExists(model);
        }

        /// <summary>
        /// Delete CorporateRole
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(CorporateRole entity)
        {
            return DataModule.CorporateRoles.Delete(entity);
        }

        /// <summary>
        /// Get CorporateRole Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CorporateRole Get(int id)
        {
            return DataModule.CorporateRoles.Get(id);
        }



        /// <summary>
        /// Get CorporateRole Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CorporateRoleModel GetModel(int id)
        {
            return DataModule.CorporateRoles.GetModel(id);
        }

        /// <summary>
        /// Insert new CorporateRole to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public CorporateRoleModel Insert(CorporateRoleModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.CorporateRoles.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("CorporateRole Name already exists");
                }
            }
            var entity = FactoryModule.CorporateRoles.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.CorporateRoles.Insert(entity);
            return FactoryModule.CorporateRoles.CreateModel(entity);
        }

        /// <summary>
        /// Update a CorporateRole Entity with a CorporateRole Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public CorporateRole Patch(CorporateRole entity, CorporateRoleModel model, string fields)
        {
            return FactoryModule.CorporateRoles.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update CorporateRole, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public CorporateRoleModel Update(CorporateRole entity, CorporateRoleModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.CorporateRoles.CreateModel(DataModule.CorporateRoles.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of CorporateRole before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(CorporateRoleModel model)
        {
            return DataModule.CorporateRoles.ItemExists(model, model.Id);
        }

    }




}