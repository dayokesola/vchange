using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Logic.Account
{

    /// <summary>
    /// SiteRole Service
    /// </summary>
    public class SiteRoleService : BaseService<SiteRole, SiteRoleModel, SiteRoleForm, int>
    {
        /// <summary>
        /// IQueryable SiteRole Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="departmentId"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public IQueryable<SiteRole> Search(string name = "", string code = "", int departmentId = 0, int roleTypeId = 0, string info = "")
        {
            return DataModule.SiteRoles.Search(name, code, departmentId, roleTypeId, info);
        }


        /// <summary>
        /// IEnumerable SiteRole Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="departmentId"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public IEnumerable<SiteRoleModel> SearchModel(string name = "", string code = "", int departmentId = 0, int roleTypeId = 0, string info = "")
        {
            return DataModule.SiteRoles.Search(name, code, departmentId, roleTypeId, info)
                .Select(FactoryModule.SiteRoles.CreateModel);
        }


        /// <summary>
        /// Paged SiteRole Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="departmentId"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="info"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<SiteRoleModel> SearchView(string name = "", string code = "", int departmentId = 0, int roleTypeId = 0, string info = "",
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.SiteRoles.SearchView(name, code, departmentId, roleTypeId, info, page, pageSize, sort);
        }

        /// <summary>
        /// Create SiteRole Model from SiteRole Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public SiteRoleModel Create(SiteRole entity)
        {
            return FactoryModule.SiteRoles.CreateModel(entity);
        }

        /// <summary>
        /// Create SiteRole Model from SiteRole Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public SiteRoleModel Create(SiteRoleForm form)
        {
            return FactoryModule.SiteRoles.CreateModel(form);
        }

        /// <summary>
        /// Create SiteRole Entity from SiteRole Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public SiteRole Create(SiteRoleModel model)
        {
            return FactoryModule.SiteRoles.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of SiteRole before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(SiteRoleModel model)
        {
            return DataModule.SiteRoles.ItemExists(model);
        }

        /// <summary>
        /// Delete SiteRole
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(SiteRole entity)
        {
            return DataModule.SiteRoles.Delete(entity);
        }

        /// <summary>
        /// Get SiteRole Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SiteRole Get(int id)
        {
            return DataModule.SiteRoles.Get(id);
        }



        /// <summary>
        /// Get SiteRole Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public SiteRoleModel GetModel(int id)
        {
            return DataModule.SiteRoles.GetModel(id);
        }

        /// <summary>
        /// Insert new SiteRole to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public SiteRoleModel Insert(SiteRoleModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.SiteRoles.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("SiteRole Name already exists");
                }
            }
            var entity = FactoryModule.SiteRoles.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.SiteRoles.Insert(entity);
            return FactoryModule.SiteRoles.CreateModel(entity);
        }

        /// <summary>
        /// Update a SiteRole Entity with a SiteRole Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public SiteRole Patch(SiteRole entity, SiteRoleModel model, string fields)
        {
            return FactoryModule.SiteRoles.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update SiteRole, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public SiteRoleModel Update(SiteRole entity, SiteRoleModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.SiteRoles.CreateModel(DataModule.SiteRoles.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of SiteRole before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(SiteRoleModel model)
        {
            return DataModule.SiteRoles.ItemExists(model, model.Id);
        }

    }

}