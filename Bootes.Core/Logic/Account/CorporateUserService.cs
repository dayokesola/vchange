using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using Bootes.Core.Common;
using Bootes.Core.Domain.Enum;

namespace Bootes.Core.Logic.Account
{



    /// <summary>
    /// CorporateUser Service
    /// </summary>
    public partial class CorporateUserService : BaseService<CorporateUser, CorporateUserModel, CorporateUserForm, int>
    {
        public CorporateUserModel AddAdmin(CorporateModel corp, CorporateRoleModel cRole, SiteUserModel user,
            bool IsLockedOut = false)
        {
            var usr = new CorporateUserModel()
            {
                AuthPin = FactoryModule.SiteUsers.MakePassword("000000", user.UserStamp),
                CorporateId = corp.Id,
                CorporateRoleId = cRole.Id,
                IsLockedOut = IsLockedOut,
                UserId = user.Id,
                UserStatusId = 1,
                RecordStatus = RecordStatus.Active
            }; 
            return Insert(usr);
        }
    }

    /// <summary>
    /// CorporateUser Service
    /// </summary>
    public partial class CorporateUserService : BaseService<CorporateUser, CorporateUserModel, CorporateUserForm, int>
    {
        /// <summary>
        /// IQueryable CorporateUser Entity Search
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="corporateId"></param>
        /// <param name="corporateRoleId"></param>
        /// <param name="userStatusId"></param>
        /// <param name="isLockedOut"></param>
        /// <param name="authPin"></param>
        /// <returns></returns>
        public IQueryable<CorporateUser> Search(long userId = 0, int corporateId = 0, int corporateRoleId = 0, int userStatusId = 0, bool? isLockedOut = null, string authPin = "")
        {
            return DataModule.CorporateUsers.Search(userId, corporateId, corporateRoleId, userStatusId, isLockedOut, authPin);
        }


        /// <summary>
        /// IEnumerable CorporateUser Model Search
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="corporateId"></param>
        /// <param name="corporateRoleId"></param>
        /// <param name="userStatusId"></param>
        /// <param name="isLockedOut"></param>
        /// <param name="authPin"></param>
        /// <returns></returns>
        public IEnumerable<CorporateUserModel> SearchModel(long userId = 0, int corporateId = 0, int corporateRoleId = 0, int userStatusId = 0, bool? isLockedOut = null, string authPin = "")
        {
            return DataModule.CorporateUsers.Search(userId, corporateId, corporateRoleId, userStatusId, isLockedOut, authPin)
                .Select(FactoryModule.CorporateUsers.CreateModel);
        }


        /// <summary>
        /// Paged CorporateUser Model Search
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="corporateId"></param>
        /// <param name="corporateRoleId"></param>
        /// <param name="userStatusId"></param>
        /// <param name="isLockedOut"></param>
        /// <param name="authPin"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<CorporateUserModel> SearchView(long userId = 0, int corporateId = 0, int corporateRoleId = 0, int userStatusId = 0, bool? isLockedOut = null, string authPin = "",
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.CorporateUsers.SearchView(userId, corporateId, corporateRoleId, userStatusId, isLockedOut, authPin, page, pageSize, sort);
        }

        /// <summary>
        /// Create CorporateUser Model from CorporateUser Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public CorporateUserModel Create(CorporateUser entity)
        {
            return FactoryModule.CorporateUsers.CreateModel(entity);
        }

        /// <summary>
        /// Create CorporateUser Model from CorporateUser Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public CorporateUserModel Create(CorporateUserForm form)
        {
            return FactoryModule.CorporateUsers.CreateModel(form);
        }

        /// <summary>
        /// Create CorporateUser Entity from CorporateUser Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public CorporateUser Create(CorporateUserModel model)
        {
            return FactoryModule.CorporateUsers.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of CorporateUser before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(CorporateUserModel model)
        {
            return DataModule.CorporateUsers.ItemExists(model);
        }

        /// <summary>
        /// Delete CorporateUser
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(CorporateUser entity)
        {
            return DataModule.CorporateUsers.Delete(entity);
        }

        /// <summary>
        /// Get CorporateUser Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CorporateUser Get(int id)
        {
            return DataModule.CorporateUsers.Get(id);
        }



        /// <summary>
        /// Get CorporateUser Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public CorporateUserModel GetModel(int id)
        {
            return DataModule.CorporateUsers.GetModel(id);
        }

        /// <summary>
        /// Insert new CorporateUser to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public CorporateUserModel Insert(CorporateUserModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.CorporateUsers.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("CorporateUser Name already exists");
                }
            }
            var entity = FactoryModule.CorporateUsers.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.CorporateUsers.Insert(entity);
            return FactoryModule.CorporateUsers.CreateModel(entity);
        }

        /// <summary>
        /// Update a CorporateUser Entity with a CorporateUser Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public CorporateUser Patch(CorporateUser entity, CorporateUserModel model, string fields)
        {
            return FactoryModule.CorporateUsers.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update CorporateUser, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public CorporateUserModel Update(CorporateUser entity, CorporateUserModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.CorporateUsers.CreateModel(DataModule.CorporateUsers.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of CorporateUser before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(CorporateUserModel model)
        {
            return DataModule.CorporateUsers.ItemExists(model, model.Id);
        }

    }

}