using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Logic.Account
{

    /// <summary>
    /// EntitySetting Service
    /// </summary>
    public class EntitySettingService : BaseService<EntitySetting, EntitySettingModel, EntitySettingForm, int>
    {
        /// <summary>
        /// IQueryable EntitySetting Entity Search
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityRef"></param>
        /// <param name="sKey"></param>
        /// <param name="sVal"></param>
        /// <param name="paramType"></param>
        /// <returns></returns>
        public IQueryable<EntitySetting> Search(string entityType = "", long entityRef = 0, string sKey = "", string sVal = "", string paramType = "")
        {
            return DataModule.EntitySettings.Search(entityType, entityRef, sKey, sVal, paramType);
        }


        /// <summary>
        /// IEnumerable EntitySetting Model Search
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityRef"></param>
        /// <param name="sKey"></param>
        /// <param name="sVal"></param>
        /// <param name="paramType"></param>
        /// <returns></returns>
        public IEnumerable<EntitySettingModel> SearchModel(string entityType = "", long entityRef = 0, string sKey = "", string sVal = "", string paramType = "")
        {
            return DataModule.EntitySettings.Search(entityType, entityRef, sKey, sVal, paramType)
                .Select(FactoryModule.EntitySettings.CreateModel);
        }


        /// <summary>
        /// Paged EntitySetting Model Search
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityRef"></param>
        /// <param name="sKey"></param>
        /// <param name="sVal"></param>
        /// <param name="paramType"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<EntitySettingModel> SearchView(string entityType = "", long entityRef = 0, string sKey = "", string sVal = "", string paramType = "",
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.EntitySettings.SearchView(entityType, entityRef, sKey, sVal, paramType, page, pageSize, sort);
        }

        /// <summary>
        /// Create EntitySetting Model from EntitySetting Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public EntitySettingModel Create(EntitySetting entity)
        {
            return FactoryModule.EntitySettings.CreateModel(entity);
        }

        /// <summary>
        /// Create EntitySetting Model from EntitySetting Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public EntitySettingModel Create(EntitySettingForm form)
        {
            return FactoryModule.EntitySettings.CreateModel(form);
        }

        /// <summary>
        /// Create EntitySetting Entity from EntitySetting Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public EntitySetting Create(EntitySettingModel model)
        {
            return FactoryModule.EntitySettings.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of EntitySetting before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(EntitySettingModel model)
        {
            return DataModule.EntitySettings.ItemExists(model);
        }

        /// <summary>
        /// Delete EntitySetting
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(EntitySetting entity)
        {
            return DataModule.EntitySettings.Delete(entity);
        }

        /// <summary>
        /// Get EntitySetting Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EntitySetting Get(int id)
        {
            return DataModule.EntitySettings.Get(id);
        }



        /// <summary>
        /// Get EntitySetting Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public EntitySettingModel GetModel(int id)
        {
            return DataModule.EntitySettings.GetModel(id);
        }

        /// <summary>
        /// Insert new EntitySetting to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public EntitySettingModel Insert(EntitySettingModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.EntitySettings.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("EntitySetting Name already exists");
                }
            }
            var entity = FactoryModule.EntitySettings.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.EntitySettings.Insert(entity);
            return FactoryModule.EntitySettings.CreateModel(entity);
        }

        /// <summary>
        /// Update a EntitySetting Entity with a EntitySetting Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public EntitySetting Patch(EntitySetting entity, EntitySettingModel model, string fields)
        {
            return FactoryModule.EntitySettings.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update EntitySetting, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public EntitySettingModel Update(EntitySetting entity, EntitySettingModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.EntitySettings.CreateModel(DataModule.EntitySettings.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of EntitySetting before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(EntitySettingModel model)
        {
            return DataModule.EntitySettings.ItemExists(model, model.Id);
        }
 

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="valField"></param>
        /// <param name="sortField"></param>
        /// <returns></returns>
        public List<SelectMenuModel<long>> SelectList(string entityType, string valField = "Name", string sortField = "EntityRef")
        {
            var ls = DataModule.EntitySettings.SelectList(entityType, valField, sortField);
            return (from l in ls
                    select new SelectMenuModel<long>()
                    {
                        Id = l.EntityRef,
                        Name = l.SVal
                    }).ToList();
        }
    }

}