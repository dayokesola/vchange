using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Form.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Logic.Account
{

    /// <summary>
    /// Client Service
    /// </summary>
    public partial class ClientService : BaseService<Client, ClientModel, ClientForm, long>
    {

    }



    /// <summary>
    /// Client Service
    /// </summary>
    public partial class ClientService : BaseService<Client, ClientModel, ClientForm, long>
    {
        /// <summary>
        /// IQueryable Client Entity Search
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientSecret1"></param>
        /// <param name="clientSecret2"></param>
        /// <param name="clientTypeId"></param>
        /// <param name="clientStatusId"></param>
        /// <returns></returns>
        public IQueryable<Client> Search(string clientId = "", string clientSecret1 = "", string clientSecret2 = "", int clientTypeId = 0, int clientStatusId = 0)
        {
            return DataModule.Clients.Search(clientId, clientSecret1, clientSecret2, clientTypeId, clientStatusId);
        }


        /// <summary>
        /// IEnumerable Client Model Search
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientSecret1"></param>
        /// <param name="clientSecret2"></param>
        /// <param name="clientTypeId"></param>
        /// <param name="clientStatusId"></param>
        /// <returns></returns>
        public IEnumerable<ClientModel> SearchModel(string clientId = "", string clientSecret1 = "", string clientSecret2 = "", int clientTypeId = 0, int clientStatusId = 0)
        {
            return DataModule.Clients.Search(clientId, clientSecret1, clientSecret2, clientTypeId, clientStatusId)
                .Select(FactoryModule.Clients.CreateModel);
        }


        /// <summary>
        /// Paged Client Model Search
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientSecret1"></param>
        /// <param name="clientSecret2"></param>
        /// <param name="clientTypeId"></param>
        /// <param name="clientStatusId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<ClientModel> SearchView(string clientId = "", string clientSecret1 = "", string clientSecret2 = "", int clientTypeId = 0, int clientStatusId = 0,
            long page = 1, long pageSize = 10, string sort = "")
        {
            return DataModule.Clients.SearchView(clientId, clientSecret1, clientSecret2, clientTypeId, clientStatusId, page, pageSize, sort);
        }

        /// <summary>
        /// Create Client Model from Client Entity
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public ClientModel Create(Client entity)
        {
            return FactoryModule.Clients.CreateModel(entity);
        }

        /// <summary>
        /// Create Client Model from Client Form
        /// </summary>
        /// <param name="form"></param>
        /// <returns></returns>
        public ClientModel Create(ClientForm form)
        {
            return FactoryModule.Clients.CreateModel(form);
        }

        /// <summary>
        /// Create Client Entity from Client Model
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public Client Create(ClientModel model)
        {
            return FactoryModule.Clients.CreateEntity(model);
        }

        /// <summary>
        /// Check Uniqueness of Client before creation
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool CreateExists(ClientModel model)
        {
            return DataModule.Clients.ItemExists(model);
        }

        /// <summary>
        /// Delete Client
        /// </summary>
        /// <param name="entity"></param>
        /// <returns></returns>
        public int Delete(Client entity)
        {
            return DataModule.Clients.Delete(entity);
        }

        /// <summary>
        /// Get Client Entity
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public Client Get(long id)
        {
            return DataModule.Clients.Get(id);
        }



        /// <summary>
        /// Get Client Model
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public ClientModel GetModel(long id)
        {
            return DataModule.Clients.GetModel(id);
        }

        /// <summary>
        /// Insert new Client to DB
        /// </summary>
        /// <param name="model"></param>
        /// <param name="check"></param>
        /// <returns></returns>
        public ClientModel Insert(ClientModel model, bool check = true)
        {
            if (check)
            {
                var routeSearch = DataModule.Clients.ItemExists(model);
                if (routeSearch)
                {
                    throw new Exception("Client Name already exists");
                }
            }
            var entity = FactoryModule.Clients.CreateEntity(model);
            entity.RecordStatus = Core.Domain.Enum.RecordStatus.Active;
            DataModule.Clients.Insert(entity);
            return FactoryModule.Clients.CreateModel(entity);
        }

        /// <summary>
        /// Update a Client Entity with a Client Model with selected fields
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public Client Patch(Client entity, ClientModel model, string fields)
        {
            return FactoryModule.Clients.Patch(entity, model, fields);
        }

        /// <summary>
        /// Update Client, with Patch Options Optional
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="model"></param>
        /// <param name="fields"></param>
        /// <returns></returns>
        public ClientModel Update(Client entity, ClientModel model = null, string fields = "")
        {
            if (model != null)
            {
                entity = Patch(entity, model, fields);
            }
            return FactoryModule.Clients.CreateModel(DataModule.Clients.Update(entity));
        }

        /// <summary>
        /// Check Uniqueness of Client before update
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool UpdateExists(ClientModel model)
        {
            return DataModule.Clients.ItemExists(model, model.Id);
        }

    }

}