﻿using System;
using System.Collections.Generic;
using System.Web;

namespace Bootes.Core.Logic.Cache
{
    /// <summary>
    /// Application Cache Service
    /// </summary>
    public class AppCacheService
    {
        /// <summary>
        /// 
        /// </summary>
        public AppCacheService()
        {

        }

        /// <summary>
        /// Add an Item to the cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="item"></param>
        public void AddItem<T>(string key, T item)
        {
            RemoveItem(key);
            _Write(key, item);
        }

        /// <summary>
        /// Add ann item to a list in cache
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="item"></param>
        public void AddItemToList<T>(string key, T item)
        {
            List<T> ls = GetList<T>(key);
            if (ls == null)
            {
                ls = new List<T>();
            }

            if (!ls.Contains(item))
            {
                ls.Add(item);
            }
            AddList(key, ls);
        }

        /// <summary>
        /// Add a nnew cache to list
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="list"></param>
        public void AddList<T>(string key, List<T> list)
        {
            RemoveList(key);
            _Write(key, list);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public T GetItem<T>(string key)
        {
            return Cast<T>(_Read(key));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <returns></returns>
        public List<T> GetList<T>(string key)
        {
            return Cast<List<T>>(_Read(key));
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public void RemoveItem(string key)
        {
            _Remove(key);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        public void RemoveList(string key)
        {
            _Remove(key);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="readData"></param>
        /// <returns></returns>
        private T Cast<T>(object readData)
        {
            if (readData is T)
            {
                return (T)readData;
            }
            try
            {
                return (T)Convert.ChangeType(readData, typeof(T));
            }
            catch (InvalidCastException)
            {
                return default(T);
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        private object _Read(string key)
        {
            return HttpContext.Current.Application.Get(key);
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        private void _Remove(string key)
        {
            HttpContext.Current.Application.Remove(key);

        }
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="key"></param>
        /// <param name="obj"></param>
        private void _Write<T>(string key, T obj)
        {
            HttpContext.Current.Application.Add(key, obj);
        }
    }

}
