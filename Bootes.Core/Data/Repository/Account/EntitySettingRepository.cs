using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Data.Repository.Account
{

    /// <summary>
    /// 
    /// </summary>
    public class EntitySettingRepository : BaseRepository<EntitySetting, EntitySettingModel, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public EntitySettingRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable EntitySetting Entity Search
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityRef"></param>
        /// <param name="sKey"></param>
        /// <param name="sVal"></param>
        /// <param name="paramType"></param>
        /// <returns></returns>
        public IQueryable<EntitySetting> Search(string entityType = "", long entityRef = 0, string sKey = "", string sVal = "", string paramType = "")
        {
            var table = Query();
            if (!string.IsNullOrEmpty(entityType))
            {
                table = table.Where(x => x.EntityType == entityType);
            }
            if (entityRef > 0)
            {
                table = table.Where(x => x.EntityRef == entityRef);
            }
            if (!string.IsNullOrEmpty(sKey))
            {
                table = table.Where(x => x.SKey == sKey);
            }
            if (!string.IsNullOrEmpty(sVal))
            {
                table = table.Where(x => x.SVal == sVal);
            }
            if (!string.IsNullOrEmpty(paramType))
            {
                table = table.Where(x => x.ParamType == paramType);
            }

            return table;
        }

        /// <summary>
        /// Paged EntitySetting Model Search
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityRef"></param>
        /// <param name="sKey"></param>
        /// <param name="sVal"></param>
        /// <param name="paramType"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<EntitySettingModel> SearchView(string entityType = "", long entityRef = 0, string sKey = "", string sVal = "", string paramType = "",
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = "where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(entityType))
            {
                sql += $" and EntityType = @{c} ";
                AddParam("entityType", entityType);
                c++;
            }
            if (entityRef > 0)
            {
                sql += $" and EntityRef = @{c} ";
                AddParam("entityRef", entityRef);
                c++;
            }
            if (!string.IsNullOrEmpty(sKey))
            {
                sql += $" and SKey = @{c} ";
                AddParam("sKey", sKey);
                c++;
            }
            if (!string.IsNullOrEmpty(sVal))
            {
                sql += $" and SVal = @{c} ";
                AddParam("sVal", sVal);
                c++;
            }
            if (!string.IsNullOrEmpty(paramType))
            {
                sql += $" and ParamType = @{c} ";
                AddParam("paramType", paramType);
                c++;
            } 
            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<EntitySettingModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }

            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<EntitySettingModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get EntitySetting Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public EntitySettingModel GetModel(int Id)
        {
            var sql = " where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="entityRef"></param>
        /// <param name="sKey"></param>
        /// <param name="sVal"></param>
        /// <param name="paramType"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string entityType = "", long entityRef = 0, string sKey = "", string sVal = "", string paramType = "", int Id = 0)
        {
            var check = Search(entityType, entityRef, sKey, sVal, paramType);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(EntitySettingModel model, int Id = 0)
        {
            var check = Search(model.EntityType, model.EntityRef, model.SKey, model.SVal, model.ParamType);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="entityType"></param>
        /// <param name="valField"></param>
        /// <param name="sortField"></param>
        /// <returns></returns>
        public List<EntitySettingModel> SelectList(string entityType, string valField = "Name", string sortField = "EntityRef")
        {
            var sql = " where EntityType = @0 and skey = @1 order by " + sortField;
            AddParam("entityType", entityType);
            AddParam("valField", valField);
            return GetList(sql);
        }
    }

}