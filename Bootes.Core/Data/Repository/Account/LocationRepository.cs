using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Data.Repository.Account
{

    /// <summary>
    /// 
    /// </summary>
    public class LocationRepository : BaseRepository<Location, LocationModel, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public LocationRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable Location Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <param name="parentId"></param>
        /// <param name="locationTypeId"></param>
        /// <returns></returns>
        public IQueryable<Location> Search(string name = "", string code = "", string info = "", int parentId = 0, int locationTypeId = 0)
        {
            var table = Query();
            if (!string.IsNullOrEmpty(name))
            {
                table = table.Where(x => x.Name == name);
            }
            if (!string.IsNullOrEmpty(code))
            {
                table = table.Where(x => x.Code == code);
            }
            if (!string.IsNullOrEmpty(info))
            {
                table = table.Where(x => x.Info == info);
            }
            if (parentId > 0)
            {
                table = table.Where(x => x.ParentId == parentId);
            }
            if (locationTypeId > 0)
            {
                table = table.Where(x => x.LocationTypeId == locationTypeId);
            }

            return table;
        }

        /// <summary>
        /// Paged Location Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <param name="parentId"></param>
        /// <param name="locationTypeId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<LocationModel> SearchView(string name = "", string code = "", string info = "", int parentId = 0, int locationTypeId = 0,
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = " where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(name))
            {
                sql += $" and Name = @{c} ";
                AddParam("name", name);
                c++;
            }
            if (!string.IsNullOrEmpty(code))
            {
                sql += $" and Code = @{c} ";
                AddParam("code", code);
                c++;
            }
            if (!string.IsNullOrEmpty(info))
            {
                sql += $" and Info = @{c} ";
                AddParam("info", info);
                c++;
            }
            if (parentId > 0)
            {
                sql += $" and ParentId = @{c} ";
                AddParam("parentId", parentId);
                c++;
            }
            if (locationTypeId > 0)
            {
                sql += $" and LocationTypeId = @{c} ";
                AddParam("locationTypeId", locationTypeId);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<LocationModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<LocationModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get Location Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public LocationModel GetModel(int Id)
        {
            var sql = " where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="info"></param>
        /// <param name="parentId"></param>
        /// <param name="locationTypeId"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string name = "", string code = "", string info = "", int parentId = 0, int locationTypeId = 0, int Id = 0)
        {
            var check = Search(name, code, info, parentId, locationTypeId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(LocationModel model, int Id = 0)
        {
            var check = Search(model.Name, model.Code, model.Info, model.ParentId, model.LocationTypeId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }

}