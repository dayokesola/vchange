using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Data.Repository.Account
{
    public partial class ClientRepository : BaseRepository<Client, ClientModel, long>
    { 
        public ClientModel GetByClientId(string clientId)
        {
            var sql = "where clientId = @0";
            AddParam("clientId", clientId);
            return GetRecord(sql);
        }
    }
    /// <summary>
    /// 
    /// </summary>
    public partial class ClientRepository : BaseRepository<Client, ClientModel, long>
    {

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public ClientRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable Client Entity Search
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientSecret1"></param>
        /// <param name="clientSecret2"></param>
        /// <param name="clientTypeId"></param>
        /// <param name="clientStatusId"></param>
        /// <returns></returns>
        public IQueryable<Client> Search(string clientId = "", string clientSecret1 = "", string clientSecret2 = "", int clientTypeId = 0, int clientStatusId = 0)
        {
            var table = Query();
            if (!string.IsNullOrEmpty(clientId))
            {
                table = table.Where(x => x.ClientId == clientId);
            }
            if (!string.IsNullOrEmpty(clientSecret1))
            {
                table = table.Where(x => x.ClientSecret1 == clientSecret1);
            }
            if (!string.IsNullOrEmpty(clientSecret2))
            {
                table = table.Where(x => x.ClientSecret2 == clientSecret2);
            }
            if (clientTypeId > 0)
            {
                table = table.Where(x => x.ClientTypeId == clientTypeId);
            }
            if (clientStatusId > 0)
            {
                table = table.Where(x => x.ClientStatusId == clientStatusId);
            }

            return table;
        }

        /// <summary>
        /// Paged Client Model Search
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientSecret1"></param>
        /// <param name="clientSecret2"></param>
        /// <param name="clientTypeId"></param>
        /// <param name="clientStatusId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<ClientModel> SearchView(string clientId = "", string clientSecret1 = "", string clientSecret2 = "", int clientTypeId = 0, int clientStatusId = 0,
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = " where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(clientId))
            {
                sql += $" and ClientId = @{c} ";
                AddParam("clientId", clientId);
                c++;
            }
            if (!string.IsNullOrEmpty(clientSecret1))
            {
                sql += $" and ClientSecret1 = @{c} ";
                AddParam("clientSecret1", clientSecret1);
                c++;
            }
            if (!string.IsNullOrEmpty(clientSecret2))
            {
                sql += $" and ClientSecret2 = @{c} ";
                AddParam("clientSecret2", clientSecret2);
                c++;
            }
            if (clientTypeId > 0)
            {
                sql += $" and ClientTypeId = @{c} ";
                AddParam("clientTypeId", clientTypeId);
                c++;
            }
            if (clientStatusId > 0)
            {
                sql += $" and ClientStatusId = @{c} ";
                AddParam("clientStatusId", clientStatusId);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<ClientModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<ClientModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get Client Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ClientModel GetModel(long Id)
        {
            var sql = " where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }



        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="clientId"></param>
        /// <param name="clientSecret1"></param>
        /// <param name="clientSecret2"></param>
        /// <param name="clientTypeId"></param>
        /// <param name="clientStatusId"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string clientId = "", string clientSecret1 = "", string clientSecret2 = "", int clientTypeId = 0, int clientStatusId = 0, long Id = 0)
        {
            var check = Search(clientId, clientSecret1, clientSecret2, clientTypeId, clientStatusId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(ClientModel model, long Id = 0)
        {
            var check = Search(model.ClientId, model.ClientSecret1, model.ClientSecret2, model.ClientTypeId, model.ClientStatusId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }

}