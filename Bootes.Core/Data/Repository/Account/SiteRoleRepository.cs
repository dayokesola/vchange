using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Data.Repository.Account
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteRoleRepository : BaseRepository<SiteRole, SiteRoleModel, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public SiteRoleRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable SiteRole Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="departmentId"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="info"></param>
        /// <returns></returns>
        public IQueryable<SiteRole> Search(string name = "", string code = "", int departmentId = 0, int roleTypeId = 0, string info = "")
        {
            var table = Query();
            if (!string.IsNullOrEmpty(name))
            {
                table = table.Where(x => x.Name == name);
            }
            if (!string.IsNullOrEmpty(code))
            {
                table = table.Where(x => x.Code == code);
            }
            if (departmentId > 0)
            {
                table = table.Where(x => x.DepartmentId == departmentId);
            }
            if (roleTypeId > 0)
            {
                table = table.Where(x => x.RoleTypeId == roleTypeId);
            }
            if (!string.IsNullOrEmpty(info))
            {
                table = table.Where(x => x.Info == info);
            }

            return table;
        }

        /// <summary>
        /// Paged SiteRole Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="departmentId"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="info"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<SiteRoleModel> SearchView(string name = "", string code = "", int departmentId = 0, int roleTypeId = 0, string info = "",
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = "where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(name))
            {
                sql += $" and Name = @{c} ";
                AddParam("name", name);
                c++;
            }
            if (!string.IsNullOrEmpty(code))
            {
                sql += $" and Code = @{c} ";
                AddParam("code", code);
                c++;
            }
            if (departmentId > 0)
            {
                sql += $" and DepartmentId = @{c} ";
                AddParam("departmentId", departmentId);
                c++;
            }
            if (roleTypeId > 0)
            {
                sql += $" and RoleTypeId = @{c} ";
                AddParam("roleTypeId", roleTypeId);
                c++;
            }
            if (!string.IsNullOrEmpty(info))
            {
                sql += $" and Info = @{c} ";
                AddParam("info", info);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<SiteRoleModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<SiteRoleModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get SiteRole Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public SiteRoleModel GetModel(int Id)
        {
            var sql = "where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="departmentId"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="info"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string name = "", string code = "", int departmentId = 0, int roleTypeId = 0, string info = "", int Id = 0)
        {
            var check = Search(name, code, departmentId, roleTypeId, info);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(SiteRoleModel model, int Id = 0)
        {
            var check = Search(model.Name, model.Code, model.DepartmentId, model.RoleTypeId, model.Info);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }

}