﻿using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using System;
using System.Linq;

namespace Bootes.Core.Data.Repository.Account
{
    /// <summary>
    /// 
    /// </summary>
    public class AppRepository : BaseRepository<App, AppModel, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public AppRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable App Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="runtimeStatusId"></param>
        /// <param name="availableAt"></param>
        /// <returns></returns>
        public IQueryable<App> Search(string name = "", string code = "", int runtimeStatusId = 0, DateTime? availableAt = null)
        {
            var table = Query();
            if (!string.IsNullOrEmpty(name))
            {
                table = table.Where(x => x.Name == name);
            }
            if (!string.IsNullOrEmpty(code))
            {
                table = table.Where(x => x.Code == code);
            }
            if (runtimeStatusId > 0)
            {
                table = table.Where(x => x.RuntimeStatusId == runtimeStatusId);
            }
            if (availableAt.HasValue)
            {
                var availableAtVal = availableAt.GetValueOrDefault();
                table = table.Where(x => x.AvailableAt == availableAtVal);
            }

            return table;
        }

        /// <summary>
        /// Paged App Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="runtimeStatusId"></param>
        /// <param name="availableAt"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<AppModel> SearchView(string name = "", string code = "", int runtimeStatusId = 0, DateTime? availableAt = null,
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = " where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(name))
            {
                sql += $" and Name = @{c} ";
                AddParam("name", name);
                c++;
            }
            if (!string.IsNullOrEmpty(code))
            {
                sql += $" and Code = @{c} ";
                AddParam("code", code);
                c++;
            }
            if (runtimeStatusId > 0)
            {
                sql += $" and RuntimeStatusId = @{c} ";
                AddParam("runtimeStatusId", runtimeStatusId);
                c++;
            }
            if (availableAt.HasValue)
            {
                var availableAtVal = availableAt.GetValueOrDefault();
                sql += $" and AvailableAt = @{c} ";
                AddParam("availableAt", availableAtVal);
                c++;
            }


            if(page <= 0)
            {
                var l = GetList(sql);
                return new Page<AppModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<AppModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get App Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public AppModel GetModel(int Id)
        {
            var sql = " where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="runtimeStatusId"></param>
        /// <param name="availableAt"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string name = "", string code = "", int runtimeStatusId = 0, DateTime? availableAt = null, int Id = 0)
        {
            var check = Search(name, code, runtimeStatusId, availableAt);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(AppModel model, int Id = 0)
        {
            var check = Search(model.Name, model.Code, model.RuntimeStatusId, model.AvailableAt);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }


}
