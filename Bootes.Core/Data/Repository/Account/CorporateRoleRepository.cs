using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using System.Linq;

namespace Bootes.Core.Data.Repository.Account
{
    /// <summary>
    ///
    /// </summary>
    public class CorporateRoleRepository : BaseRepository<CorporateRole, CorporateRoleModel, int>
    {
        /// <summary>
        ///
        /// </summary>
        /// <param name="context"></param>
        public CorporateRoleRepository(BootesDbContext context) : base(context)
        {
        }

        /// <summary>
        /// IQueryable CorporateRole Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="corporateId"></param>
        /// <param name="isAdmin"></param>
        /// <param name="roleTypeId"></param>
        /// <returns></returns>
        public IQueryable<CorporateRole> Search(string name = "", int corporateId = 0, bool? isAdmin = null, int roleTypeId = 0)
        {
            var table = Query();
            if (!string.IsNullOrEmpty(name))
            {
                table = table.Where(x => x.Name == name);
            }
            if (corporateId > 0)
            {
                table = table.Where(x => x.CorporateId == corporateId);
            }
            if (isAdmin.HasValue)
            {
                var isAdminVal = isAdmin.GetValueOrDefault();
                table = table.Where(x => x.IsAdmin == isAdminVal);
            }
            if (roleTypeId > 0)
            {
                table = table.Where(x => x.RoleTypeId == roleTypeId);
            }

            return table;
        }

        /// <summary>
        /// Paged CorporateRole Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="corporateId"></param>
        /// <param name="isAdmin"></param>
        /// <param name="roleTypeId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<CorporateRoleModel> SearchView(string name = "", int corporateId = 0, bool? isAdmin = null, int roleTypeId = 0,
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = "select * from " + Constants.DBPrefix + "corporaterolemodel where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(name))
            {
                sql += $" and Name = @{c} ";
                AddParam("name", name);
                c++;
            }
            if (corporateId > 0)
            {
                sql += $" and CorporateId = @{c} ";
                AddParam("corporateId", corporateId);
                c++;
            }
            if (isAdmin.HasValue)
            {
                var isAdminVal = isAdmin.GetValueOrDefault();
                sql += $" and IsAdmin = @{c} ";
                AddParam("isAdmin", isAdminVal);
                c++;
            }
            if (roleTypeId > 0)
            {
                sql += $" and RoleTypeId = @{c} ";
                AddParam("roleTypeId", roleTypeId);
                c++;
            }

            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<CorporateRoleModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }

            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<CorporateRoleModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get CorporateRole Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public CorporateRoleModel GetModel(int Id)
        {
            var sql = "select * from " + Constants.DBPrefix + "corporaterolemodel where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="name"></param>
        /// <param name="corporateId"></param>
        /// <param name="isAdmin"></param>
        /// <param name="roleTypeId"></param>
        ///
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string name = "", int corporateId = 0, bool? isAdmin = null, int roleTypeId = 0, int Id = 0)
        {
            var check = Search(name, corporateId, isAdmin, roleTypeId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(CorporateRoleModel model, int Id = 0)
        {
            var check = Search(model.Name, model.CorporateId, model.IsAdmin, model.RoleTypeId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }
}