using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Data.Repository.Account
{
    public partial class CorporateRepository : BaseRepository<Corporate, CorporateModel, int>
    {

        public List<CorporateModel> GetUserCorporates(long userId, int corporateStatus = -1)
        {
            var sql = @"select * from bts_corporatemodel where id in (select corporateId from bts_corporateuser where userId = @0)";
            AddParam("userId", userId);

            if (corporateStatus > -1)
            {
                sql += " and corporatestatus = @1";
                AddParam("corporateStatus", corporateStatus);
            }
            return GetListAnon<CorporateModel>(sql);
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public partial class CorporateRepository : BaseRepository<Corporate, CorporateModel, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public CorporateRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable Corporate Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="countryId"></param>
        /// <param name="regId"></param>
        /// <param name="publicName"></param>
        /// <param name="appId"></param>
        /// <returns></returns>
        public IQueryable<Corporate> Search(string name = "", string code = "", int countryId = 0, string regId = "", string publicName = "", int appId = 0)
        {
            var table = Query();
            if (!string.IsNullOrEmpty(name))
            {
                table = table.Where(x => x.Name == name);
            }
            if (!string.IsNullOrEmpty(code))
            {
                table = table.Where(x => x.Code == code);
            }
            if (countryId > 0)
            {
                table = table.Where(x => x.CountryId == countryId);
            }
            if (!string.IsNullOrEmpty(regId))
            {
                table = table.Where(x => x.RegId == regId);
            }
            if (!string.IsNullOrEmpty(publicName))
            {
                table = table.Where(x => x.PublicName == publicName);
            }
            if (appId > 0)
            {
                table = table.Where(x => x.AppId == appId);
            }

            return table;
        }

        /// <summary>
        /// Paged Corporate Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="countryId"></param>
        /// <param name="regId"></param>
        /// <param name="publicName"></param>
        /// <param name="appId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<CorporateModel> SearchView(string name = "", string code = "", int countryId = 0, string regId = "", 
            string publicName = "", int appId = 0, int corporatestatus = -1,
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = " where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(name))
            {
                sql += $" and Name = @{c} ";
                AddParam("name", name);
                c++;
            }
            if (!string.IsNullOrEmpty(code))
            {
                sql += $" and Code = @{c} ";
                AddParam("code", code);
                c++;
            }
            if (countryId > 0)
            {
                sql += $" and CountryId = @{c} ";
                AddParam("countryId", countryId);
                c++;
            }
            if (!string.IsNullOrEmpty(regId))
            {
                sql += $" and RegId = @{c} ";
                AddParam("regId", regId);
                c++;
            }
            if (!string.IsNullOrEmpty(publicName))
            {
                sql += $" and PublicName = @{c} ";
                AddParam("publicName", publicName);
                c++;
            }
            if (appId > 0)
            {
                sql += $" and AppId = @{c} ";
                AddParam("appId", appId);
                c++;
            }
            if (corporatestatus > -1)
            {
                sql += $" and CorporateStatus = @{c} ";
                AddParam("corporatestatus", corporatestatus);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<CorporateModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<CorporateModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }


        /// <summary>
        /// Get Corporate Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public CorporateModel GetModel(int Id)
        {
            var sql = " where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="countryId"></param>
        /// <param name="regId"></param>
        /// <param name="publicName"></param>
        /// <param name="appId"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string name = "", string code = "", int countryId = 0, string regId = "", string publicName = "", int appId = 0, int Id = 0)
        {
            var check = Search(name, code, countryId, regId, publicName, appId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(CorporateModel model, int Id = 0)
        {
            var check = Search(model.Name, model.Code, model.CountryId, model.RegId, model.PublicName, model.AppId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }

}