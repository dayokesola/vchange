﻿using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Enum;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;
using System;
using System.Linq;

namespace Bootes.Core.Data.Repository.Account
{

    /// <summary>
    /// 
    /// </summary>
    public class SiteUserRepository : BaseRepository<SiteUser, SiteUserModel, long>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public SiteUserRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable SiteUser Entity Search
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="password"></param>
        /// <param name="pIN"></param>
        /// <param name="token"></param>
        /// <param name="appId"></param>
        /// <param name="passwordChangedAt"></param>
        /// <param name="userStatusId"></param>
        /// <param name="mobileValidated"></param>
        /// <param name="emailValidated"></param>
        /// <param name="isLookedOut"></param>
        /// <param name="userStamp"></param>
        /// <param name="countryId"></param>
        /// <returns></returns>
        public IQueryable<SiteUser> Search(string userName = "", string firstName = "", string lastName = "", string email = "", 
            string mobile = "", string token = "", int appId = 0, UserStatus? userStatusId = null, 
            bool? mobileValidated = null, bool? emailValidated = null, bool? isLockedOut = null, string userStamp = "", int countryId = 0)
        {
            var table = Query();
            if (!string.IsNullOrEmpty(userName))
            {
                table = table.Where(x => x.UserName == userName);
            }
            if (!string.IsNullOrEmpty(firstName))
            {
                table = table.Where(x => x.FirstName == firstName);
            }
            if (!string.IsNullOrEmpty(lastName))
            {
                table = table.Where(x => x.LastName == lastName);
            }
            if (!string.IsNullOrEmpty(email))
            {
                table = table.Where(x => x.Email == email);
            }
            if (!string.IsNullOrEmpty(mobile))
            {
                table = table.Where(x => x.Mobile == mobile);
            } 
            if (!string.IsNullOrEmpty(token))
            {
                table = table.Where(x => x.Token == token);
            }
            if (appId > 0)
            {
                table = table.Where(x => x.AppId == appId);
            } 
            if (userStatusId.HasValue)
            {
                var userStatusIdVal = userStatusId.GetValueOrDefault();
                table = table.Where(x => x.UserStatusId == userStatusIdVal);
            }
            if (mobileValidated.HasValue)
            {
                var mobileValidatedVal = mobileValidated.GetValueOrDefault();
                table = table.Where(x => x.MobileValidated == mobileValidatedVal);
            }
            if (emailValidated.HasValue)
            {
                var emailValidatedVal = emailValidated.GetValueOrDefault();
                table = table.Where(x => x.EmailValidated == emailValidatedVal);
            }
            if (isLockedOut.HasValue)
            {
                var isLockedOutVal = isLockedOut.GetValueOrDefault();
                table = table.Where(x => x.IsLockedOut == isLockedOutVal);
            }
            if (!string.IsNullOrEmpty(userStamp))
            {
                var userStampVal = new Guid(userStamp); 
                table = table.Where(x => x.UserStamp == userStampVal);
            }
            if (countryId > 0)
            {
                table = table.Where(x => x.CountryId == countryId);
            }

            return table;
        }

        /// <summary>
        /// Paged SiteUser Model Search
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="password"></param>
        /// <param name="pIN"></param>
        /// <param name="token"></param>
        /// <param name="appId"></param>
        /// <param name="passwordChangedAt"></param>
        /// <param name="userStatusId"></param>
        /// <param name="mobileValidated"></param>
        /// <param name="emailValidated"></param>
        /// <param name="isLockedOut"></param>
        /// <param name="userStamp"></param>
        /// <param name="countryId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<SiteUserModel> SearchView(string userName = "", string firstName = "", string lastName = "", string email = "", 
            string mobile = "", string token = "", int appId = 0,  UserStatus? userStatusId = null,
            bool? mobileValidated = null, bool? emailValidated = null, bool? isLockedOut = null, string userStamp = "", int countryId = 0,
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = " where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(userName))
            {
                sql += $" and UserName = @{c} ";
                AddParam("userName", userName);
                c++;
            }
            if (!string.IsNullOrEmpty(firstName))
            {
                sql += $" and FirstName = @{c} ";
                AddParam("firstName", firstName);
                c++;
            }
            if (!string.IsNullOrEmpty(lastName))
            {
                sql += $" and LastName = @{c} ";
                AddParam("lastName", lastName);
                c++;
            }
            if (!string.IsNullOrEmpty(email))
            {
                sql += $" and Email = @{c} ";
                AddParam("email", email);
                c++;
            }
            if (!string.IsNullOrEmpty(mobile))
            {
                sql += $" and Mobile = @{c} ";
                AddParam("mobile", mobile);
                c++;
            }
            
            if (!string.IsNullOrEmpty(token))
            {
                sql += $" and Token = @{c} ";
                AddParam("token", token);
                c++;
            }
            if (appId > 0)
            {
                sql += $" and AppId = @{c} ";
                AddParam("appId", appId);
                c++;
            }
            
            if (userStatusId.HasValue)
            {
                var userStatusIdVal = userStatusId.GetValueOrDefault();
                sql += $" and UserStatusId = @{c} ";
                AddParam("userStatusId", userStatusIdVal);
                c++;
            }
            if (mobileValidated.HasValue)
            {
                var mobileValidatedVal = mobileValidated.GetValueOrDefault();
                sql += $" and MobileValidated = @{c} ";
                AddParam("mobileValidated", mobileValidatedVal);
                c++;
            }
            if (emailValidated.HasValue)
            {
                var emailValidatedVal = emailValidated.GetValueOrDefault();
                sql += $" and EmailValidated = @{c} ";
                AddParam("emailValidated", emailValidatedVal);
                c++;
            }
            if (isLockedOut.HasValue)
            {
                var isLockedOutVal = isLockedOut.GetValueOrDefault();
                sql += $" and IsLockedOut = @{c} ";
                AddParam("isLockedOut", isLockedOutVal);
                c++;
            }
          
            if (countryId > 0)
            {
                sql += $" and CountryId = @{c} ";
                AddParam("countryId", countryId);
                c++;
            }

            if (!string.IsNullOrEmpty(userStamp))
            {
                sql += $" and Userstamp = @{c} ";
                AddParam("userStamp", userStamp);
                c++;
            }

            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<SiteUserModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<SiteUserModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get SiteUser Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public SiteUserModel GetModel(long Id)
        {
            var sql = " where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="firstName"></param>
        /// <param name="lastName"></param>
        /// <param name="email"></param>
        /// <param name="mobile"></param>
        /// <param name="password"></param>
        /// <param name="pIN"></param>
        /// <param name="token"></param>
        /// <param name="appId"></param>
        /// <param name="passwordChangedAt"></param>
        /// <param name="userStatusId"></param>
        /// <param name="mobileValidated"></param>
        /// <param name="emailValidated"></param>
        /// <param name="isLockedOut"></param>
        /// <param name="userStamp"></param>
        /// <param name="countryId"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string userName = "", string firstName = "", string lastName = "", string email = "",
            string mobile = "", string token = "", int appId = 0,  UserStatus? userStatusId = null,
            bool? mobileValidated = null, bool? emailValidated = null, bool? isLockedOut = null, string userStamp = "", int countryId = 0, long Id = 0)
        {
            var check = Search(userName, firstName, lastName, email, mobile, token, appId, userStatusId, mobileValidated, emailValidated, isLockedOut, userStamp, countryId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(SiteUserModel model, long Id = 0)
        {
            var check = Search(model.UserName, model.FirstName, model.LastName, model.Email, model.Mobile,
                model.Token, model.AppId, model.UserStatusId, model.MobileValidated, model.EmailValidated,
                model.IsLockedOut, model.UserStamp.ToString(), model.CountryId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }

}
