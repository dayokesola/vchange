using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Account;

namespace Bootes.Core.Data.Repository.Account
{

    /// <summary>
    /// 
    /// </summary>
    public class CorporateUserRepository : BaseRepository<CorporateUser, CorporateUserModel, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public CorporateUserRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable CorporateUser Entity Search
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="corporateId"></param>
        /// <param name="corporateRoleId"></param>
        /// <param name="userStatusId"></param>
        /// <param name="isLockedOut"></param>
        /// <param name="authPin"></param>
        /// <returns></returns>
        public IQueryable<CorporateUser> Search(long userId = 0, int corporateId = 0, int corporateRoleId = 0, int userStatusId = 0, bool? isLockedOut = null, string authPin = "")
        {
            var table = Query();
            if (userId > 0)
            {
                table = table.Where(x => x.UserId == userId);
            }
            if (corporateId > 0)
            {
                table = table.Where(x => x.CorporateId == corporateId);
            }
            if (corporateRoleId > 0)
            {
                table = table.Where(x => x.CorporateRoleId == corporateRoleId);
            }
            if (userStatusId > 0)
            {
                table = table.Where(x => x.UserStatusId == userStatusId);
            }
            if (isLockedOut.HasValue)
            {
                var isLockedOutVal = isLockedOut.GetValueOrDefault();
                table = table.Where(x => x.IsLockedOut == isLockedOutVal);
            }
            if (!string.IsNullOrEmpty(authPin))
            {
                table = table.Where(x => x.AuthPin == authPin);
            }

            return table;
        }

        /// <summary>
        /// Paged CorporateUser Model Search
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="corporateId"></param>
        /// <param name="corporateRoleId"></param>
        /// <param name="userStatusId"></param>
        /// <param name="isLockedOut"></param>
        /// <param name="authPin"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<CorporateUserModel> SearchView(long userId = 0, int corporateId = 0, int corporateRoleId = 0, int userStatusId = 0, bool? isLockedOut = null, string authPin = "",
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = " where Id > 0 ";
            var c = 0;

            if (userId > 0)
            {
                sql += $" and UserId = @{c} ";
                AddParam("userId", userId);
                c++;
            }
            if (corporateId > 0)
            {
                sql += $" and CorporateId = @{c} ";
                AddParam("corporateId", corporateId);
                c++;
            }
            if (corporateRoleId > 0)
            {
                sql += $" and CorporateRoleId = @{c} ";
                AddParam("corporateRoleId", corporateRoleId);
                c++;
            }
            if (userStatusId > 0)
            {
                sql += $" and UserStatusId = @{c} ";
                AddParam("userStatusId", userStatusId);
                c++;
            }
            if (isLockedOut.HasValue)
            {
                var isLockedOutVal = isLockedOut.GetValueOrDefault();
                sql += $" and IsLockedOut = @{c} ";
                AddParam("isLockedOut", isLockedOutVal);
                c++;
            }
            if (!string.IsNullOrEmpty(authPin))
            {
                sql += $" and AuthPin = @{c} ";
                AddParam("authPin", authPin);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<CorporateUserModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<CorporateUserModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get CorporateUser Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public CorporateUserModel GetModel(int Id)
        {
            var sql = "where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="corporateId"></param>
        /// <param name="corporateRoleId"></param>
        /// <param name="userStatusId"></param>
        /// <param name="isLockedOut"></param>
        /// <param name="authPin"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(long userId = 0, int corporateId = 0, int corporateRoleId = 0, int userStatusId = 0, bool? isLockedOut = null, string authPin = "", int Id = 0)
        {
            var check = Search(userId, corporateId, corporateRoleId, userStatusId, isLockedOut, authPin);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(CorporateUserModel model, int Id = 0)
        {
            var check = Search(model.UserId, model.CorporateId, model.CorporateRoleId, model.UserStatusId, model.IsLockedOut, model.AuthPin);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }

}