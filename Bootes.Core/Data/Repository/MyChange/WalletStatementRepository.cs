using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.MyChange;
using System;
using System.Linq;

namespace Bootes.Core.Data.Repository.MyChange
{
/// <summary>
    /// 
    /// </summary>
    public class WalletStatementRepository : BaseRepository<WalletStatement, WalletStatementModel, long>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public WalletStatementRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable WalletStatement Entity Search
        /// </summary>
        /// <param name="walletTransactId"></param>
        /// <param name="walletId"></param>
        /// <param name="amount"></param>
        /// <param name="currentBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="drCr"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactionCodeId"></param>
        /// <returns></returns>
        public IQueryable<WalletStatement> Search(long walletTransactId = 0, long walletId = 0, decimal amount = 0, decimal currentBalance = 0, int currencyId = 0, int drCr = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "", string sourceId = "", int transactionCodeId = 0)
        {
            var table = Query();
            if (walletTransactId > 0)
            {
                table = table.Where(x => x.WalletTransactId == walletTransactId);
            }
            if (walletId > 0)
            {
                table = table.Where(x => x.WalletId == walletId);
            }
            if (amount > 0)
            {
                table = table.Where(x => x.Amount == amount);
            }
            if (currentBalance > 0)
            {
                table = table.Where(x => x.CurrentBalance == currentBalance);
            }
            if (currencyId > 0)
            {
                table = table.Where(x => x.CurrencyId == currencyId);
            }
            if (drCr > 0)
            {
                table = table.Where(x => x.DrCr == drCr);
            }
            if (valueDate.HasValue)
            {
                var valueDateVal = valueDate.GetValueOrDefault();
                table = table.Where(x => x.ValueDate == valueDateVal);
            }
            if (!string.IsNullOrEmpty(paymentReference))
            {
                table = table.Where(x => x.PaymentReference == paymentReference);
            }
            if (!string.IsNullOrEmpty(narration))
            {
                table = table.Where(x => x.Narration == narration);
            }
            if (!string.IsNullOrEmpty(sourceId))
            {
                table = table.Where(x => x.SourceId == sourceId);
            }
            if (transactionCodeId > 0)
            {
                table = table.Where(x => x.TransactionCodeId == transactionCodeId);
            }

            return table;
        }

        /// <summary>
        /// Paged WalletStatement Model Search
        /// </summary>
        /// <param name="walletTransactId"></param>
        /// <param name="walletId"></param>
        /// <param name="amount"></param>
        /// <param name="currentBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="drCr"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactionCodeId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<WalletStatementModel> SearchView(long walletTransactId = 0, long walletId = 0, decimal amount = 0, 
            decimal currentBalance = 0, int currencyId = 0, int drCr = 0, DateTime? valueDate = null, 
            string paymentReference = "", string narration = "", string sourceId = "", int transactionCodeId = 0,
            long ownerId = 0, int walletTypeId = 0,
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = "select * from " + Constants.DBPrefix + "walletstatementmodel where Id > 0 ";
            var c = 0;

            if (walletTransactId > 0)
            {
                sql += $" and WalletTransactId = @{c} ";
                AddParam("walletTransactId", walletTransactId);
                c++;
            }
            if (walletId > 0)
            {
                sql += $" and WalletId = @{c} ";
                AddParam("walletId", walletId);
                c++;
            }
            if (amount > 0)
            {
                sql += $" and Amount = @{c} ";
                AddParam("amount", amount);
                c++;
            }
            if (currentBalance > 0)
            {
                sql += $" and CurrentBalance = @{c} ";
                AddParam("currentBalance", currentBalance);
                c++;
            }
            if (currencyId > 0)
            {
                sql += $" and CurrencyId = @{c} ";
                AddParam("currencyId", currencyId);
                c++;
            }
            if (drCr > 0)
            {
                sql += $" and DrCr = @{c} ";
                AddParam("drCr", drCr);
                c++;
            }
            if (valueDate.HasValue)
            {
                var valueDateVal = valueDate.GetValueOrDefault();
                sql += $" and ValueDate = @{c} ";
                AddParam("valueDate", valueDateVal);
                c++;
            }
            if (!string.IsNullOrEmpty(paymentReference))
            {
                sql += $" and PaymentReference = @{c} ";
                AddParam("paymentReference", paymentReference);
                c++;
            }
            if (!string.IsNullOrEmpty(narration))
            {
                sql += $" and Narration = @{c} ";
                AddParam("narration", narration);
                c++;
            }
            if (!string.IsNullOrEmpty(sourceId))
            {
                sql += $" and SourceId = @{c} ";
                AddParam("sourceId", sourceId);
                c++;
            }
            if (transactionCodeId > 0)
            {
                sql += $" and TransactionCodeId = @{c} ";
                AddParam("transactionCodeId", transactionCodeId);
                c++;
            }

            if (ownerId > 0)
            {
                sql += $" and WalletOwnerId = @{c} ";
                AddParam("ownerId", ownerId);
                c++;
            }
            if (walletTypeId > 0)
            {
                sql += $" and walletTypeId = @{c} ";
                AddParam("walletTypeId", walletTypeId);
                c++;
            }

            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<WalletStatementModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<WalletStatementModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get WalletStatement Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public WalletStatementModel GetModel(long Id)
        {
            var sql = "select * from " + Constants.DBPrefix + "walletstatementmodel where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="walletTransactId"></param>
        /// <param name="walletId"></param>
        /// <param name="amount"></param>
        /// <param name="currentBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="drCr"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactionCodeId"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(long walletTransactId = 0, long walletId = 0, decimal amount = 0, decimal currentBalance = 0, int currencyId = 0, int drCr = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "", string sourceId = "", int transactionCodeId = 0, long Id = 0)
        {
            var check = Search(walletTransactId, walletId, amount, currentBalance, currencyId, drCr, valueDate, paymentReference, narration, sourceId, transactionCodeId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(WalletStatementModel model, long Id = 0)
        {
            var check = Search(model.WalletTransactId, model.WalletId, model.Amount, model.CurrentBalance, model.CurrencyId, model.DrCr, model.ValueDate, model.PaymentReference, model.Narration, model.SourceId, model.TransactionCodeId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }
}