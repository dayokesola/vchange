using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.MyChange;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

namespace Bootes.Core.Data.Repository.MyChange
{
    public partial class WalletTransactRepository : BaseRepository<WalletTransact, WalletTransactModel, long>
    {
        public int Post(long transactId)
        {
            try
            {
                var resp1 = -1;
                var tid = new SqlParameter()
                {
                    ParameterName = "@transactId",
                    Value = transactId
                };
                var returnCode = new SqlParameter
                {
                    ParameterName = "@ReturnCode",
                    SqlDbType = SqlDbType.Int,
                    Direction = ParameterDirection.Output
                };
                _context.Database.ExecuteSqlCommand("exec @ReturnCode = bts_FundsTransfer @transactId", returnCode, tid);
                resp1 = Util.ToInt32(returnCode.SqlValue.ToString());
                return resp1;
            }
            catch (Exception ex)
            {
                Log.Error(ex);
                return -2;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public partial class WalletTransactRepository : BaseRepository<WalletTransact, WalletTransactModel, long>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public WalletTransactRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable WalletTransact Entity Search
        /// </summary>
        /// <param name="drWalletId"></param>
        /// <param name="crWalletId"></param>
        /// <param name="amount"></param>
        /// <param name="currencyId"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="initId"></param>
        /// <param name="initDate"></param>
        /// <param name="authId"></param>
        /// <param name="authDate"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactCodeId"></param>
        /// <returns></returns>
        public IQueryable<WalletTransact> Search(long drWalletId = 0, long crWalletId = 0, decimal amount = 0, int currencyId = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "", long initId = 0, DateTime? initDate = null, long authId = 0, DateTime? authDate = null, string sourceId = "", int transactCodeId = 0)
        {
            var table = Query();
            if (drWalletId > 0)
            {
                table = table.Where(x => x.DrWalletId == drWalletId);
            }
            if (crWalletId > 0)
            {
                table = table.Where(x => x.CrWalletId == crWalletId);
            }
            if (amount > 0)
            {
                table = table.Where(x => x.Amount == amount);
            }
            if (currencyId > 0)
            {
                table = table.Where(x => x.CurrencyId == currencyId);
            }
            if (valueDate.HasValue)
            {
                var valueDateVal = valueDate.GetValueOrDefault();
                table = table.Where(x => x.ValueDate == valueDateVal);
            }
            if (!string.IsNullOrEmpty(paymentReference))
            {
                table = table.Where(x => x.PaymentReference == paymentReference);
            }
            if (!string.IsNullOrEmpty(narration))
            {
                table = table.Where(x => x.Narration == narration);
            }
            if (initId > 0)
            {
                table = table.Where(x => x.InitId == initId);
            }
            if (initDate.HasValue)
            {
                var initDateVal = initDate.GetValueOrDefault();
                table = table.Where(x => x.InitDate == initDateVal);
            }
            if (authId > 0)
            {
                table = table.Where(x => x.AuthId == authId);
            }
            if (authDate.HasValue)
            {
                var authDateVal = authDate.GetValueOrDefault();
                table = table.Where(x => x.AuthDate == authDateVal);
            }
            if (!string.IsNullOrEmpty(sourceId))
            {
                table = table.Where(x => x.SourceId == sourceId);
            }
            if (transactCodeId > 0)
            {
                table = table.Where(x => x.TransactCodeId == transactCodeId);
            }

            return table;
        }

        /// <summary>
        /// Paged WalletTransact Model Search
        /// </summary>
        /// <param name="drWalletId"></param>
        /// <param name="crWalletId"></param>
        /// <param name="amount"></param>
        /// <param name="currencyId"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="initId"></param>
        /// <param name="initDate"></param>
        /// <param name="authId"></param>
        /// <param name="authDate"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactCodeId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<WalletTransactModel> SearchView(long drWalletId = 0, long crWalletId = 0, decimal amount = 0,
            int currencyId = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "",
            long initId = 0, DateTime? initDate = null, long authId = 0, DateTime? authDate = null,
            string sourceId = "", int transactCodeId = 0, string txId = "", string gatewayRef = "", int authStatusId = 0,
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = "select * from " + Constants.DBPrefix + "wallettransactmodel where Id > 0 ";
            var c = 0;

            if (drWalletId > 0)
            {
                sql += $" and DrWalletId = @{c} ";
                AddParam("drWalletId", drWalletId);
                c++;
            }
            if (crWalletId > 0)
            {
                sql += $" and CrWalletId = @{c} ";
                AddParam("crWalletId", crWalletId);
                c++;
            }
            if (amount > 0)
            {
                sql += $" and Amount = @{c} ";
                AddParam("amount", amount);
                c++;
            }
            if (currencyId > 0)
            {
                sql += $" and CurrencyId = @{c} ";
                AddParam("currencyId", currencyId);
                c++;
            }
            if (valueDate.HasValue)
            {
                var valueDateVal = valueDate.GetValueOrDefault();
                sql += $" and ValueDate = @{c} ";
                AddParam("valueDate", valueDateVal);
                c++;
            }
            if (!string.IsNullOrEmpty(paymentReference))
            {
                sql += $" and PaymentReference = @{c} ";
                AddParam("paymentReference", paymentReference);
                c++;
            }
            if (!string.IsNullOrEmpty(narration))
            {
                sql += $" and Narration = @{c} ";
                AddParam("narration", narration);
                c++;
            }
            if (initId > 0)
            {
                sql += $" and InitId = @{c} ";
                AddParam("initId", initId);
                c++;
            }
            if (initDate.HasValue)
            {
                var initDateVal = initDate.GetValueOrDefault();
                sql += $" and InitDate = @{c} ";
                AddParam("initDate", initDateVal);
                c++;
            }
            if (authId > 0)
            {
                sql += $" and AuthId = @{c} ";
                AddParam("authId", authId);
                c++;
            }
            if (authDate.HasValue)
            {
                var authDateVal = authDate.GetValueOrDefault();
                sql += $" and AuthDate = @{c} ";
                AddParam("authDate", authDateVal);
                c++;
            }
            if (!string.IsNullOrEmpty(sourceId))
            {
                sql += $" and SourceId = @{c} ";
                AddParam("sourceId", sourceId);
                c++;
            }
            if (!string.IsNullOrEmpty(txId))
            {
                sql += $" and TxId = @{c} ";
                AddParam("txId", txId);
                c++;
            }
            if (!string.IsNullOrEmpty(gatewayRef))
            {
                sql += $" and GateWayReference = @{c} ";
                AddParam("gatewayRef", gatewayRef);
                c++;
            }
            if (authStatusId > 0)
            {
                sql += $" and AuthStatusId = @{c} ";
                AddParam("authStatusId", authStatusId);
                c++;
            }

            if (transactCodeId > 0)
            {
                sql += $" and TransactCodeId = @{c} ";
                AddParam("transactCodeId", transactCodeId);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<WalletTransactModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<WalletTransactModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get WalletTransact Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public WalletTransactModel GetModel(long Id)
        {
            var sql = "select * from " + Constants.DBPrefix + "wallettransactmodel where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="drWalletId"></param>
        /// <param name="crWalletId"></param>
        /// <param name="amount"></param>
        /// <param name="currencyId"></param>
        /// <param name="valueDate"></param>
        /// <param name="paymentReference"></param>
        /// <param name="narration"></param>
        /// <param name="initId"></param>
        /// <param name="initDate"></param>
        /// <param name="authId"></param>
        /// <param name="authDate"></param>
        /// <param name="sourceId"></param>
        /// <param name="transactCodeId"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(long drWalletId = 0, long crWalletId = 0, decimal amount = 0, int currencyId = 0, DateTime? valueDate = null, string paymentReference = "", string narration = "", long initId = 0, DateTime? initDate = null, long authId = 0, DateTime? authDate = null, string sourceId = "", int transactCodeId = 0, long Id = 0)
        {
            var check = Search(drWalletId, crWalletId, amount, currencyId, valueDate, paymentReference, narration, initId, initDate, authId, authDate, sourceId, transactCodeId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(WalletTransactModel model, long Id = 0)
        {
            var check = Search(model.DrWalletId, model.CrWalletId, model.Amount, model.CurrencyId, model.ValueDate, model.PaymentReference, model.Narration, model.InitId, model.InitDate, model.AuthId, model.AuthDate, model.SourceId, model.TransactCodeId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }
}