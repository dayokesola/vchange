﻿using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.MyChange;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.MyChange;
using System.Linq;

namespace Bootes.Core.Data.Repository.MyChange
{

    /// <summary>
    /// 
    /// </summary>
    public class WalletRepository : BaseRepository<Wallet, WalletModel, long>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public WalletRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable Wallet Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="walletTypeId"></param>
        /// <param name="currentBalance"></param>
        /// <param name="openingBalance"></param>
        /// <param name="balanceLimit"></param>
        /// <param name="heldBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        public IQueryable<Wallet> Search(string name = "", string code = "", int walletTypeId = 0, decimal currentBalance = 0, 
            decimal openingBalance = 0, decimal balanceLimit = 0, decimal heldBalance = 0, int currencyId = 0, long ownerId = 0)
        {
            var table = Query();
            if (!string.IsNullOrEmpty(name))
            {
                table = table.Where(x => x.Name == name);
            }
            if (!string.IsNullOrEmpty(code))
            {
                table = table.Where(x => x.Code == code);
            }
            if (walletTypeId > 0)
            {
                table = table.Where(x => x.WalletTypeId == walletTypeId);
            }
            if (currentBalance > 0)
            {
                table = table.Where(x => x.CurrentBalance == currentBalance);
            }
            if (openingBalance > 0)
            {
                table = table.Where(x => x.OpeningBalance == openingBalance);
            }
            if (balanceLimit > 0)
            {
                table = table.Where(x => x.BalanceLimit == balanceLimit);
            }
            if (heldBalance > 0)
            {
                table = table.Where(x => x.HeldBalance == heldBalance);
            }
            if (currencyId > 0)
            {
                table = table.Where(x => x.CurrencyId == currencyId);
            }
            if (ownerId > 0)
            {
                table = table.Where(x => x.OwnerId == ownerId);
            }

            return table;
        }

        /// <summary>
        /// Paged Wallet Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="walletTypeId"></param>
        /// <param name="currentBalance"></param>
        /// <param name="openingBalance"></param>
        /// <param name="balanceLimit"></param>
        /// <param name="heldBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="ownerId"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<WalletModel> SearchView(string name = "", string code = "", int walletTypeId = 0, decimal currentBalance = 0,
            decimal openingBalance = 0, decimal balanceLimit = 0, decimal heldBalance = 0, int currencyId = 0, long ownerId = 0,
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = "select * from " + Constants.DBPrefix + "walletmodel where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(name))
            {
                sql += $" and Name = @{c} ";
                AddParam("name", name);
                c++;
            }
            if (!string.IsNullOrEmpty(code))
            {
                sql += $" and Code = @{c} ";
                AddParam("code", code);
                c++;
            }
            if (walletTypeId > 0)
            {
                sql += $" and WalletTypeId = @{c} ";
                AddParam("walletTypeId", walletTypeId);
                c++;
            }
            if (currentBalance > 0)
            {
                sql += $" and CurrentBalance = @{c} ";
                AddParam("currentBalance", currentBalance);
                c++;
            }
            if (openingBalance > 0)
            {
                sql += $" and OpeningBalance = @{c} ";
                AddParam("openingBalance", openingBalance);
                c++;
            }
            if (balanceLimit > 0)
            {
                sql += $" and BalanceLimit = @{c} ";
                AddParam("balanceLimit", balanceLimit);
                c++;
            }
            if (heldBalance > 0)
            {
                sql += $" and HeldBalance = @{c} ";
                AddParam("heldBalance", heldBalance);
                c++;
            }
            if (currencyId > 0)
            {
                sql += $" and CurrencyId = @{c} ";
                AddParam("currencyId", currencyId);
                c++;
            }
            if (ownerId > 0)
            {
                sql += $" and OwnerId = @{c} ";
                AddParam("ownerId", ownerId);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<WalletModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<WalletModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get Wallet Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public WalletModel GetModel(long Id)
        {
            var sql = "select * from " + Constants.DBPrefix + "walletmodel where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="name"></param>
        /// <param name="code"></param>
        /// <param name="walletTypeId"></param>
        /// <param name="currentBalance"></param>
        /// <param name="openingBalance"></param>
        /// <param name="balanceLimit"></param>
        /// <param name="heldBalance"></param>
        /// <param name="currencyId"></param>
        /// <param name="ownerId"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string name = "", string code = "", int walletTypeId = 0, decimal currentBalance = 0,
            decimal openingBalance = 0, decimal balanceLimit = 0, decimal heldBalance = 0, int currencyId = 0, long ownerId = 0, long Id = 0)
        {
            var check = Search(name, code, walletTypeId, currentBalance, openingBalance, balanceLimit, heldBalance, currencyId, ownerId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(WalletModel model, long Id = 0)
        {
            var check = Search(model.Name, model.Code, model.WalletTypeId, model.CurrentBalance, model.OpeningBalance, model.BalanceLimit, model.HeldBalance, model.CurrencyId, model.OwnerId);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }
}
