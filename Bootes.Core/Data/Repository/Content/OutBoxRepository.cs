using System;
using System.Linq;
using System.Collections.Generic;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Content;

namespace Bootes.Core.Data.Repository.Content
{

    /// <summary>
    /// 
    /// </summary>
    public class OutBoxRepository : BaseRepository<OutBox, OutBoxModel, long>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public OutBoxRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable OutBox Entity Search
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="messageCode"></param>
        /// <param name="messageTypeId"></param>
        /// <param name="scheduledAt"></param>
        /// <param name="contents"></param>
        /// <param name="attachments"></param>
        /// <param name="isSent"></param>
        /// <param name="sentAt"></param>
        /// <param name="sentRemarks"></param>
        /// <returns></returns>
        public IQueryable<OutBox> Search(string toAddress = "", string fromAddress = "", string subject = "", string messageCode = "", int messageTypeId = 0, DateTime? scheduledAt = null, string contents = "", string attachments = "", bool? isSent = null, DateTime? sentAt = null, string sentRemarks = "")
        {
            var table = Query();
            if (!string.IsNullOrEmpty(toAddress))
            {
                table = table.Where(x => x.ToAddress == toAddress);
            }
            if (!string.IsNullOrEmpty(fromAddress))
            {
                table = table.Where(x => x.FromAddress == fromAddress);
            }
            if (!string.IsNullOrEmpty(subject))
            {
                table = table.Where(x => x.Subject == subject);
            }
            if (!string.IsNullOrEmpty(messageCode))
            {
                table = table.Where(x => x.MessageCode == messageCode);
            }
            if (messageTypeId > 0)
            {
                table = table.Where(x => x.MessageTypeId == messageTypeId);
            }
            if (scheduledAt.HasValue)
            {
                var scheduledAtVal = scheduledAt.GetValueOrDefault();
                table = table.Where(x => x.ScheduledAt == scheduledAtVal);
            }
            if (!string.IsNullOrEmpty(contents))
            {
                table = table.Where(x => x.Contents == contents);
            }
            if (!string.IsNullOrEmpty(attachments))
            {
                table = table.Where(x => x.Attachments == attachments);
            }
            if (isSent.HasValue)
            {
                var isSentVal = isSent.GetValueOrDefault();
                table = table.Where(x => x.IsSent == isSentVal);
            }
            if (sentAt.HasValue)
            {
                var sentAtVal = sentAt.GetValueOrDefault();
                table = table.Where(x => x.SentAt == sentAtVal);
            }
            if (!string.IsNullOrEmpty(sentRemarks))
            {
                table = table.Where(x => x.SentRemarks == sentRemarks);
            }

            return table;
        }

        /// <summary>
        /// Paged OutBox Model Search
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="messageCode"></param>
        /// <param name="messageTypeId"></param>
        /// <param name="scheduledAt"></param>
        /// <param name="contents"></param>
        /// <param name="attachments"></param>
        /// <param name="isSent"></param>
        /// <param name="sentAt"></param>
        /// <param name="sentRemarks"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<OutBoxModel> SearchView(string toAddress = "", string fromAddress = "", string subject = "", string messageCode = "", int messageTypeId = 0, DateTime? scheduledAt = null, string contents = "", string attachments = "", bool? isSent = null, DateTime? sentAt = null, string sentRemarks = "",
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = " where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(toAddress))
            {
                sql += $" and ToAddress = @{c} ";
                AddParam("toAddress", toAddress);
                c++;
            }
            if (!string.IsNullOrEmpty(fromAddress))
            {
                sql += $" and FromAddress = @{c} ";
                AddParam("fromAddress", fromAddress);
                c++;
            }
            if (!string.IsNullOrEmpty(subject))
            {
                sql += $" and Subject = @{c} ";
                AddParam("subject", subject);
                c++;
            }
            if (!string.IsNullOrEmpty(messageCode))
            {
                sql += $" and MessageCode = @{c} ";
                AddParam("messageCode", messageCode);
                c++;
            }
            if (messageTypeId > 0)
            {
                sql += $" and MessageTypeId = @{c} ";
                AddParam("messageTypeId", messageTypeId);
                c++;
            }
            if (scheduledAt.HasValue)
            {
                var scheduledAtVal = scheduledAt.GetValueOrDefault();
                sql += $" and ScheduledAt = @{c} ";
                AddParam("scheduledAt", scheduledAtVal);
                c++;
            }
            if (!string.IsNullOrEmpty(contents))
            {
                sql += $" and Contents = @{c} ";
                AddParam("contents", contents);
                c++;
            }
            if (!string.IsNullOrEmpty(attachments))
            {
                sql += $" and Attachments = @{c} ";
                AddParam("attachments", attachments);
                c++;
            }
            if (isSent.HasValue)
            {
                var isSentVal = isSent.GetValueOrDefault();
                sql += $" and IsSent = @{c} ";
                AddParam("isSent", isSentVal);
                c++;
            }
            if (sentAt.HasValue)
            {
                var sentAtVal = sentAt.GetValueOrDefault();
                sql += $" and SentAt = @{c} ";
                AddParam("sentAt", sentAtVal);
                c++;
            }
            if (!string.IsNullOrEmpty(sentRemarks))
            {
                sql += $" and SentRemarks = @{c} ";
                AddParam("sentRemarks", sentRemarks);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<OutBoxModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<OutBoxModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get OutBox Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public OutBoxModel GetModel(long Id)
        {
            var sql = " where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="toAddress"></param>
        /// <param name="fromAddress"></param>
        /// <param name="subject"></param>
        /// <param name="messageCode"></param>
        /// <param name="messageTypeId"></param>
        /// <param name="scheduledAt"></param>
        /// <param name="contents"></param>
        /// <param name="attachments"></param>
        /// <param name="isSent"></param>
        /// <param name="sentAt"></param>
        /// <param name="sentRemarks"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string toAddress = "", string fromAddress = "", string subject = "", string messageCode = "", int messageTypeId = 0, DateTime? scheduledAt = null, string contents = "", string attachments = "", bool? isSent = null, DateTime? sentAt = null, string sentRemarks = "", long Id = 0)
        {
            var check = Search(toAddress, fromAddress, subject, messageCode, messageTypeId, scheduledAt, contents, attachments, isSent, sentAt, sentRemarks);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(OutBoxModel model, long Id = 0)
        {
            var check = Search(model.ToAddress, model.FromAddress, model.Subject, model.MessageCode, model.MessageTypeId, model.ScheduledAt, model.Contents, model.Attachments, model.IsSent, model.SentAt, model.SentRemarks);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }
}