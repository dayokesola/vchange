﻿using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Content;
using System;
using System.Linq;

namespace Bootes.Core.Data.Repository.Content
{
    /// <summary>
    /// 
    /// </summary>
    public class ArticleRepository : BaseRepository<Article, ArticleModel, int>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public ArticleRepository(BootesDbContext context) : base(context)
        {
        }
        /// <summary>
        /// IQueryable Article Entity Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="title"></param>
        /// <param name="tags"></param>
        /// <param name="content"></param>
        /// <param name="editorType"></param>
        /// <param name="altContents"></param>
        /// <returns></returns>
        public IQueryable<Article> Search(string name = "", string title = "", string tags = "", string content = "", int editorType = 0, string altContents = "")
        {
            var table = Query();
            if (!string.IsNullOrEmpty(name))
            {
                table = table.Where(x => x.Name == name);
            }
            if (!string.IsNullOrEmpty(title))
            {
                table = table.Where(x => x.Title == title);
            }
            if (!string.IsNullOrEmpty(tags))
            {
                table = table.Where(x => x.Tags == tags);
            }
            if (!string.IsNullOrEmpty(content))
            {
                table = table.Where(x => x.Content == content);
            }
            if (editorType > 0)
            {
                table = table.Where(x => x.EditorType == editorType);
            }
            if (!string.IsNullOrEmpty(altContents))
            {
                table = table.Where(x => x.AltContents == altContents);
            }

            return table;
        }

        /// <summary>
        /// Paged Article Model Search
        /// </summary>
        /// <param name="name"></param>
        /// <param name="title"></param>
        /// <param name="tags"></param>
        /// <param name="content"></param>
        /// <param name="editorType"></param>
        /// <param name="altContents"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<ArticleModel> SearchView(string name = "", string title = "", string tags = "", string content = "", int editorType = 0, string altContents = "",
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = "where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(name))
            {
                sql += $" and Name = @{c} ";
                AddParam("name", name);
                c++;
            }
            if (!string.IsNullOrEmpty(title))
            {
                sql += $" and Title = @{c} ";
                AddParam("title", title);
                c++;
            }
            if (!string.IsNullOrEmpty(tags))
            {
                sql += $" and Tags = @{c} ";
                AddParam("tags", tags);
                c++;
            }
            if (!string.IsNullOrEmpty(content))
            {
                sql += $" and Content = @{c} ";
                AddParam("content", content);
                c++;
            }
            if (editorType > 0)
            {
                sql += $" and EditorType = @{c} ";
                AddParam("editorType", editorType);
                c++;
            }
            if (!string.IsNullOrEmpty(altContents))
            {
                sql += $" and AltContents = @{c} ";
                AddParam("altContents", altContents);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<ArticleModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<ArticleModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get Article Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public ArticleModel GetModel(int Id)
        {
            var sql = "where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="name"></param>
        /// <param name="title"></param>
        /// <param name="tags"></param>
        /// <param name="content"></param>
        /// <param name="editorType"></param>
        /// <param name="altContents"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string name = "", string title = "", string tags = "", string content = "", int editorType = 0, string altContents = "", int Id = 0)
        {
            var check = Search(name, title, tags, content, editorType, altContents);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(ArticleModel model, int Id = 0)
        {
            var check = Search(model.Name, model.Title, model.Tags, model.Content, model.EditorType, model.AltContents);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }



}
