using Bootes.Core.Common;
using Bootes.Core.Data.DB;
using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Model;
using Bootes.Core.Domain.Model.Content;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Bootes.Core.Data.Repository.Content
{
    public partial class TagRepository : BaseRepository<Tag, TagModel, long>
    {
        public void DeleteByObject(long objectId, string objecttype)
        {
            var txt = "Delete " + Constants.DBPrefix + "tag where objecttype = '{0}' and objectId = {1}";
            _context.Database.ExecuteSqlCommand(string.Format(txt, objecttype, objectId));
        }


        public List<TagFrequency> TagFrequenies(string objecttype)
        {
            var txt = @"select TagName, count(id) as Frequency from bts_tag where ObjectType = '{0}' group by TagName"; 
            return GetListAnon<TagFrequency>(string.Format(txt, objecttype));
        }

        public int DeleteById(long id)
        {
            var txt = "Delete " + Constants.DBPrefix + "tag where Id = '{0}'";
            return _context.Database.ExecuteSqlCommand(string.Format(txt, id));
        }
    }

    /// <summary>
    /// 
    /// </summary>
    public partial class TagRepository : BaseRepository<Tag, TagModel, long>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public TagRepository(BootesDbContext context) : base(context)
        {
        }


        /// <summary>
        /// IQueryable Tag Entity Search
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="objectId"></param>
        /// <param name="tagName"></param>
        /// <returns></returns>
        public IQueryable<Tag> Search(string objectType = "", long objectId = 0, string tagName = "")
        {
            var table = Query();
            if (!string.IsNullOrEmpty(objectType))
            {
                table = table.Where(x => x.ObjectType == objectType);
            }
            if (objectId > 0)
            {
                table = table.Where(x => x.ObjectId == objectId);
            }
            if (!string.IsNullOrEmpty(tagName))
            {
                table = table.Where(x => x.TagName == tagName);
            }

            return table;
        }

        /// <summary>
        /// Paged Tag Model Search
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="objectId"></param>
        /// <param name="tagName"></param>
        /// <param name="page"></param>
        ///<param name="pageSize"></param>
        ///<param name="sort"></param>
        /// <returns></returns>
        public Page<TagModel> SearchView(string objectType = "", long objectId = 0, string tagName = "",
            long page = 1, long pageSize = 10, string sort = "Id")
        {
            var sql = "select * from " + Constants.DBPrefix + "tagmodel where Id > 0 ";
            var c = 0;

            if (!string.IsNullOrEmpty(objectType))
            {
                sql += $" and ObjectType = @{c} ";
                AddParam("objectType", objectType);
                c++;
            }
            if (objectId > 0)
            {
                sql += $" and ObjectId = @{c} ";
                AddParam("objectId", objectId);
                c++;
            }
            if (!string.IsNullOrEmpty(tagName))
            {
                sql += $" and TagName = @{c} ";
                AddParam("tagName", tagName);
                c++;
            }


            if (page <= 0)
            {
                var l = GetList(sql);
                return new Page<TagModel>()
                {
                    CurrentPage = 0,
                    Items = l,
                    ItemsPerPage = 0,
                    TotalItems = 0,
                    TotalPages = 0
                };
            }


            sql += ApplySort(sort);
            var k = SearchView(sql, page, pageSize);
            return new Page<TagModel>()
            {
                CurrentPage = k.CurrentPage,
                Items = k.Items,
                ItemsPerPage = k.ItemsPerPage,
                TotalItems = k.TotalItems,
                TotalPages = k.TotalPages
            };
        }

        /// <summary>
        /// Get Tag Entity
        /// </summary>
        /// <param name="Id"></param>
        /// <returns></returns>
        public TagModel GetModel(long Id)
        {
            var sql = "select * from " + Constants.DBPrefix + "tagmodel where Id = @0";
            AddParam("Id", Id);
            return GetRecord(sql);
        }

        /// <summary>
        /// Check exists
        /// </summary>
        /// <param name="objectType"></param>
        /// <param name="objectId"></param>
        /// <param name="tagName"></param>
        /// 
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(string objectType = "", long objectId = 0, string tagName = "", long Id = 0)
        {
            var check = Search(objectType, objectId, tagName);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }

        /// <summary>
        /// check exists
        /// </summary>
        /// <param name="model"></param>
        /// <param name="Id"></param>
        /// <returns></returns>
        public bool ItemExists(TagModel model, long Id = 0)
        {
            var check = Search(model.ObjectType, model.ObjectId, model.TagName);
            if (Id > 0)
            {
                check = check.Where(x => x.Id != Id);
            }
            return check.Any();
        }
    }
}