﻿using Bootes.Core.Common;
using NPoco;
using NPoco.FluentMappings;

namespace Bootes.Core.Data.DB
{
    public static class BootesPoco
    {
        /// <summary>
        /// 
        /// </summary>
        public static DatabaseFactory DbFactory { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public static void Setup()
        {
            //var fluentConfig = FluentMappingConfiguration.Configure(new BootesMappings());
            DbFactory = DatabaseFactory.Config(x =>
            {
                x.UsingDatabase(() => new NPoco.Database(Settings.Connection()));
                //x.WithFluentConfig(fluentConfig);/

            });
        }
    }
}
