﻿using Bootes.Core.Common;
using Bootes.Core.Domain.Entity.Account;
using Bootes.Core.Domain.Entity.Content;
using Bootes.Core.Domain.Entity.MyChange;
using System.Data.Entity;

namespace Bootes.Core.Data.DB
{
    /// <summary>
    /// 
    /// </summary>
    public class BootesDbContext : DbContext
    {
        /// <summary>
        /// 
        /// </summary>
        public BootesDbContext() : base(Settings.Connection())
        {
            Database.SetInitializer<BootesDbContext>(null);
        }
        public DbSet<App> Apps { get; set; }
        public DbSet<Article> Articles { get; set; }
        public DbSet<Client> Clients { get; set; }
        public DbSet<CorporateRole> CorporateRoles { get; set; }
        public DbSet<Corporate> Corporates { get; set; }
        public DbSet<CorporateUser> CorporateUsers { get; set; }
        public DbSet<EntitySetting> EntitySettings { get; set; }
        public DbSet<Location> Locations { get; set; }
        public DbSet<OutBox> OutBoxes { get; set; }
        public DbSet<Permission> Permissions { get; set; }
        public DbSet<SiteRole> SiteRoles { get; set; }
        public DbSet<SiteUser> SiteUsers { get; set; }
        public DbSet<Tag> Tags { get; set; }


        public DbSet<Wallet> Wallets { get; set; }
        public DbSet<WalletTransact> WalletTransacts { get; set; }
        public DbSet<WalletStatement> WalletStatements { get; set; }


        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Types().Configure(entity => entity.ToTable(Constants.DBPrefix + entity.ClrType.Name.ToLower()));
            base.OnModelCreating(modelBuilder);
        }
    }
}
