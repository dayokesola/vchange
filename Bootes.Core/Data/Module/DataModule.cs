﻿using Bootes.Core.Data.DB;
using Bootes.Core.Data.Repository.Account;
using Bootes.Core.Data.Repository.Content;
using Bootes.Core.Data.Repository.MyChange;

namespace Bootes.Core.Data.Module
{
    public class DataModule
    {
        private AppRepository _apps;
        private ArticleRepository _articles;
        private ClientRepository _clients;
        private CorporateRoleRepository _corporateroles;
        private CorporateRepository _corporates;
        private CorporateUserRepository _corporateusers;
        private EntitySettingRepository _entitysettings;
        private LocationRepository _locations;
        private OutBoxRepository _outboxes;
        private PermissionRepository _permissions;
        private SiteRoleRepository _siteroles;
        private SiteUserRepository _siteusers;
        private TagRepository _tags;
        private WalletRepository _wallets;
        private WalletStatementRepository _walletstatements;
        private WalletTransactRepository _wallettransacts;
        private BootesDbContext context;
        /// <summary>
        /// 
        /// </summary>
        public DataModule()
        {

        }

        /// <summary>
        /// 
        /// </summary>
        public AppRepository Apps { get { if (_apps == null) { _apps = new AppRepository(_context); } return _apps; } }

        public ArticleRepository Articles { get { if (_articles == null) { _articles = new ArticleRepository(_context); } return _articles; } }
        public ClientRepository Clients { get { if (_clients == null) { _clients = new ClientRepository(_context); } return _clients; } }
        public CorporateRoleRepository CorporateRoles { get { if (_corporateroles == null) { _corporateroles = new CorporateRoleRepository(_context); } return _corporateroles; } }
        public CorporateRepository Corporates { get { if (_corporates == null) { _corporates = new CorporateRepository(_context); } return _corporates; } }
        public CorporateUserRepository CorporateUsers { get { if (_corporateusers == null) { _corporateusers = new CorporateUserRepository(_context); } return _corporateusers; } }
        public EntitySettingRepository EntitySettings { get { if (_entitysettings == null) { _entitysettings = new EntitySettingRepository(_context); } return _entitysettings; } }
        public LocationRepository Locations { get { if (_locations == null) { _locations = new LocationRepository(_context); } return _locations; } }
        public OutBoxRepository OutBoxes { get { if (_outboxes == null) { _outboxes = new OutBoxRepository(_context); } return _outboxes; } }
        public PermissionRepository Permissions { get { if (_permissions == null) { _permissions = new PermissionRepository(_context); } return _permissions; } }
        public SiteRoleRepository SiteRoles { get { if (_siteroles == null) { _siteroles = new SiteRoleRepository(_context); } return _siteroles; } }
        /// <summary>
        /// 
        /// </summary>
        public SiteUserRepository SiteUsers { get { if (_siteusers == null) { _siteusers = new SiteUserRepository(_context); } return _siteusers; } }

        public TagRepository Tags { get { if (_tags == null) { _tags = new TagRepository(_context); } return _tags; } }
        public WalletRepository Wallets { get { if (_wallets == null) { _wallets = new WalletRepository(_context); } return _wallets; } }
        public WalletStatementRepository WalletStatements { get { if (_walletstatements == null) { _walletstatements = new WalletStatementRepository(_context); } return _walletstatements; } }
        public WalletTransactRepository WalletTransacts { get { if (_wallettransacts == null) { _wallettransacts = new WalletTransactRepository(_context); } return _wallettransacts; } }
        private BootesDbContext _context
        {
            get
            {
                if (context == null)
                {
                    context = new Data.DB.BootesDbContext();
                }
                return context;
            }
        }
    }
}
